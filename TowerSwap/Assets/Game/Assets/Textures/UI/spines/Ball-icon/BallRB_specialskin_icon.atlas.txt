
BallRB_specialskin_icon.png
size: 358,204
format: RGBA8888
filter: Linear,Linear
repeat: none
1_angel
  rotate: true
  xy: 287, 120
  size: 82, 28
  orig: 84, 30
  offset: 1, 1
  index: -1
1_body
  rotate: false
  xy: 2, 61
  size: 141, 141
  orig: 143, 143
  offset: 1, 1
  index: -1
1_eyeball
  rotate: false
  xy: 245, 2
  size: 71, 24
  orig: 73, 26
  offset: 1, 1
  index: -1
1_eyes
  rotate: false
  xy: 2, 5
  size: 88, 54
  orig: 90, 56
  offset: 1, 1
  index: -1
1_mouth_basic
  rotate: false
  xy: 182, 5
  size: 38, 11
  orig: 40, 13
  offset: 1, 1
  index: -1
1_mouth_smile
  rotate: true
  xy: 317, 131
  size: 71, 26
  orig: 73, 28
  offset: 1, 1
  index: -1
1_mouth_wow
  rotate: false
  xy: 245, 28
  size: 29, 31
  orig: 31, 33
  offset: 1, 1
  index: -1
1_wing
  rotate: true
  xy: 215, 20
  size: 39, 28
  orig: 41, 30
  offset: 1, 1
  index: -1
2_body
  rotate: true
  xy: 145, 61
  size: 141, 140
  orig: 143, 142
  offset: 1, 1
  index: -1
2_eyeball
  rotate: true
  xy: 332, 58
  size: 71, 24
  orig: 73, 26
  offset: 1, 1
  index: -1
2_eyebrow
  rotate: true
  xy: 301, 37
  size: 81, 29
  orig: 83, 31
  offset: 1, 1
  index: -1
2_eyes
  rotate: false
  xy: 92, 5
  size: 88, 54
  orig: 90, 56
  offset: 1, 1
  index: -1
2_horn
  rotate: false
  xy: 276, 28
  size: 23, 31
  orig: 23, 31
  offset: 0, 0
  index: -1
2_mouth
  rotate: true
  xy: 318, 7
  size: 28, 19
  orig: 30, 21
  offset: 1, 1
  index: -1
2_wing
  rotate: true
  xy: 182, 18
  size: 41, 31
  orig: 43, 33
  offset: 1, 1
  index: -1
