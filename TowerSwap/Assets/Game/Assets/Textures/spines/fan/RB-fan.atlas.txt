
RB-fan.png
size: 225,133
format: RGBA8888
filter: Linear,Linear
repeat: none
fan
  rotate: false
  xy: 3, 36
  size: 186, 94
  orig: 186, 94
  offset: 0, 0
  index: -1
fan1
  rotate: false
  xy: 3, 3
  size: 124, 30
  orig: 126, 32
  offset: 1, 1
  index: -1
fan2
  rotate: false
  xy: 130, 4
  size: 35, 29
  orig: 37, 31
  offset: 1, 1
  index: -1
fan3
  rotate: true
  xy: 192, 6
  size: 124, 30
  orig: 126, 32
  offset: 1, 1
  index: -1
