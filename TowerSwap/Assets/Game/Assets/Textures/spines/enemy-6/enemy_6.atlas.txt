
enemy_6.png
size: 256,256
format: RGBA8888
filter: Linear,Linear
repeat: none
3/1
  rotate: true
  xy: 219, 149
  size: 32, 21
  orig: 32, 21
  offset: 0, 0
  index: -1
3/11
  rotate: false
  xy: 2, 127
  size: 125, 125
  orig: 125, 125
  offset: 0, 0
  index: -1
3/2
  rotate: false
  xy: 221, 215
  size: 28, 37
  orig: 28, 37
  offset: 0, 0
  index: -1
3/22
  rotate: false
  xy: 129, 193
  size: 41, 59
  orig: 41, 59
  offset: 0, 0
  index: -1
3/3
  rotate: false
  xy: 214, 107
  size: 34, 21
  orig: 34, 21
  offset: 0, 0
  index: -1
3/33
  rotate: false
  xy: 187, 70
  size: 36, 22
  orig: 36, 22
  offset: 0, 0
  index: -1
3/4
  rotate: false
  xy: 179, 162
  size: 28, 43
  orig: 28, 43
  offset: 0, 0
  index: -1
3/44
  rotate: true
  xy: 125, 82
  size: 43, 60
  orig: 43, 60
  offset: 0, 0
  index: -1
3/5
  rotate: false
  xy: 209, 183
  size: 40, 22
  orig: 40, 22
  offset: 0, 0
  index: -1
3/55
  rotate: false
  xy: 179, 130
  size: 38, 30
  orig: 38, 30
  offset: 0, 0
  index: -1
3/6
  rotate: true
  xy: 182, 5
  size: 32, 54
  orig: 32, 54
  offset: 0, 0
  index: -1
3/66
  rotate: true
  xy: 125, 39
  size: 41, 60
  orig: 41, 60
  offset: 0, 0
  index: -1
3/7
  rotate: false
  xy: 2, 4
  size: 121, 121
  orig: 121, 121
  offset: 0, 0
  index: -1
3/77
  rotate: true
  xy: 187, 94
  size: 34, 25
  orig: 34, 25
  offset: 0, 0
  index: -1
3/8
  rotate: false
  xy: 129, 146
  size: 48, 45
  orig: 48, 45
  offset: 0, 0
  index: -1
3/88
  rotate: true
  xy: 125, 3
  size: 34, 55
  orig: 34, 55
  offset: 0, 0
  index: -1
3/9
  rotate: false
  xy: 182, 2
  size: 1, 1
  orig: 1, 1
  offset: 0, 0
  index: -1
3/mathit
  rotate: false
  xy: 172, 207
  size: 47, 45
  orig: 47, 45
  offset: 0, 0
  index: -1
