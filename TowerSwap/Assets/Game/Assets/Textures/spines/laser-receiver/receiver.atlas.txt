
receiver.png
size: 108,150
format: RGBA8888
filter: Linear,Linear
repeat: none
blue/light
  rotate: true
  xy: 50, 2
  size: 80, 46
  orig: 80, 51
  offset: 0, 0
  index: -1
blue/projecter
  rotate: false
  xy: 2, 84
  size: 104, 31
  orig: 104, 31
  offset: 0, 0
  index: -1
green/light
  rotate: true
  xy: 2, 2
  size: 80, 46
  orig: 82, 53
  offset: 1, 1
  index: -1
green/projecter
  rotate: false
  xy: 2, 117
  size: 104, 31
  orig: 106, 33
  offset: 1, 1
  index: -1
