
boss_6.png
size: 389,279
format: RGBA8888
filter: Linear,Linear
repeat: none
4/1
  rotate: true
  xy: 192, 20
  size: 41, 28
  orig: 41, 28
  offset: 0, 0
  index: -1
4/11
  rotate: true
  xy: 137, 17
  size: 119, 53
  orig: 136, 60
  offset: 10, 1
  index: -1
4/12
  rotate: false
  xy: 2, 138
  size: 139, 139
  orig: 139, 139
  offset: 0, 0
  index: -1
4/14
  rotate: true
  xy: 352, 63
  size: 31, 28
  orig: 31, 28
  offset: 0, 0
  index: -1
4/2
  rotate: false
  xy: 192, 63
  size: 34, 75
  orig: 34, 75
  offset: 0, 0
  index: -1
4/23
  rotate: false
  xy: 143, 140
  size: 81, 28
  orig: 81, 28
  offset: 0, 0
  index: -1
4/3
  rotate: false
  xy: 316, 14
  size: 41, 28
  orig: 41, 28
  offset: 0, 0
  index: -1
4/33
  rotate: true
  xy: 228, 48
  size: 31, 69
  orig: 31, 69
  offset: 0, 0
  index: -1
4/4
  rotate: true
  xy: 229, 151
  size: 34, 75
  orig: 34, 75
  offset: 0, 0
  index: -1
4/43
  rotate: false
  xy: 310, 81
  size: 31, 69
  orig: 31, 69
  offset: 0, 0
  index: -1
4/46
  rotate: true
  xy: 359, 30
  size: 31, 28
  orig: 31, 28
  offset: 0, 0
  index: -1
4/5
  rotate: false
  xy: 299, 44
  size: 51, 35
  orig: 51, 35
  offset: 0, 0
  index: -1
4/6
  rotate: false
  xy: 228, 81
  size: 39, 68
  orig: 39, 68
  offset: 0, 0
  index: -1
4/7
  rotate: false
  xy: 222, 11
  size: 51, 35
  orig: 51, 35
  offset: 0, 0
  index: -1
4/8
  rotate: false
  xy: 269, 81
  size: 39, 68
  orig: 39, 68
  offset: 0, 0
  index: -1
4/9
  rotate: false
  xy: 2, 5
  size: 133, 131
  orig: 133, 131
  offset: 0, 0
  index: -1
4/d1
  rotate: false
  xy: 229, 187
  size: 99, 90
  orig: 99, 90
  offset: 0, 0
  index: -1
4/mat_hit
  rotate: true
  xy: 330, 152
  size: 125, 56
  orig: 132, 57
  offset: 0, 1
  index: -1
4/tp1
  rotate: false
  xy: 343, 96
  size: 35, 54
  orig: 35, 54
  offset: 0, 0
  index: -1
4/tp2
  rotate: true
  xy: 275, 2
  size: 40, 39
  orig: 40, 39
  offset: 0, 0
  index: -1
4/tp3
  rotate: false
  xy: 143, 170
  size: 84, 107
  orig: 86, 107
  offset: 0, 0
  index: -1
