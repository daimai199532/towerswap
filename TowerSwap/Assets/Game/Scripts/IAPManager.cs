﻿
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.Purchasing;

public class IAPManager : IStoreListener
{
    private static IAPManager m_instance;

    public static IAPManager Instance => m_instance ?? (m_instance = new IAPManager());

    public static Action<string, bool> purchaseResultEvent;

    private static IStoreController m_StoreController;          // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    public const string PRODUCT_ID_VIP_SUBSCRIPTION = "com.fanmob.ball.bounce.sub";
    public const string PRODUCT_ID_REMOVE_ADS_PLUS = "com.fanmob.ball.bounce.pack2";
    public const string PRODUCT_ID_REMOVE_ADS = "com.fanmob.ball.bounce.pack1";
    public const string PRODUCT_ID_SPECIAL_OFFER = "com.fanmob.ball.bounce.specialoffer";

    // Apple App Store-specific product identifier for the subscription product.
    private const string PRODUCT_VIP_SUBSCRIPTION_APPLE_ID = "com.fanmob.ball.bounce.sub";

    // Google Play Store-specific product identifier subscription product.
    private const string PRODUCT_VIP_SUBSCRIPTION_GOOGLE_ID = "com.fanmob.ball.bounce.sub";

    public void Init()
    {
        if (m_StoreController == null)
            InitializePurchasing();
    }

    public void InitializePurchasing()
    {
        if (IsInitialized())
        {
            return;
        }

        var builder = ConfigurationBuilder.Instance(StandardPurchasingModule.Instance());
        builder.AddProduct(PRODUCT_ID_SPECIAL_OFFER, ProductType.Consumable);
        builder.AddProduct(PRODUCT_ID_REMOVE_ADS_PLUS, ProductType.Consumable);
        builder.AddProduct(PRODUCT_ID_REMOVE_ADS, ProductType.Consumable);
        builder.AddProduct(PRODUCT_ID_VIP_SUBSCRIPTION, ProductType.Subscription, new IDs(){
                        { PRODUCT_VIP_SUBSCRIPTION_APPLE_ID, AppleAppStore.Name },
                        { PRODUCT_VIP_SUBSCRIPTION_GOOGLE_ID, GooglePlay.Name },
                    });
        UnityPurchasing.Initialize(this, builder);
    }


    private bool IsInitialized()
    {
        // Only say we are initialized if both the Purchasing references are set.
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public Product GetProduct(string productId)
    {
        if(m_StoreController == null)
            return null;
        return m_StoreController.products.WithID(productId);
    }

    public void BuyProductID(string productId)
    {
        // If Purchasing has been initialized ...
        if (IsInitialized())
        {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID(productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase)
            {
                Debug.Log(string.Format("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase(product);
            }
            // Otherwise ...
            else
            {
                // ... report the product look-up failure situation  
                Debug.Log("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else
        {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log("BuyProductID FAIL. Not initialized.");
        }
    }


    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases()
    {
        // If Purchasing has not yet been set up ...
        if (!IsInitialized())
        {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log("RestorePurchases FAIL. Not initialized.");
            return;
        }

        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer)
        {
            // ... begin restoring purchases
            Debug.Log("RestorePurchases started ...");

            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions((result) =>
            {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
            });
        }
        // Otherwise ...
        else
        {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
        }
    }

    //////////////////////////////////////////////////////////////////////////
    #region IStoreListener

    public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
    {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log("OnInitialized: PASS");

        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;
    }


    public void OnInitializeFailed(InitializationFailureReason error)
    {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log("OnInitializeFailed InitializationFailureReason:" + error);
    }


    public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs args)
    {
        //// A consumable product has been purchased by this user.
        //if (String.Equals(args.purchasedProduct.definition.id, kProductIDConsumable, StringComparison.Ordinal))
        //{
        //    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //    // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
        //    purchaseResultEvent?.Invoke(args.purchasedProduct.definition.storeSpecificId, true);
        //}
        //// Or ... a non-consumable product has been purchased by this user.
        //else if (String.Equals(args.purchasedProduct.definition.id, kProductIDNonConsumable, StringComparison.Ordinal))
        //{
        //    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //    purchaseResultEvent?.Invoke(args.purchasedProduct.definition.storeSpecificId, true);
        //}
        //// Or ... a subscription product has been purchased by this user.
        //else if (String.Equals(args.purchasedProduct.definition.id, PRODUCT_ID_VIP_SUBSCRIPTION, StringComparison.Ordinal))
        //{
        //    Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
        //    purchaseResultEvent?.Invoke(args.purchasedProduct.definition.storeSpecificId, true);
        //}
        //// Or ... an unknown product has been purchased by this user. Fill in additional products here....
        //else
        //{
        //    purchaseResultEvent?.Invoke(args.purchasedProduct.definition.storeSpecificId, false);
        //}

        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        purchaseResultEvent?.Invoke(args.purchasedProduct.definition.storeSpecificId, true);
        return PurchaseProcessingResult.Complete;
    }


    public void OnPurchaseFailed(Product product, PurchaseFailureReason failureReason)
    {
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log(string.Format("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        purchaseResultEvent?.Invoke(product.definition.storeSpecificId, false);
    }

    #endregion
}
