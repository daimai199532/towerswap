﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectTeleportChild : MonoBehaviour
{
    [SerializeField] private ObjectTeleport m_teleport;
    public void OnTriggerExit2D(Collider2D collider)
    {
       m_teleport.RemoveIgnore(collider.gameObject);
    }
}
