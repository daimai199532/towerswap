﻿
using System;
using DG.Tweening;
using Spine.Unity;
using TMPro;
using UnityEngine;

public class ObjectCoin : MonoBehaviour
{
    [SerializeField] private GameObject m_effect;
    [SerializeField] private GameObject m_text;
    private bool m_stop;
    private Transform m_target;

    void Awake()
    {
        m_stop = false;
    }
    
    private void OnActive(bool active)
    {
        gameObject.SetActive(active);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_stop)
            return;
        switch (collision.tag)
        {
            case GameConstant.TAG_MAGNET:
                m_target = collision.transform;
                break;
            case GameConstant.TAG_PLAYER:
                Player p = collision.GetComponent<Player>();
                if(p == null)
                    return;
                DoTrigger();
                break;
        }
    }

    void DoTrigger()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_EAT_COIN, false);
        m_stop = true;
        m_effect.transform.parent.SetParent(transform.parent, false);
        m_effect.transform.parent.position = transform.position;
        m_effect.transform.parent.gameObject.SetActive(true);
        m_effect.GetComponent<SkeletonAnimation>().AnimationState.Complete += (entry) =>
        {
            Destroy(m_effect.transform.parent.gameObject);
        };
        //effect text
        TextMeshPro text = m_text.GetComponent<TextMeshPro>();
        text.text = "+" + GameConstant.COIN_RATIO;
        m_text.SetActive(true);
        m_text.transform.SetParent(transform.parent, true);
        m_text.transform.localEulerAngles = Vector3.zero;
        m_text.transform.DOMoveY(transform.position.y + 1.5f, 0.5f).OnComplete(()=>{
            text.DOFade(0, 0.5f).OnComplete(()=>{
                m_text.transform.DOKill();
                text.DOKill();
                Destroy(m_text);
            });
        });
        //
        GameController.UpdateCoin(1);
        Destroy(gameObject);
    }

    void Update()
    {
        if(m_target == null || m_stop)
            return;
        transform.position = Vector3.Lerp(transform.position, m_target.position, 0.1f);
    }
}
