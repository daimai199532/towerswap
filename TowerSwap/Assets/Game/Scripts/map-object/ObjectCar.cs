﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCar : MonoBehaviour
{
    [SerializeField] private Vector2 m_startPoint;
    [SerializeField] private Vector2 m_endPoint;
    [SerializeField] private GameObject m_startPointObj;
    [SerializeField] private GameObject m_endPointObj;
    [SerializeField] private SpriteRenderer m_line;
    [SerializeField] private GameObject m_truckle;

    public void UpdateLine()
    {
        m_startPointObj.transform.localPosition = m_startPoint;
        m_endPointObj.transform.localPosition = m_endPoint;
        m_line.transform.localPosition = m_startPoint;  
        Vector2 direction = (m_endPoint - m_startPoint);
        Vector2 directNorm = direction.normalized;
        m_line.size = new Vector2(direction.magnitude, m_line.size.y);
        m_line.transform.localEulerAngles = new Vector3(0, 0, Mathf.Atan2(directNorm.y, directNorm.x)*Mathf.Rad2Deg);    
    }

    public void UpdatePos(Vector2 startPos, Vector2 endPos)
    {
        m_startPoint = transform.InverseTransformPoint(startPos);
        m_endPoint = transform.InverseTransformPoint(endPos);
        UpdateLine();
    }

    public void InitPoint()
    {
        m_startPoint = m_startPointObj.transform.localPosition;
        m_endPoint = m_endPointObj.transform.localPosition;
    }

    public Vector2 GetStartPoint()
    {
        return m_startPointObj.transform.position;
    }

    public Vector2 GetEndPoint()
    {
        return m_endPointObj.transform.position;
    }
}
