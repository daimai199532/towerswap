﻿using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine;

public class ObjectCloud : MonoBehaviour
{
    enum State
    {
        Idle,
        Awake,
        Show,
        Hide
    }

    [SerializeField] private float m_timeShow = 5;
    [SerializeField] private float m_timeHide = 2;
    private Collider2D m_collider;
    private SkeletonAnimation m_animation;

    private State m_state;
    private bool m_loop;

    void Start()
    {
        m_collider = GetComponent<BoxCollider2D>();
        m_animation = GetComponent<SkeletonAnimation>();
        m_state = State.Idle;
        m_loop = false;
        m_animation.AnimationState.Complete += OnAnimComplete;
        m_animation.AnimationState.SetAnimation(0, "idle", true);
    }

    void OnDestroy()
    {
        m_animation.AnimationState.Complete -= OnAnimComplete;
    }


    IEnumerator Fade()
    {
        m_animation.AnimationState.SetAnimation(0, "hit", true);
        m_state = State.Awake;
        yield return new WaitForSeconds(m_timeShow);
        SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_CLOUD_OUT, 5, false, transform.position);
        m_collider.enabled = false;
        m_state = State.Hide;
        m_animation.AnimationState.SetAnimation(0, "off", false);
        
    }

    IEnumerator Show()
    {
        yield return new WaitForSeconds(m_timeHide);
        SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_CLOUD_IN, 5, false, transform.position);
        m_state = State.Show;
      
        m_animation.AnimationState.SetAnimation(0, "on", false);
        // yield return new WaitForSeconds(1.35f);
        m_collider.enabled = true;

    }

    private void OnAnimComplete(TrackEntry trackentry)
    {
        if(trackentry.Animation.Name == "idle" || trackentry.Animation.Name == "hit")
            return;
        switch (m_state)
        {
            case State.Show:
                if (m_loop)
                    StartCoroutine(Fade());
                else
                {
                    m_animation.AnimationState.SetAnimation(0, "idle", true);
                    m_state = State.Idle;
                }
             
                break;
            case State.Hide:
                StartCoroutine(Show());
                break;
            default:
                m_animation.AnimationState.SetAnimation(0, "idle", true);
                break;
        }
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
      
    }
    private void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.collider.tag == GameConstant.TAG_PLAYER && m_state == State.Idle && collision.rigidbody.velocity.y <= 0)
        {
            m_loop = true;
            m_animation.AnimationState.SetAnimation(0, "hit", false);
            if (m_state == State.Idle)
                StartCoroutine(Fade());
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag == GameConstant.TAG_PLAYER && m_state == State.Awake)
        {
            m_loop = false;
            //m_state = State.Idle;
            //StopAllCoroutines();
        }
    }
}
