﻿
using Spine.Unity;
using UnityEngine;

public class ObjectMonsterStatic : MonoBehaviour
{
    [SerializeField] private int m_healthReduce = 20;
    [SerializeField] private int m_ballHitTimes = -1;

    [SerializeField] private GameObject m_effectDead;

    private bool m_dead;

    private int m_count;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null)
            return;
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            GameController.UpdateHealth(-1 * m_healthReduce);
            if(m_ballHitTimes < 0)
                return;
            m_count ++;
            if(m_count >= m_ballHitTimes)
                Dead();
        }
    }

    public void Dead()
    {
        SoundManager.PlaySound3D(GameConstant.AUDIO_BALL_HURT_ENEMI, 10, false, transform.position);
        m_dead = true;
        GetComponent<PointEffector2D>().enabled = false;
        Collider2D collider = GetComponent<Collider2D>();
        collider.enabled = false;
        GameObject eff = Instantiate(m_effectDead);
        eff.transform.SetParent(transform.parent, false);
        eff.transform.localPosition = transform.localPosition;
        Destroy(gameObject);
    }
}
