﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFan : MonoBehaviour
{
    [SerializeField] private BoxCollider2D m_collider;
    [SerializeField] private BuoyancyEffector2D m_effector;
    [SerializeField] private Transform m_wind;
    [SerializeField] private float m_height;

    public void UpdateHeight()
    {
        m_collider.size = new Vector2(m_collider.size.x, m_height);
        m_collider.offset = new Vector2(0, m_height/2);
        m_wind.localScale = new Vector3(m_wind.localScale.x, m_height*0.1f + 0.2f, 1);
        m_effector.surfaceLevel = m_height + 1;        
    }   

    // private void OnTriggerEnter2D(Collider2D collider)
    // {
    //     if(collider.tag != GameConstant.TAG_PLAYER)
    //         return;
    //     Player player = collider.GetComponent<Player>();
    //     if(player == null)
    //         return;
    //     player.controller.FakeJump();
    // } 

}
