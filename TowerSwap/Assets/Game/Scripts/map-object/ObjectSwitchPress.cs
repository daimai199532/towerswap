﻿
using System.Collections.Generic;
using DG.Tweening;
using Spine.Unity;
using UnityEngine;

public class ObjectSwitchPress : MonoBehaviour
{
    [SerializeField] private string m_key = "";
    [SerializeField] private Transform m_button;

    private bool m_on;
    private List<Collider2D> m_colliders;
    private float m_posY;

    void Start()
    {
        m_on = false;
        m_colliders = new List<Collider2D>();
        m_posY = m_button.localPosition.y;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null)
            return;
        if (collision.tag == GameConstant.TAG_PLAYER || collision.tag == GameConstant.TAG_OBJECT_BOX || collision.tag == GameConstant.TAG_MONSTER_MOVE_GROUND)
        {
            m_colliders.Add(collision);
            if(m_on)
                return;
            SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_SWITCH_PRESS, 5, false, transform.position);
            m_on = true;   
            m_button.DOKill();         
            m_button.DOLocalMoveY(0, 0.2f);
            GameController.DoTrigger(m_key, true);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject == null)
            return;
        if (collision.tag == GameConstant.TAG_PLAYER || collision.tag == GameConstant.TAG_OBJECT_BOX || collision.tag == GameConstant.TAG_MONSTER_MOVE_GROUND)
        {
            m_colliders.Remove(collision);
            if(!m_on || m_colliders.Count > 0)
                return;
            m_on = false;
            m_button.DOKill();         
            m_button.DOLocalMoveY(m_posY, 0.2f);
            GameController.DoTrigger(m_key, false);
        }
    }
}
