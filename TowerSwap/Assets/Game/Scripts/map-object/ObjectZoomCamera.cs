﻿
using System.Collections.Generic;
using UnityEngine;

public class ObjectZoomCamera : MonoBehaviour
{
    [SerializeField] private float m_ratio = 1.5f;
    [SerializeField] private float m_time = 2f;

    private List<GameObject> m_players = new List<GameObject>();

    private void OnTriggerEnter2D(Collider2D collision)
    {  
        if (collision.tag != GameConstant.TAG_PLAYER)
            return;
        Player p = collision.GetComponent<Player>();
        if(p == null || m_players.Contains(p.gameObject))
            return;
        m_players.Add(p.gameObject);
        if(m_players.Count > 1)
            return;
        GameController.ZoomCamera(m_ratio, m_time);
    } 
    private void OnTriggerExit2D(Collider2D collision)
    { 
        if (collision.tag != GameConstant.TAG_PLAYER)
            return;
        Player p = collision.GetComponent<Player>();
        if(p == null || !m_players.Contains(p.gameObject))
            return;   
        m_players.Remove(p.gameObject);
        if(m_players.Count > 0)
            return;
        GameController.ZoomCamera(-1, m_time);
    } 
}
