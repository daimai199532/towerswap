﻿
using Spine;
using Spine.Unity;
using UnityEngine;

public class ObjectMonsterMoveFlat : MonoBehaviour
{
    [SerializeField] private int m_healthReduce = 20;
    [SerializeField] private int m_forceTopMagnitude = 1000;
    [SerializeField] private float m_speed = 5;
    [SerializeField] private LayerMask m_wallLayer;
    [SerializeField] private Rigidbody2D m_rigidbody;
    [SerializeField] private BoxCollider2D m_collider;
    [SerializeField] private GameObject m_body;
    [SerializeField] private SkeletonAnimation m_spine;
    [SerializeField] private GameObject m_effectDead;

    private bool m_dead;
    private int m_direction;

    // Start is called before the first frame update
    void Start()
    {
        m_dead = false;
        m_direction = 1;
    }

    public int GetDamage()
    {
        return m_healthReduce;
    }

    public bool IsDead()
    {
        return m_dead;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == null || m_dead)
            return;
        if (collision.gameObject.tag == GameConstant.TAG_PLAYER)
        {
            SoundManager.PlaySound3D(GameConstant.AUDIO_BALL_HURT_ENEMI, 10, false, transform.position);
            m_dead = true;
            collision.rigidbody.AddForce(new Vector2(0, m_forceTopMagnitude));
            m_collider.enabled = false;
            m_rigidbody.simulated = false;
            m_body.SetActive(false);
            m_spine.AnimationState.SetAnimation(0, "dead", false);
            m_spine.AnimationState.Complete += OnDead;
        }
    }

    private void OnDead(TrackEntry trackentry)
    {        
        GameObject eff = Instantiate(m_effectDead);
        eff.transform.SetParent(transform.parent, false);
        eff.transform.localPosition = transform.localPosition;
        Destroy(gameObject);
    }

    private void Move()
    {
        m_rigidbody.velocity = new Vector2(m_direction*m_speed, 0);
    }

    void FixedUpdate()
    {
        if(m_dead)
            return;
        RaycastHit2D raycastLeft = Physics2D.Raycast(m_collider.bounds.center, Vector2.left, m_collider.bounds.extents.x + 0.5f, m_wallLayer);
        if (raycastLeft.collider != null)
            m_direction = 1;
        RaycastHit2D raycastRight = Physics2D.Raycast(m_collider.bounds.center, Vector2.right, m_collider.bounds.extents.x + 0.5f, m_wallLayer);
        if (raycastRight.collider != null)
            m_direction = -1;
        Move();
    }
}
