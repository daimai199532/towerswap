﻿
using Spine;
using Spine.Unity;
using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class ObjectHome : MonoBehaviour
{
    public static Action<bool, PlayerType> areaEvent;
    public static Action<bool, PlayerType> readyFinishEvent;

    [SerializeField] private PlayerType m_playerType = PlayerType.Red;
    [SerializeField] private BoxCollider2D m_hitBox;
    [SerializeField] private Transform m_finishPoint;
    [SerializeField] private SkeletonAnimation m_spine;
    [SerializeField] private GameObject m_active;

    private bool m_stop;
    private bool m_complete;
    private bool m_area;
    private bool m_readyFinish;
    private GameObject m_soundWheel;

    void Awake()
    {
        GameController.updatePointEvent += OnPointUpdate;
        areaEvent += OnArea;
        readyFinishEvent += OnFinish;
        m_hitBox.enabled = false;
        m_stop = false;
        m_complete = false;
        m_area = false;
        m_readyFinish = false;
    }

    void Start()
    {
        m_active.SetActive(false);
        StartCoroutine(DelayStart());
    }

    IEnumerator DelayStart()
    {
        yield return null;
        yield return null;
        m_spine.state.SetAnimation(0, "close", false);
        m_spine.state.Complete += OnOpenComplete;
    }

    void OnDestroy()
    {
        GameController.updatePointEvent -= OnPointUpdate;
        areaEvent -= OnArea;
        readyFinishEvent -= OnFinish;
        StopAllCoroutines();
    }

    private void OnFinish(bool finished, PlayerType playerType)
    {
        if(playerType == m_playerType || m_stop || !finished || !m_readyFinish)
            return;        
        m_stop = true;
        GameController.Finish(m_finishPoint.position, m_playerType); 
        if(m_readyFinish)
            readyFinishEvent?.Invoke(m_readyFinish, m_playerType);
        if(finished && m_readyFinish)
            GameController.FinishLevel();
    }

    private void OnArea(bool status, PlayerType playerType)
    {
        if(playerType != m_playerType)
            return;
        if(m_area && !status && m_complete)
        {
            StopAllCoroutines();
            Destroy(m_soundWheel);
            m_spine.state.SetAnimation(0, "close2", false);
            m_hitBox.enabled = false;            
        }
        m_area = status;
        if(m_area && m_complete)
            Open();        
    }

    private void OnPointUpdate(int remainTotal, int remain, PlayerType playerType)
    {
        if(playerType != m_playerType)
            return;
        if (remain <= 0)
        {
            m_complete = true;
            m_active.SetActive(true);
            if(m_area)
                Open();
        }
    }

    private void OnOpenComplete(TrackEntry trackEntry)
    {        
        if(trackEntry.Animation.Name == "open")
        {            
            m_soundWheel = SoundManager.PlaySound3D(GameConstant.AUDIO_HOME_WHEEL, 10, true, transform.position);
            m_spine.state.SetAnimation(0, "open2", true);
        }
    }


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(m_stop)
            return;
        if(collision.tag != GameConstant.TAG_PLAYER)
            return;
        Player player = collision.GetComponent<Player>();
        if(player.playerType != m_playerType)
            return;     
        player.PlayAnimLaugh();
        m_readyFinish = true;
        readyFinishEvent?.Invoke(m_readyFinish, m_playerType);        
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(m_stop)
            return;
        if(collision.tag != GameConstant.TAG_PLAYER)
            return;
        Player player = collision.GetComponent<Player>();
        if(player.playerType != m_playerType)
            return;  
        player.PlayAnimIdle();
        m_readyFinish = false;
    }

    void Open()
    {
        SoundManager.PlaySound3D(GameConstant.AUDIO_HOME_OPEN, 10, false, transform.position);
        m_spine.state.SetAnimation(0, "open", false);              
        StartCoroutine(WaitingActiveDoor());  
    }
    IEnumerator WaitingActiveDoor()
    {
        yield return new WaitForSeconds(1f);
        m_hitBox.enabled = true;
    }

    public PlayerType GetPlayerType()
    {
        return m_playerType;
    }
}
