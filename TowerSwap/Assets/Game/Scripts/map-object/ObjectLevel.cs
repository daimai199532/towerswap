﻿
using TMPro;
using UnityEngine;

public class ObjectLevel : MonoBehaviour
{
    [SerializeField] private TextMeshPro m_text;

    void Start()
    {
        m_text.text = MainModel.mapLevel < 1 ? "TUTORIAL" : ("LEVEL " + MainModel.mapLevel);
    }
}
