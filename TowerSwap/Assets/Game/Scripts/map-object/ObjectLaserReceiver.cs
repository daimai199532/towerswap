﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class ObjectLaserReceiver : MonoBehaviour
{
    [SerializeField] private string m_key = "";
    [SerializeField] private SkeletonAnimation m_anim;
    private float m_timeReceived = -1;
    private bool m_received = false;
    private bool m_active = false;
    
    void Start()
    {
        m_timeReceived = -1;
        m_active = false;
        m_received = false;
    }

    void Update()
    {
        if(m_timeReceived < 0 || !m_received)
            return;
        float delTime = Time.time - m_timeReceived;
        if(delTime > 0.2f)
        {
            m_received = false;
            m_active = false;
            m_anim.state.SetAnimation(0, "close", false);
            GameController.DoTrigger(m_key, false);            
            return;
        }
        if(m_active)
            return;
        m_active = true;
        m_anim.state.SetAnimation(0, "open", false);
        GameController.DoTrigger(m_key, true);
    }

    public void Received()
    {
        m_timeReceived = Time.time;
        m_received = true;
    }
}
