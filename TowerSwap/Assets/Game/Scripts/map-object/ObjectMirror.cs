﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ObjectMirror : MonoBehaviour
{
    [SerializeField] private string m_key = "";
    [SerializeField] private float m_startAngle = 0;
    [SerializeField] private float m_maxAngle = -1;
    [SerializeField] private float m_rotateSpeed = 1;
    [SerializeField] private bool m_loop = false;
    [SerializeField] private Transform m_mirror;

    private bool m_state;
    private Tweener m_tween;

    void Awake()
    {
        m_state = false;
        UpdateRotate();
        GameController.triggerEvent += OnTrigger;
    }

    void OnDestroy()
    {
        GameController.triggerEvent -= OnTrigger;
        if(m_tween != null)
            m_tween.Kill();
        m_mirror.DOKill();
    }

    private void OnTrigger(string key, bool state)
    {
        if(m_key != key)
            return;        
        m_state = !m_state;
        if(m_loop)
        {
            if(m_state)
                Rotate();
            else
            {
                if(m_tween != null)
                    m_tween.Pause();
            }
        }else
        {
            if(m_state)
                RotateToTarget();
            else
                ResetAngle();
        }
        
    }

    void Rotate()
    {
        if(m_tween == null)
        {
            float time = (m_maxAngle - m_startAngle)/m_rotateSpeed;
            m_tween = m_mirror.DOLocalRotate(new Vector3(0,0,m_maxAngle), time, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetAutoKill(false).SetLoops(-1, LoopType.Yoyo);
        }else
            m_tween.Play();
    }

    void RotateToTarget()
    {
        if(m_tween == null)
        {
            float time = (m_maxAngle - m_startAngle)/m_rotateSpeed;
            m_tween = m_mirror.DOLocalRotate(new Vector3(0,0,m_maxAngle), time, RotateMode.FastBeyond360).SetEase(Ease.Linear).SetAutoKill(false);
        }else
            m_tween.PlayForward();
    }

    void ResetAngle()
    {
        if(m_tween != null)
            m_tween.PlayBackwards();
    }

    public void UpdateRotate()
    {
        m_mirror.localEulerAngles = new Vector3(0,0,m_startAngle);
    }
}
