﻿using DG.Tweening;
using UnityEngine;

public class ObjectDoor : MonoBehaviour
{
    [SerializeField] private string m_key = "";
    [SerializeField] private float m_time = 1;
    [SerializeField] private float m_distance = 4;
    [SerializeField] private SpriteMaskInteraction m_maskInteraction = SpriteMaskInteraction.VisibleInsideMask;
    [SerializeField] private Transform m_door;
    [SerializeField] private BoxCollider2D m_collider;
    [SerializeField] private SpriteRenderer m_sprite;

    private Vector2 m_starPos;
    private bool m_state;
    private GameObject m_sound;

    void Awake()
    {
        m_state = false;
        GameController.triggerEvent += OnTrigger;
        m_starPos = m_door.localPosition;
        m_sprite.maskInteraction = m_maskInteraction;
    }

    void OnDestroy()
    {
        GameController.triggerEvent -= OnTrigger;
        m_door.DOKill();
    }

    private void OnTrigger(string key, bool state)
    {
        if(m_key != key)
            return;        
        m_state = !m_state;
        if(m_state)
            Open();
        else
            Close();
    }

    private void Open()
    {
        if(m_sound == null)
            m_sound = SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_DOOR_SWITCH, 10, true, transform.position);
        m_door.DOKill();
        m_door.DOLocalMoveY(m_starPos.y + m_distance, m_time).OnComplete(() =>
        {
            m_collider.enabled = m_maskInteraction == SpriteMaskInteraction.None;
            Destroy(m_sound);
        });
    }

    private void Close()
    {
        if(m_sound == null)
            m_sound = SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_DOOR_SWITCH, 10, true, transform.position);
        m_collider.enabled = true;
        m_door.DOKill();
        m_door.DOLocalMoveY(m_starPos.y, m_time).OnComplete(() =>
        {
            Destroy(m_sound);
        });
    }
}
