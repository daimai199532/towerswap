﻿
using UnityEngine;

public class ObjectSavePoint : MonoBehaviour
{
    [SerializeField] private GameObject m_off;
    [SerializeField] private GameObject m_on;

    private bool m_saved;

    // Start is called before the first frame update
    void Start()
    {
        m_off.SetActive(true);
        m_on.SetActive(false);
        m_saved = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null)
            return;
        if(collision.tag == GameConstant.TAG_PLAYER && !m_saved)
        {
            SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_CHECK_POINT, 5, false, transform.position);
            m_off.SetActive(false);
            m_on.SetActive(true);
            m_saved = true;
            //GameController.SavePoint(transform.position);
        }
    }
}
