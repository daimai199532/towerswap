﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectLaserProjecter : MonoBehaviour
{
    private const float MAX_DISTANCE = 1000f;

    [SerializeField] private Color m_color;
    [SerializeField] private float m_size = 0.1f;
    [SerializeField] private LayerMask m_layer;
    [SerializeField] private LineRenderer m_lineRenderer;

    void Start()
    {
        //m_lineRenderer.material = new Material(Shader.Find("Sprites/Default"));
        m_lineRenderer.startColor = m_color;
        m_lineRenderer.endColor = m_color;
        m_lineRenderer.startWidth = m_size;
        m_lineRenderer.endWidth = m_size;
    }

    void Update()
    {
        CalculateLaserLine(transform.position, transform.up);
    }

    void CalculateLaserLine(Vector3 startPos, Vector3 direction)
    {
        List<Vector3> listPos = new List<Vector3>();
        listPos.Add(startPos);
        RaycastHit2D ray;
        int count = 0;
        bool loop = true;
        while(loop)
        {
            count ++;
            ray =  Physics2D.Raycast(startPos + direction*0.1f, direction, MAX_DISTANCE, m_layer);
            if(ray.collider == null)
            {
                listPos.Add(startPos + direction*MAX_DISTANCE);
                break;
            }else
            {
                listPos.Add(ray.point);
                switch(ray.collider.tag)
                {      
                    case GameConstant.TAG_MIRROR:
                        break;              
                    case GameConstant.TAG_LASER_RECEIVER:
                        loop = false;
                        ObjectLaserReceiver receiver = ray.collider.GetComponent<ObjectLaserReceiver>();
                        if(receiver != null)
                            receiver.Received();
                        break;
                    default:
                        loop = false;
                        break;
                }
                startPos = ray.point;
                direction = Vector3.Reflect(direction, ray.normal).normalized;
            }
            if(count > 10)
                break;
        }     
        m_lineRenderer.positionCount = listPos.Count; 
        m_lineRenderer.SetPositions(listPos.ToArray());
    }
}
