﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public enum WaterType{
    Red,
    Blue,
    Black
}

public class ObjectWater : MonoBehaviour
{
    [SerializeField] private SpriteRenderer m_sprite;
    [SerializeField] private Transform m_notice;
    [SerializeField] private BoxCollider2D m_collider;
    [SerializeField] private float m_width = 1f;
    [SerializeField] private float m_animSpeed = 1f;
    [SerializeField] private float m_noticeHeight = 1;
    [SerializeField] private int m_healthReduce = 100;
    [SerializeField] private float m_force = 3000;
    [SerializeField] private WaterType m_type = WaterType.Red;
    [SerializeField] private SpriteRenderer m_iceSprite;

    private List<GameObject> m_lasersHot = new List<GameObject>();
    private List<GameObject> m_lasersCold = new List<GameObject>();   
    private Tweener m_tweenAnim;
    private Tweener m_tweenResume;
    private bool m_isFreeze;
    void Start()
    {
        m_sprite.gameObject.SetActive(true);
        m_iceSprite.gameObject.SetActive(false);
        RunAnim();
    }

    void OnDestroy()
    {
        m_tweenAnim.Kill();
        m_tweenResume.Kill();
    }

    void RunAnim()
    {
        if(m_tweenAnim == null)
            m_tweenAnim = m_sprite.material.DOOffset(new Vector2(1, 0), m_animSpeed).SetLoops(-1).SetEase(Ease.Linear).SetAutoKill(false);
        else
            m_tweenAnim.Play();
    }

    public void UpdateNotice()
    {
        m_notice.localPosition = new Vector3(0, m_noticeHeight, 0);
    }

    public void UpdateWidth()
    {
        m_sprite.size = new Vector2(m_width, m_sprite.size.y);
        m_collider.size = new Vector2(m_width, m_collider.size.y);
        m_iceSprite.size = new Vector2(m_width, m_iceSprite.size.y);
        BoxCollider2D box = m_iceSprite.GetComponent<BoxCollider2D>();
        box.size = new Vector2(m_width, box.size.y);
    }

    private void OnTriggerEnter2D(Collider2D collider)
    {
        if(m_isFreeze)
            return;
        if(collider.tag != GameConstant.TAG_PLAYER)
            return;
        Player player = collider.GetComponent<Player>();
        if(player == null)
            return;
        if((int)player.playerType != (int) m_type)
        {
            GameController.UpdateHealth(-m_healthReduce);
            Vector2 force = transform.up*m_force;
            player.controller.Rigidbody.AddForce(force);
        }
    }

    void Freeze()
    {
        if(m_isFreeze)
            return;
        m_isFreeze = true;
        m_tweenAnim.Pause();
        if(m_tweenResume != null)
            m_tweenResume.Kill();
        m_sprite.gameObject.SetActive(false);
        m_iceSprite.gameObject.SetActive(true);
    }

    void Resume()
    {
        if(!m_isFreeze || m_lasersCold.Count > 0 || m_lasersHot.Count < 1)
            return;
        m_isFreeze = false;
        m_sprite.gameObject.SetActive(true);
        m_sprite.size = new Vector2(0, m_sprite.size.y);
        if(m_tweenResume != null)
            m_tweenResume.Kill();
        m_tweenResume = DOTween.To(() => 0f, x => {
                m_sprite.size = new Vector2(x, m_sprite.size.y);
        }, m_width, 1f).OnComplete(()=>{
            m_tweenAnim.Play();
            m_iceSprite.gameObject.SetActive(false);
        });
    }

    public void AddLaserHot(GameObject go)
    {
        if(m_lasersHot.Contains(go))
            return;
        m_lasersHot.Add(go);
        Resume();
    }

    public void RemoveLaserHot(GameObject go)
    {
        m_lasersHot.Remove(go);
    }

    public void AddLaserCold(GameObject go)
    {
        if(m_lasersCold.Contains(go))
            return;
        m_lasersCold.Add(go);
        Freeze();
    }

    public void RemoveLaserCold(GameObject go)
    {
        m_lasersCold.Remove(go);
        Resume();
    }
}
