using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Spine;
using Spine.Unity;
using TMPro;
using DG.Tweening;




public class ObjectAdsFree : MonoBehaviour
{
    enum Type { 
        Heart= 0,
        Coin= 1,
        Magnet =2

    }
   
    [SerializeField] private Type m_spriteType = Type.Heart;
    [SerializeField] private int m_quantity = 1;

    [SerializeField] private GameObject m_textCointEffect;

  
    private TextMeshPro m_text;
 
    private SkeletonAnimation m_spine;
    // Start is called before the first frame update
    void Start()
    {

        m_spine = transform.GetChild(0).GetComponent<SkeletonAnimation>();
        m_text = transform.GetChild(1).GetComponent<TextMeshPro>();
        // m_spine = GetComponentInChildren<GameObject>().
        UpdateText();
        UpdateSkin();
        
    }

    // Update is called once per frame
    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag != GameConstant.TAG_PLAYER)
            return;
        switch(m_spriteType)
        {
            case Type.Heart:
                MainController.UpdateHeart(m_quantity, transform.position);               
                break;
            case Type.Coin:
                GameController.UpdateCoin(m_quantity);
                ShowCoinEffect();
                break;
            case Type.Magnet:              
                GameController.ActiveMagnet();
                break;
        }        
        Destroy(gameObject);
    }

    public void UpdateSkin()
    {
        switch (m_spriteType)
        {
            case Type.Heart:
                m_spine.SetSkin("heart");
                break;
            case Type.Coin:
                m_spine.SetSkin("coin");
                break;
            case Type.Magnet:
                m_spine.SetSkin("magnet");
                break;
        }
    }

    public void UpdateText()
    {
        switch (m_spriteType)
        {
            case Type.Heart:
                m_text.text = m_quantity < 1 ? "+0" : "+" + m_quantity;
                break;
            case Type.Coin:
                m_text.text = m_quantity < 1 ? "+0" : "+" + m_quantity*GameConstant.COIN_RATIO;
                break;
            case Type.Magnet:
                m_text.text = "";
                break;
        }
    }

    void ShowCoinEffect()
    {
        TextMeshPro text = m_textCointEffect.GetComponent<TextMeshPro>();
        text.text = "+" + m_quantity*GameConstant.COIN_RATIO;
        m_textCointEffect.SetActive(true);
        m_textCointEffect.transform.SetParent(transform.parent, true);
        m_textCointEffect.transform.localEulerAngles = Vector3.zero;
        m_textCointEffect.transform.DOMoveY(transform.position.y + 1.5f, 0.5f).OnComplete(()=>{
            text.DOFade(0, 0.5f).OnComplete(()=>{
                m_textCointEffect.transform.DOKill();
                text.DOKill();
                Destroy(m_textCointEffect);
            });
        });
    }   
}
