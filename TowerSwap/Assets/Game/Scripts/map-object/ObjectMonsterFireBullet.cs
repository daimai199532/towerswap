﻿
using Spine;
using Spine.Unity;
using UnityEngine;

public class ObjectMonsterFireBullet : MonoBehaviour
{
    [SerializeField] private Transform m_shootPoint;
    [SerializeField] private SkeletonAnimation m_spine;

    private bool m_dead = false;
    private float m_startTime;
    private int m_damage;
    private float m_bulletSpeed;
    private int m_direction;
    private Vector3 m_startPos;
    private Vector3 m_scale;

    void Awake()
    {
        m_spine.AnimationState.Complete += OnAnimComplete;
        m_spine.AnimationState.SetAnimation(0, "bullet", true);
        SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_ENEMI_SHOOT, 10, false, transform.position);
    }

    public void Init(int damage, float bulletSpeed, int direction)//direction 0 = x, 1 = y
    {
        m_dead = false;
        m_startTime = Time.time;
        m_damage = damage;
        m_bulletSpeed = bulletSpeed;
        m_direction = direction;
        m_startPos = m_shootPoint.position;
        transform.position = m_startPos;
        m_scale = m_shootPoint.parent.localScale;
    }

    void OnDestroy()
    {
        m_spine.AnimationState.Complete -= OnAnimComplete;
    }

    private void OnAnimComplete(TrackEntry trackentry)
    {
        if(trackentry.Animation.Name != "fx_enemi")
            return;
        Destroy(gameObject);
    }


    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject == null || m_dead)
            return;
        switch (collision.gameObject.tag)
        {
            case GameConstant.TAG_PLAYER:
                StartDestroy();
                GameController.UpdateHealth(-1 * m_damage);
                break;
            case GameConstant.TAG_GROUND:
            case GameConstant.TAG_WALL:
            case GameConstant.TAG_OBJECT_BOX:
                StartDestroy();
                break;
        }
    }

    void StartDestroy()
    {
        SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_BULLET, 10, false, transform.position);
        m_dead = true;
        m_spine.AnimationState.SetAnimation(0, "fx_enemi", false);
        GetComponent<Collider2D>().isTrigger = true;
        GetComponent<Collider2D>().enabled = false;
    }

    void Update()
    {
        if(m_dead)
            return;
        if (m_direction == 0) //move x
        {
            float x = m_startPos.x - (Time.time - m_startTime) * m_bulletSpeed *m_scale.x;
            transform.position = new Vector2(x, m_startPos.y);
        }
        else
        {
            float y = m_startPos.y - (Time.time - m_startTime) * m_bulletSpeed * m_scale.y;
            transform.position = new Vector2(m_startPos.x, y);
        }
    }
}
