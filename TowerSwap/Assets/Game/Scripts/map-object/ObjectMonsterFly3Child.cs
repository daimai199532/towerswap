﻿
using UnityEngine;

public class ObjectMonsterFly3Child : MonoBehaviour
{
    private ObjectMonsterFly3 m_monster;

    void Awake()
    {
        m_monster = gameObject.transform.parent.GetComponent<ObjectMonsterFly3>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_monster == null || m_monster.IsDead())
            return;
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            m_monster.Dead();
        }
    }
}
