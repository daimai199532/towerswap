﻿
using System;
using Spine.Unity;
using UnityEngine;

public class ObjectSprings : MonoBehaviour
{
    private static Action<bool> m_triggerEvent;
    [SerializeField] private float m_force = 4500;
    [SerializeField] private SkeletonAnimation m_animator;

    private bool m_collid;

    void Awake()
    {
        m_triggerEvent += OnTrigger;
    }

    void OnDestroy()
    {
        m_triggerEvent -= OnTrigger;
    }

    private void OnTrigger(bool status)
    {
        m_collid = status;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (m_collid || collision.tag != GameConstant.TAG_PLAYER)
            return;
        m_collid = true;
        m_triggerEvent?.Invoke(m_collid);
        Vector2 force = transform.up*m_force;
        collision.gameObject.GetComponent<CharacterController2D>().AddSpringForce(force);
        SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_SPRINGS, 5, false, transform.position);
        m_animator.AnimationState.SetAnimation(0, "animation", false);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag != GameConstant.TAG_PLAYER)
            return;
        m_collid = false;
        m_triggerEvent?.Invoke(m_collid);
    }
}
