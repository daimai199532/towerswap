﻿
using UnityEngine;

public class ObjectMonsterMoveUpDownChild : MonoBehaviour
{
    private ObjectMonsterMoveUpDown m_monster;

    void Awake()
    {
        m_monster = gameObject.transform.parent.GetComponent<ObjectMonsterMoveUpDown>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_monster == null || m_monster.IsDead())
            return;
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            GameController.UpdateHealth(-1 * m_monster.GetDamage());
        }
    }
}
