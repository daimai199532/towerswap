﻿
using UnityEngine;

public class ObjectSawChild : MonoBehaviour
{
    [SerializeField] private ObjectSaw m_object;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            GameController.UpdateHealth(-1 * m_object.GetDamage());
        }
    }
}
