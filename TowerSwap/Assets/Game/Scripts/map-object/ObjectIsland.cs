﻿
using DG.Tweening;
using UnityEngine;

public class ObjectIsland : MonoBehaviour
{
    enum Type
    {
        Spring = 0,
        Summer = 1,
        Autumn = 2,
        Winter = 3
    }

    [SerializeField] private Vector3[] m_path;
    [SerializeField] private float m_moveTime = 4f;
    [SerializeField] private Type m_spriteType = Type.Spring;

    [SerializeField] private Sprite[] m_sprites;


    void Start()
    {
        if(m_path.Length > 0)
        {
            transform.position = m_path[0];
            transform.DOPath(m_path, m_moveTime).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        }  
    }

    void OnDestroy()
    {
        transform.DOKill();
    }

    public void UpdateSprite()
    {
        SpriteRenderer sprite = GetComponent<SpriteRenderer>();
        sprite.sprite = m_sprites[(int) m_spriteType];
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        if (collider.tag != GameConstant.TAG_PLAYER)
            return;
        Player player = collider.GetComponent<Player>();
        if(player == null)
            return;
        if(collider.transform.parent != player.parent) 
            return;
        collider.transform.SetParent(transform, true);
        collider.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.None;
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        if (collider.tag != GameConstant.TAG_PLAYER || collider.transform.parent != transform)
            return;
        Player player = collider.GetComponent<Player>();
        if(player == null)
            return;
        collider.transform.SetParent(player.parent);
        collider.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.Interpolate;
    }

    public Vector3[] GetPath()
    {
        return m_path;
    }
    public void UpdatePath(Vector3 value, int index)
    {
        m_path[index] = value;
    }

}
