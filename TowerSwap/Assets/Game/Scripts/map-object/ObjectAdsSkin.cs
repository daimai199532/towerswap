﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;

public class ObjectAdsSkin : MonoBehaviour
{
    enum Type
    {
        Skin_1 = 1,
        Skin_2 = 2,
        Skin_3 = 3,
        Skin_4 = 4,
        Skin_5 = 5,
        Skin_6 = 6,
        Skin_7 = 7,
        Skin_8 = 8,
        Skin_9 = 9,
        Skin_10 = 10,
        Skin_11 = 11,
        Skin_12 = 12,
        Skin_13 = 13,
        Skin_14 = 14,
        Skin_15 = 15,
        Skin_16 = 16,
        Skin_17 = 17,
        Skin_18 = 18,
        Skin_19 = 19,
        Skin_20 = 20,
        Skin_21 = 21,
        Skin_22 = 22,
        Skin_23 = 23,
        Skin_24 = 24,
        Skin_25 = 25,
        Skin_26 = 26,
        Skin_27 = 27,
        Skin_28 = 28,
        Skin_29 = 29,
        Skin_30 = 30,
        Skin_31 = 31,
        Skin_32 = 32,
        Skin_33 = 33,
        Skin_34 = 34,
        Skin_35 = 35,
        Skin_36 = 36,
        Skin_37 = 37,
        Skin_38 = 38,
        Skin_39 = 39,
        Skin_40 = 40,
        Skin_41 = 41,
        Skin_42 = 42,
        Skin_43 = 43,
        Skin_44 = 44,
        Skin_45 = 45,
        Skin_46 = 46,
        Skin_47 = 47,
        Skin_48 = 48,
        Skin_49 = 49,
        Skin_50 = 50,
        Skin_51 = 51,
        Skin_52 = 52,
        Skin_53 = 53,
        Skin_54 = 54,
        Skin_55 = 55
    }
    [SerializeField] private Type m_skin = Type.Skin_1;
    [SerializeField] private SkeletonAnimation m_spineRed;
    [SerializeField] private SkeletonAnimation m_spineBlue;

    void Start()
    {
        UpdateSkin();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag != GameConstant.TAG_PLAYER)
            return;
        int skin = (int)m_skin;
        if (Application.isEditor)
        {            
            MainController.TrySkin(skin);
            Destroy(gameObject);
        }
        else
        {
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_GAME_TRY_SKIN, skin, ()=>{
                TrackingManager.TrySkinAdsInGame(skin);
                Destroy(gameObject);
            });  
        }
    }

    public void UpdateSkin()
    {
#if UNITY_EDITOR
        string skin = "" + (int)m_skin;
        m_spineRed.initialSkinName = skin;
        m_spineRed.SetSkin(skin);
        m_spineBlue.initialSkinName = skin;
        m_spineBlue.SetSkin(skin);
#else
        SkinInfo sk = MainModel.skinConfig.GetSkin((int)m_skin);
        m_spineRed.initialSkinName = sk.red;
        m_spineRed.SetSkin(sk.red);
        m_spineBlue.initialSkinName = sk.red;
        m_spineBlue.SetSkin(sk.red);
#endif        
    }
}
