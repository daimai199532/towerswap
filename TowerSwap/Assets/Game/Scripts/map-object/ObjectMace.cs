﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ObjectMace : MonoBehaviour
{
    [SerializeField] private float m_timeWaitStart = 2f;
    [SerializeField] private float m_timeMove = 2f;
    [SerializeField] private float m_timeWaitEnd = 2f;
    [SerializeField] private int m_healthReduce = 20;
    [SerializeField] private Transform m_body;
    [SerializeField] private SpriteRenderer m_line;
    [SerializeField] private LayerMask m_layer;

    private Vector2 m_startPos;
    private Vector2 m_endPos;
    private Tweener m_tween;
    private bool m_shake = false;
    private GameObject m_sound;
    private float m_range;

    void Start()
    {
        m_range = GetComponent<BoxCollider2D>().size.x/2f;
        m_startPos = m_body.position;
        RaycastHit2D ray = Physics2D.Raycast(m_body.position, -m_body.up, Mathf.Infinity, m_layer);
        if(ray.collider != null)
        {
            m_endPos = ray.point;
            StartCoroutine(WaitStart());
        }
    }

    IEnumerator WaitStart()
    {
        yield return new WaitForSeconds(m_timeWaitStart);
        Move();
        if(m_sound == null)
            m_sound = SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_DOOR_SWITCH, m_range, true, transform.position);
    }

    void Move()
    {
        if(m_tween == null)
        {
            m_tween = m_body.DOMove(m_endPos, m_timeMove).SetEase(Ease.InQuint).SetAutoKill(false).OnComplete(()=>{
                if(m_shake)
                    GameController.ShakeCamera();
                StartCoroutine(WaitEnd());
                Destroy(m_sound);
                GameObject go = SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_MACE, m_range, false, transform.position);
                go.transform.position = m_body.position;
            }).OnUpdate(()=>{
                m_line.size = new Vector2(m_line.size.x, Vector2.Distance(m_body.position, m_startPos));
            });
            m_tween.OnRewind(()=>{
                StartCoroutine(WaitStart());
                Destroy(m_sound);
            });
        }
        else
            m_tween.PlayForward();
    }

    IEnumerator WaitEnd()
    {
        yield return new WaitForSeconds(m_timeWaitEnd);
        if(m_tween != null)
        {
            if(m_sound == null)
                m_sound = SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_DOOR_SWITCH, m_range, true, transform.position);
            m_tween.PlayBackwards();
        }
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == GameConstant.TAG_PLAYER)
            m_shake = true;
    }

    public void OnTriggerExit2D(Collider2D collider)
    {
        if(collider.tag == GameConstant.TAG_PLAYER)
            m_shake = false;
    }

    public int GetDamage()
    {
        return m_healthReduce;
    }
}
