﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectBox : MonoBehaviour
{    
    [HideInInspector]
    public Transform parent;

    private Vector3 m_scale;
    void Start()
    {
        parent = transform.parent;
        m_scale = transform.localScale;
    }

    public void Reset()
    {
        transform.SetParent(parent);
        transform.localScale = m_scale;
        GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.Interpolate;
    }
}
