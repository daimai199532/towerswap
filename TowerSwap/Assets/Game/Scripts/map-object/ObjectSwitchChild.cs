﻿
using UnityEngine;

public class ObjectSwitchChild : MonoBehaviour
{    void OnCollisionStay2D(Collision2D collider)
    {
        if (collider.collider.tag != GameConstant.TAG_PLAYER)
            return;
        Player player = collider.collider.GetComponent<Player>();
        if(collider.transform.parent != player.parent) 
            return;
        collider.transform.SetParent(transform, true);
        collider.rigidbody.interpolation = RigidbodyInterpolation2D.None;
    }

    void OnCollisionExit2D(Collision2D collider)
    {
        if (collider.collider.tag != GameConstant.TAG_PLAYER || collider.transform.parent != transform)
            return;
        Player player = collider.collider.GetComponent<Player>();
        collider.transform.SetParent(player.parent);
        collider.rigidbody.interpolation = RigidbodyInterpolation2D.Interpolate;
    }
}
