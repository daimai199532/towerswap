﻿
using UnityEngine;
using DG.Tweening;

public class ObjectElevator : MonoBehaviour
{
    private const float MIN_Y = 0.5f;
    
    [SerializeField] private string m_key = "";
    [SerializeField] private float m_time = 4;
    [SerializeField] private float m_height = 4;
    [SerializeField] private bool m_topToBot = false;
    [SerializeField] private Transform m_floor;
    [SerializeField] private Transform m_gear;

    private bool m_state;
    private GameObject m_sound;

    void Awake()
    {
        GameController.triggerEvent += OnTrigger;
        m_state = false;
    }

    void OnDestroy()
    {
        GameController.triggerEvent -= OnTrigger;
        m_floor.DOKill();
    }

    private void OnTrigger(string key, bool state)
    {
        if (m_key != key || m_state == state)
            return;
        m_state = state;
        if (m_topToBot)
        {
            if (m_state)
                Down();
            else
                Up();
        }
        else
        {
            if (m_state)
                Up();
            else
                Down();
        }
        
    }

    private void Up()
    {
        if(m_sound == null)
            m_sound = SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_ELEVATOR, 10, true, m_floor.position);
        m_floor.DOKill();
        m_gear.DOKill();
        float distance = GetDistanceMove();
        float speed = distance/m_time;
        float delY = Mathf.Abs(m_floor.localPosition.y - MIN_Y);
        float time = m_time - delY/speed;
        m_floor.DOLocalMoveY(MIN_Y + distance, time).SetEase(Ease.Linear).OnComplete(()=>{
            Destroy(m_sound);
            m_gear.DOKill();
        });
        m_gear.DOBlendableLocalRotateBy(new Vector3(0, 0, -360), m_time/3, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
    }

    private void Down()
    {
        if(m_sound == null)
            m_sound = SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_ELEVATOR, 10, true, m_floor.position);
        m_floor.DOKill();
        m_gear.DOKill();
        float distance = GetDistanceMove();
        float speed = distance/m_time;
        float delY = Mathf.Abs(m_floor.localPosition.y - (MIN_Y + distance));
        float time = m_time - delY/speed;
        m_floor.DOLocalMoveY(MIN_Y, time).SetEase(Ease.Linear).OnComplete(()=>{
            Destroy(m_sound);
            m_gear.DOKill();
        });
        m_gear.DOBlendableLocalRotateBy(new Vector3(0, 0, 360), m_time/3, RotateMode.FastBeyond360).SetLoops(-1, LoopType.Incremental).SetEase(Ease.Linear);
    }

    float GetDistanceMove()
    {
        return m_height - 1.1f;
    }

    public float GetHeight()
    {
        return m_height;
    }

    public bool IsTop()
    {
        return m_topToBot;
    }

    public Vector2 GetTopPos()
    {
        return new Vector2(m_floor.localPosition.x, MIN_Y + GetDistanceMove());
    }

    public Vector2 GetBotPos()
    {
        return new Vector2(m_floor.localPosition.x, MIN_Y);
    }

    void Update()
    {
        if(m_sound == null)
            return;
        m_sound.transform.position = m_floor.position;
    }
}
