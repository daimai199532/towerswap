﻿
using System.Collections;
using UnityEngine;

public class ObjectMonsterFollow : MonoBehaviour
{
    [HideInInspector]
    public Transform parent;
    [SerializeField] private LayerMask m_playerLayer;
    [SerializeField] private CircleCollider2D m_collider;
    [SerializeField] private Rigidbody2D m_rigidbody2D;
    [SerializeField] private int m_force = 80;
    [SerializeField] private int m_healthReduce = 20;
    [SerializeField] private float m_timeWaitToHit = 1;

    private Coroutine m_timer;
    private Vector3 m_scale;

    void Start()
    {
        m_timer = null;
        parent = transform.parent;
        m_scale = transform.localScale;
    }

    void FixedUpdate()
    {
        RaycastHit2D raycastLeft = Physics2D.Raycast(m_collider.bounds.center, Vector2.left, Mathf.Infinity, m_playerLayer);
        if (raycastLeft.collider != null)
            m_rigidbody2D.AddForce(Vector2.left*m_force);
        RaycastHit2D raycastRight = Physics2D.Raycast(m_collider.bounds.center, Vector2.right, Mathf.Infinity, m_playerLayer);
        if (raycastRight.collider != null)
            m_rigidbody2D.AddForce(Vector2.right * m_force);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            GameController.UpdateHealth(-1*m_healthReduce);
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.tag == GameConstant.TAG_PLAYER && m_timer == null)
        {
            m_timer = StartCoroutine(ReduceHealth());
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.tag == GameConstant.TAG_PLAYER && m_timer != null)
        {
           StopCoroutine(m_timer);
           m_timer = null;
        }
    }

    IEnumerator ReduceHealth()
    {
        yield return new WaitForSeconds(m_timeWaitToHit);
        GameController.UpdateHealth(-1 * m_healthReduce);
        m_timer = null;
    }

    public void Reset()
    {
        transform.SetParent(parent);
        transform.localScale = m_scale;
        GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.Interpolate;
    }
}
