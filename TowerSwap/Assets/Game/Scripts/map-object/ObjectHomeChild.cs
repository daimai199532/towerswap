﻿
using UnityEngine;

public class ObjectHomeChild : MonoBehaviour
{
    [SerializeField] private ObjectHome m_home;
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.tag != GameConstant.TAG_PLAYER)
            return;
        Player player = collision.GetComponent<Player>();
        if(player == null)
            return;
        if(m_home.GetPlayerType() != player.playerType)
            return;
        ObjectHome.areaEvent?.Invoke(true, player.playerType);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.tag != GameConstant.TAG_PLAYER)
            return;
        Player player = collision.GetComponent<Player>();
        if(player == null)
            return;
        if(m_home.GetPlayerType() != player.playerType)
            return;
        ObjectHome.areaEvent?.Invoke(false, player.playerType);
    }
}
