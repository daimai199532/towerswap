﻿
using Spine.Unity;
using UnityEngine;

public class ObjectStar : MonoBehaviour
{
    [SerializeField] public PlayerType playerType = PlayerType.Red;
    [SerializeField] private GameObject m_effect;

    private bool m_stop;

    void Start()
    {
        //GameController.UpdateTarget(TargetType.Star);
        m_stop = false;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_stop)
            return;
        if(collision.tag != GameConstant.TAG_PLAYER)
            return;
        Player player = collision.GetComponent<Player>();
        if(player.playerType != playerType)
            return;
        SoundManager.PlaySound(GameConstant.AUDIO_EAT_ITEM, false);
        m_stop = true;
        m_effect.transform.SetParent(transform.parent, false);
        m_effect.transform.position = transform.position;
        m_effect.SetActive(true);
        m_effect.GetComponent<SkeletonAnimation>().AnimationState.Complete += (entry) => { Destroy(m_effect); };
        GameController.UpdatePoint(1, playerType);
        Destroy(gameObject);
    }

}
