﻿
using UnityEngine;

public class ObjectMonsterMoveFlatChild : MonoBehaviour
{
    private ObjectMonsterMoveFlat m_monster;

    void Awake()
    {
        m_monster = gameObject.transform.parent.GetComponent<ObjectMonsterMoveFlat>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_monster == null || m_monster.IsDead())
            return;
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            GameController.UpdateHealth(-1 * m_monster.GetDamage());
        }
    }
}
