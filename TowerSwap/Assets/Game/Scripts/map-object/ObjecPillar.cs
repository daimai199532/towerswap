﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjecPillar : MonoBehaviour
{
    [SerializeField] private int m_healthReduce = 20;
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            GameController.UpdateHealth(-1*m_healthReduce);
        }
    }
}
