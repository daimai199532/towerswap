﻿
using System.Collections;
using Spine;
using Spine.Unity;
using UnityEngine;
using Event = Spine.Event;

public class ObjectMonsterFire : MonoBehaviour
{
    [SerializeField] private int m_healthReducee = 20;
    [SerializeField] private int m_damage = 20;
    [SerializeField] private float m_bulletSpeed = 5f;
    [SerializeField] private float m_timeShoot = 2f;
    [SerializeField] private SkeletonAnimation m_spine;
    [SerializeField] private GameObject m_bullet;
    [SerializeField] private GameObject m_effectDead;
    [SerializeField] private GameObject m_body;

    private bool m_dead;

    void Start()
    {
        m_spine.AnimationState.Complete += OnAnimComplete;
        m_spine.AnimationState.Event += OnAnimEvent;
        StartCoroutine(ScheduleShoot());
    }

    void OnDestroy()
    {
        m_spine.AnimationState.Complete -= OnAnimComplete;
        m_spine.AnimationState.Event -= OnAnimEvent;
    }

    private void OnAnimEvent(TrackEntry trackentry, Event e)
    {
        if (trackentry.Animation.Name != "attack")
            return;
        if(e.Data.Name != "attackready")
            return;
        Shoot();
    }

    private void OnAnimComplete(TrackEntry trackentry)
    {
        if(trackentry.Animation.Name != "attack")
            return;
        StartCoroutine(ScheduleShoot());
    }

    public int GetDamage()
    {
        return m_damage;
    }

    public float GetBulletSpeed()
    {
        return this.m_bulletSpeed;
    }

    IEnumerator ScheduleShoot()
    {
        m_spine.AnimationState.SetAnimation(0, "stand", true);
        yield return new WaitForSeconds(m_timeShoot);
        m_spine.AnimationState.SetAnimation(0, "attack", false);
    }

    void Shoot()
    {
        GameObject go = Instantiate(m_bullet);
        go.transform.SetParent(transform.parent, false);
        go.GetComponent<ObjectMonsterFireBullet>().Init(m_damage, m_bulletSpeed, 0);
        go.SetActive(true);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_dead)
            return;
        switch (collision.tag)
        {
            case GameConstant.TAG_PLAYER:
                GameController.UpdateHealth(-1 * m_healthReducee);
                break;
        }
    }

    public bool IsDead()
    {
        return m_dead;
    }

    public void Dead()
    {
        StopAllCoroutines();
        SoundManager.PlaySound3D(GameConstant.AUDIO_BALL_HURT_ENEMI, 10, false, transform.position);
        m_dead = true;
        GetComponent<PointEffector2D>().enabled = false;
        Collider2D collider = GetComponent<Collider2D>();
        collider.enabled = false;
        m_body.SetActive(false);
        m_spine.AnimationState.SetAnimation(0, "dead", false);
        m_spine.AnimationState.Complete += OnDead;
    }

    private void OnDead(TrackEntry trackentry)
    {
        GameObject eff = Instantiate(m_effectDead);
        eff.transform.SetParent(transform.parent, false);
        eff.transform.localPosition = transform.localPosition;
        Destroy(gameObject);
    }
}
