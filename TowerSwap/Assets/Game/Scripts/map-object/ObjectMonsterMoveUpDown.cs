﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ObjectMonsterMoveUpDown : MonoBehaviour
{
    [SerializeField] private float m_range = 10f;
    [SerializeField] private float m_moveTime = 4f;
    [SerializeField] private int m_healthReduce = 20;
    [SerializeField] private Transform m_monster;
    [SerializeField] private SpriteRenderer m_line;

    private bool m_dead;
    private float m_startPosY;

    void Start()
    {
        m_dead = false;
        m_startPosY = m_monster.localPosition.y;
        m_monster.DOLocalMoveY(m_monster.localPosition.y - m_range, m_moveTime).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.OutQuad).OnUpdate(OnMonsterUpdate);
    }

    void OnDestroy()
    {
        m_monster.DOKill();
    }

    public int GetDamage()
    {
        return m_healthReduce;
    }

    public bool IsDead()
    {
        return m_dead;
    }

    private void OnMonsterUpdate()
    {
        float size = Mathf.Abs(m_monster.localPosition.y - m_startPosY);
        m_line.size = new Vector2(m_line.size.x, size+1);
    }
}
