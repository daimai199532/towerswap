﻿
using DG.Tweening;
using UnityEngine;

public class ObjectElevatorPath : MonoBehaviour
{
    [SerializeField] private string m_key = "";
    [SerializeField] private float m_moveTime = 4f;
    [SerializeField] private Vector3[] m_path;

    private bool m_state;  
    private Tweener m_tween;

    void Awake()
    {
        GameController.triggerEvent += OnTrigger;
        m_state = false;
    }

    void Start()
    {
        if(m_path.Length > 0)
        {
            transform.position = m_path[0];            
        }  
    }

    void OnDestroy()
    {
        if(m_tween != null)
            m_tween.Kill();
        transform.DOKill();   
        GameController.triggerEvent -= OnTrigger;     
    }

    private void OnTrigger(string key, bool state)
    {
        if (m_key != key || m_state == state)
            return;
        m_state = state;
        if (m_state)
            Move();
        else
            MoveInvert();        
    }

    void Move()
    {
        if(m_tween == null)
            m_tween = transform.DOPath(m_path, m_moveTime).SetEase(Ease.Linear).SetAutoKill(false);
        else
            m_tween.PlayForward();
    }

    void MoveInvert()
    {
        if(m_tween != null)
            m_tween.PlayBackwards();
    }

    void OnTriggerStay2D(Collider2D collider)
    {
        switch(collider.tag)
        {
            case GameConstant.TAG_PLAYER:
                DoPlayerEnter(collider);
                break;
            case GameConstant.TAG_OBJECT_BOX:
                DoBoxEnter(collider);
                break;
            case GameConstant.TAG_MONSTER_MOVE_GROUND:
                DoMonsterEnter(collider);
                break;
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        switch(collider.tag)
        {
            case GameConstant.TAG_PLAYER:
                DoPlayerExit(collider);
                break;
            case GameConstant.TAG_OBJECT_BOX:
                DoBoxExit(collider);
                break;
            case GameConstant.TAG_MONSTER_MOVE_GROUND:
                DoMonsterExit(collider);
                break;
        }  
    }

    void DoPlayerEnter(Collider2D collider)
    {
        Player comp = collider.GetComponent<Player>();
        if(comp == null)
            return;
        if (collider.transform.parent != comp.parent)
            return;
        collider.transform.SetParent(transform, true);
        collider.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.None;
    }

    void DoBoxEnter(Collider2D collider)
    {
        ObjectBox comp = collider.GetComponent<ObjectBox>();
        if(comp == null)
            return;
        if (collider.transform.parent != comp.parent)
            return;
        collider.transform.SetParent(transform, true);
        collider.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.None;
    }

    void DoMonsterEnter(Collider2D collider)
    {
        ObjectMonsterFollow comp = collider.GetComponent<ObjectMonsterFollow>();
        if(comp == null)
            return;
        if (collider.transform.parent != comp.parent)
            return;
        collider.transform.SetParent(transform, true);
        collider.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.None;
    }

    void DoPlayerExit(Collider2D collider)
    {
        Player comp = collider.GetComponent<Player>();
        if(comp == null)
            return;
        if (collider.transform.parent != transform)
            return;
        comp.Reset();
    }

    void DoBoxExit(Collider2D collider)
    {
        ObjectBox comp = collider.GetComponent<ObjectBox>();
        if(comp == null)
            return;
        if (collider.transform.parent != transform)
            return;
        comp.Reset();
    }

    void DoMonsterExit(Collider2D collider)
    {
        ObjectMonsterFollow comp = collider.GetComponent<ObjectMonsterFollow>();
        if(comp == null)
            return;
        if (collider.transform.parent != transform)
            return;
        comp.Reset();
    }

    public Vector3[] GetPath()
    {
        return m_path;
    }
    public void UpdatePath(Vector3 value, int index)
    {
        m_path[index] = value;
    }
}
