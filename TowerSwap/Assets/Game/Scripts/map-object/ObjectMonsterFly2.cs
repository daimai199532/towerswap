﻿
using DG.Tweening;
using UnityEngine;

public class ObjectMonsterFly2 : MonoBehaviour
{
    [SerializeField] private Vector3[] m_path;
    [SerializeField] private float m_moveTime = 4f;
    [SerializeField] private int m_healthReduce = 20;


    void Start()
    {
        if(m_path.Length > 0)
        {
            transform.position = m_path[0];
            transform.DOPath(m_path, m_moveTime).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        }
    }

    void OnDestroy()
    {
        transform.DOKill();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null)
            return;
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            GameController.UpdateHealth(-1 * m_healthReduce);
        }
    }
    public Vector3[] GetPath()
    {
        return m_path;
    }

    public void UpdatePath(Vector3 value, int index)
    {
        m_path[index] = value;
    }

}
