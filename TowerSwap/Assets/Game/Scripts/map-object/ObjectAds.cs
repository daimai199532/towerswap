﻿
using DG.Tweening;
using TMPro;
using UnityEngine;

public class ObjectAds : MonoBehaviour
{
    enum Type
    {
        Heart = 0,
        Coin = 1,
        Magnet = 2
    }

    [SerializeField] private Type m_spriteType = Type.Heart;
    [SerializeField] private int m_quantity = 1;
    [SerializeField] private SpriteRenderer m_sprite;
    [SerializeField] private TextMeshPro m_text;
    [SerializeField] private Sprite[] m_sprites;
    [SerializeField] private GameObject m_textCointEffect;

    void Start()
    {
        UpdateText();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.tag != GameConstant.TAG_PLAYER)
            return;
        switch (m_spriteType)
        {
            case Type.Heart:
                if (Application.isEditor)
                {                    
                    MainController.UpdateHeart(m_quantity, transform.position);
                    Destroy(gameObject);
                }
                else
                    AdsManager.Instance.ShowVideoReward(GameConstant.ADS_GAME_HEART, m_quantity, ()=>{
                        MainController.UpdateHeart(m_quantity, transform.position);
                        TrackingManager.WatchAdsGameLive();
                        Destroy(gameObject);
                    });
                break;
            case Type.Coin:
                if (Application.isEditor)
                {                    
                    GameController.UpdateCoin(m_quantity);
                    ShowCoinEffect();
                    Destroy(gameObject);

                }
                else
                    AdsManager.Instance.ShowVideoReward(GameConstant.ADS_GAME_COIN, m_quantity, ()=>{
                        ShowCoinEffect();
                        Destroy(gameObject);
                        TrackingManager.WatchAdsGameCoin();
                    });
                break;
            case Type.Magnet:
                if (Application.isEditor)
                {
                    GameController.ActiveMagnet();
                    Destroy(gameObject);
                }
                else
                    AdsManager.Instance.ShowVideoReward(GameConstant.ADS_GAME_MAGNET, m_quantity, ()=>{
                        Destroy(gameObject);
                        TrackingManager.WatchAdsGameMagnet(MainModel.GetMapLevel());
                    });
                break;
        }
    }

    public void UpdateSprite()
    {
        m_sprite.sprite = m_sprites[(int)m_spriteType];
    }

    public void UpdateText()
    {
        switch (m_spriteType)
        {
            case Type.Heart:
                m_text.text = m_quantity < 1 ? "+0" : "+" + m_quantity;
                break;
            case Type.Coin:
                m_text.text = m_quantity < 1 ? "+0" : "+" + m_quantity*GameConstant.COIN_RATIO;
                break;
            case Type.Magnet:
                m_text.text = "";
                break;
        }
    }

    void ShowCoinEffect()
    {
        TextMeshPro text = m_textCointEffect.GetComponent<TextMeshPro>();
        text.text = "+" + m_quantity*GameConstant.COIN_RATIO;
        m_textCointEffect.SetActive(true);
        m_textCointEffect.transform.SetParent(transform.parent, true);
        m_textCointEffect.transform.localEulerAngles = Vector3.zero;
        m_textCointEffect.transform.DOMoveY(transform.position.y + 1.5f, 0.5f).OnComplete(()=>{
            text.DOFade(0, 0.5f).OnComplete(()=>{
                m_textCointEffect.transform.DOKill();
                text.DOKill();
                Destroy(m_textCointEffect);
            });
        });
    }
}
