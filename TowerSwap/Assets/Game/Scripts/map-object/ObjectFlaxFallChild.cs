﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectFlaxFallChild : MonoBehaviour
{
    [SerializeField] private ObjectFlaxFall m_flax;
    [SerializeField] private GameObject m_effectDead;

    private bool m_stop;

    void Start()
    {
        m_stop = false;
    }
    
    public void OnTriggerEnter2D(Collider2D collider)
    {
        if(m_stop || !m_flax.IsFall())
            return;
        m_stop = true;
        switch(collider.tag)
        {
            case GameConstant.TAG_PLAYER:
                GameController.UpdateHealth(-1 * m_flax.GetDamage());
                break;
        }                
        GameObject eff = Instantiate(m_effectDead);
        eff.transform.SetParent(m_flax.transform.parent, false);
        eff.transform.position = transform.position;
        Destroy(m_flax.gameObject);
    }
}
