﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMaceChild : MonoBehaviour
{
    [SerializeField] private ObjectMace m_parent;
    public void OnTriggerEnter2D(Collider2D collider)
    {
        if(collider.tag == GameConstant.TAG_PLAYER)
            GameController.UpdateHealth(-1*m_parent.GetDamage());
    }
}
