﻿
using DG.Tweening;
using Spine;
using Spine.Unity;
using UnityEngine;

public class ObjectMonsterFly1 : MonoBehaviour
{
    [SerializeField] private Vector3[] m_path;
    [SerializeField] private float m_moveTime = 4f;
    [SerializeField] private int m_healthReduce = 20;
    [SerializeField] private GameObject m_body;
    [SerializeField] private SkeletonAnimation m_spine;
    [SerializeField] private GameObject m_effectDead;

    private bool m_dead;

    void Start()
    {
        if(m_path.Length > 0)
        {
            transform.position = m_path[0];
            transform.DOPath(m_path, m_moveTime).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        }
    }

    void OnDestroy()
    {
        transform.DOKill();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_dead)
            return;
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            GameController.UpdateHealth(-1 * m_healthReduce);
        }
    }

    public bool IsDead()
    {
        return m_dead;
    }

    public void Dead()
    {
        SoundManager.PlaySound3D(GameConstant.AUDIO_BALL_HURT_ENEMI, 10, false, transform.position);
        m_dead = true;
        transform.DOKill();
        GetComponent<PointEffector2D>().enabled = false;
        Collider2D collider = GetComponent<Collider2D>();
        collider.enabled = false;
        m_body.SetActive(false);
        m_spine.AnimationState.SetAnimation(0, "dead", false);
        m_spine.AnimationState.Complete += OnDead;
    }

    private void OnDead(TrackEntry trackentry)
    {
        GameObject eff = Instantiate(m_effectDead);
        eff.transform.SetParent(transform.parent, false);
        eff.transform.localPosition = transform.localPosition;
        Destroy(gameObject);
    }

    public Vector3[] GetPath()
    {
        return m_path;
    }

    public void UpdatePath(Vector3 value, int index)
    {
        m_path[index] = value;
    }
}
