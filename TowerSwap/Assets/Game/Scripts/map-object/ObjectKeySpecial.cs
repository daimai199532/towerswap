﻿
using Spine.Unity;
using UnityEngine;

public class ObjectKeySpecial : MonoBehaviour
{
    [SerializeField] private GameObject m_effect;

    private bool m_stop;

    void Start()
    {
        m_stop = false;

    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_stop)
            return;
        if(collision.tag == GameConstant.TAG_PLAYER)
        {
            SoundManager.PlaySound(GameConstant.AUDIO_EAT_ITEM, false);
            m_stop = true;
            m_effect.transform.SetParent(transform.parent, false);
            m_effect.transform.position = transform.position;
            m_effect.SetActive(true);
            m_effect.GetComponent<SkeletonAnimation>().AnimationState.Complete += (entry) => { Destroy(m_effect); };
            GameController.UpdateKeyTreasure(1);
            Destroy(gameObject);
        }
    }
}
