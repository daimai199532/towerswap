﻿
using DG.Tweening;
using UnityEngine;

public class ObjectDoorKey : MonoBehaviour
{
    [SerializeField] private string m_key = "";
    [SerializeField] private Transform m_top;
    [SerializeField] private Transform m_bot;
    [SerializeField] private BoxCollider2D m_topCollider;
    [SerializeField] private BoxCollider2D m_botCollider;

    private bool m_stop;
    private bool m_trigger;
    private BoxCollider2D m_collider;
    
    void Start()
    {
        m_stop = false;
        m_trigger = false;
        m_collider = GetComponent<BoxCollider2D>();
        GameController.triggerEvent += OnOpen;
    }

    void OnDestroy()
    {
        GameController.triggerEvent -= OnOpen;
        transform.DOKill();
    }

    private void OnOpen(string key, bool state)
    {
        if (key != m_key || m_stop)
            return;
        GameObject sound = SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_DOOR_SWITCH, 10, true, transform.position);
        m_stop = true;
        m_top.DOLocalMoveY(2.3f, 0.5f).OnComplete(() =>
        {
            m_topCollider.enabled = false;
            Destroy(sound);
        });
        m_bot.DOLocalMoveY(-2.3f, 0.5f).OnComplete(() =>
        {
            m_botCollider.enabled = false;
            m_collider.enabled = false;
        });
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_stop || m_trigger || !MainModel.HasKey(m_key))
            return;
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            m_trigger = true;
            GameController.ActiveKey(m_key, transform.position);
        }
    }
}
