﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectCarTruckle : MonoBehaviour
{   
    [SerializeField] private Transform m_truckle;
    [SerializeField] private SpriteRenderer m_line;
    [SerializeField] private Transform m_wheelLeft;
    [SerializeField] private Transform m_wheelRight;

    private Vector3 m_target;
    private Vector3 m_startPos;
    private float m_lastPosX;
    private List<Player> m_players;

    void Start()
    {
        m_target = m_truckle.localPosition;
        m_startPos = m_truckle.localPosition;
        m_players = new List<Player>();
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.tag != GameConstant.TAG_PLAYER)
            return;
        Player player = collision.gameObject.GetComponent<Player>();
        if(player == null)
            return;
        m_players.Add(player);
        player.transform.SetParent(transform);
        player.LockMove();
        RegMove();
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if(collision.gameObject.tag != GameConstant.TAG_PLAYER)
            return;
        Player player = collision.gameObject.GetComponent<Player>();
        if(player == null)
            return;
        m_players.Remove(player);
        player.Reset();
        if(m_players.Count < 1)
            UnRegMove();
    }

    void LateUpdate()
    {
        if(!IsActive)
            return;
        m_lastPosX = m_truckle.localPosition.x;
        m_truckle.localPosition = Vector3.Lerp(m_truckle.localPosition, m_target, 0.05f);
        float delX = m_truckle.localPosition.x - m_lastPosX;
        m_wheelLeft.localEulerAngles = Vector3.Lerp(m_wheelLeft.localEulerAngles, new Vector3(0,0, m_wheelLeft.localEulerAngles.z - delX*2000f), 0.05f);
        m_wheelRight.localEulerAngles = Vector3.Lerp(m_wheelRight.localEulerAngles, new Vector3(0,0, m_wheelRight.localEulerAngles.z - delX*2000f), 0.05f);
    }

    void RegMove()
    {
        UnRegMove();
        InputController.leftAction += OnMoveLeft;
        InputController.rightAction += OnMoveRight;
    }

    void UnRegMove()
    {
        InputController.leftAction -= OnMoveLeft;
        InputController.rightAction -= OnMoveRight;
    }

    void OnMoveRight(PlayerType playerType)
    {
        m_target += new Vector3(0.1f, 0);
        CheckTarget();
    }

    
    void OnMoveLeft(PlayerType playerType)
    {
        m_target -= new Vector3(0.1f, 0);
        CheckTarget();
    }

    void CheckTarget()
    {
        if(m_target.x < m_startPos.x)
            m_target = m_startPos;
        if(m_target.x > (m_startPos.x + m_line.size.x - 2.3f))
            m_target = m_startPos + new Vector3(m_line.size.x - 2.3f, 0);
    }

    bool IsActive{
        get{
            foreach(Player p in m_players)
            {
                if(p.IsActive)
                    return true;
            }
            return false;
        }
    }
}
