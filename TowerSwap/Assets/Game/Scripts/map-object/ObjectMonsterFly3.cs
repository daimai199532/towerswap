﻿
using System.Collections;
using DG.Tweening;
using Spine;
using Spine.Unity;
using UnityEngine;
using Event = Spine.Event;

public class ObjectMonsterFly3 : MonoBehaviour
{
    [SerializeField] private Vector3[] m_path;
    [SerializeField] private float m_moveTime = 4f;
    [SerializeField] private int m_healthReduce = 20;
    [SerializeField] private float m_bulletSpeed = 5f;
    [SerializeField] private float m_timeShoot = 2f;
    [SerializeField] private int m_damage = 20;
    [SerializeField] private GameObject m_body;
    [SerializeField] private SkeletonAnimation m_spine;
    [SerializeField] private GameObject m_effectDead;
    [SerializeField] private GameObject m_bullet;

    private bool m_dead;

    void Start()
    {
        m_spine.AnimationState.Complete += OnAnimComplete;
        m_spine.AnimationState.Event += OnAnimEvent;
        StartCoroutine(ScheduleShoot());
        //
        if(m_path.Length > 0)
        {
            transform.position = m_path[0];
            transform.DOPath(m_path, m_moveTime).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        }
    }

    void OnDestroy()
    {
        m_spine.AnimationState.Complete -= OnAnimComplete;
        m_spine.AnimationState.Event -= OnAnimEvent;
        transform.DOKill();
    }

    private void OnAnimEvent(TrackEntry trackentry, Event e)
    {
        if (trackentry.Animation.Name != "attack")
            return;
        if (e.Data.Name != "attackready")
            return;
        Shoot();
    }

    private void OnAnimComplete(TrackEntry trackentry)
    {
        if (trackentry.Animation.Name == "dead")
        {
            Dead();
            return;
        }

        if (trackentry.Animation.Name != "attack")
            return;
        StartCoroutine(ScheduleShoot());
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_dead)
            return;
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            GameController.UpdateHealth(-1 * m_healthReduce);
        }
    }

    public bool IsDead()
    {
        return m_dead;
    }

    public void Dead()
    {
        SoundManager.PlaySound3D(GameConstant.AUDIO_BALL_HURT_ENEMI, 10, false, transform.position);
        m_dead = true;
        transform.DOKill();
        GetComponent<PointEffector2D>().enabled = false;
        GetComponent<EdgeCollider2D>().enabled = false;
        m_body.SetActive(false);
        m_spine.AnimationState.SetAnimation(0, "dead", false);
        PlayDead();
    }

    private void PlayDead()
    {
        GameObject eff = Instantiate(m_effectDead);
        eff.transform.SetParent(transform.parent, false);
        eff.transform.localPosition = transform.localPosition;
        Destroy(gameObject);
    }

    IEnumerator ScheduleShoot()
    {
        m_spine.AnimationState.SetAnimation(0, "idle", true);
        yield return new WaitForSeconds(m_timeShoot);
        m_spine.AnimationState.SetAnimation(0, "attack", false);
    }

    void Shoot()
    {
        GameObject go = Instantiate(m_bullet);
        go.transform.SetParent(transform.parent, false);
        go.GetComponent<ObjectMonsterFireBullet>().Init(m_damage, m_bulletSpeed, 1);
        go.SetActive(true);
    }

    public Vector3[] GetPath()
    {
        return m_path;
    }

    public void UpdatePath(Vector3 value, int index)
    {
        m_path[index] = value;
    }
}
