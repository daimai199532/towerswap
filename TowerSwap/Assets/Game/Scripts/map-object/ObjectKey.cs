﻿
using DG.Tweening;
using Spine.Unity;
using UnityEngine;

public class ObjectKey : MonoBehaviour
{
    [SerializeField] private string m_key = "";
    [SerializeField] private GameObject m_effect;

    private bool m_stop;
    private bool m_active;

    // Start is called before the first frame update
    void Start()
    {
        m_stop = false;
        m_active = false;
        GameController.keyActiveEvent += OnActive;
    }

    void OnDestroy()
    {
        transform.DOKill();
        GameController.keyActiveEvent -= OnActive;
    }

    private void OnActive(string key, Vector2 position)
    {
        if(key != m_key || m_active)
            return;
        m_active = true;
        GameController.RemoveKey(m_key);
        gameObject.transform.position = GamePlayScene.keyTarget.position;
        gameObject.transform.rotation = GamePlayScene.keyTarget.rotation;
        gameObject.SetActive(true);
        transform.DOMove(position, 0.5f).SetEase(Ease.OutQuad).OnComplete(() =>
        {
            GameController.DoTrigger(m_key, true);
            Destroy(gameObject);
        });
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_stop)
            return;
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            SoundManager.PlaySound3D(GameConstant.AUDIO_EAT_ITEM, 5, false, transform.position);
            m_stop = true;
            gameObject.SetActive(false);
            m_effect.transform.SetParent(transform.parent, false);
            m_effect.transform.position = transform.position;
            m_effect.SetActive(true);
            m_effect.GetComponent<SkeletonAnimation>().AnimationState.Complete += (entry) =>
            {
                GameController.PickupKey(m_key);
                Destroy(m_effect);
            };
        }
    }
}
