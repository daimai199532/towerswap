﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMonsterFireChild : MonoBehaviour
{
    private ObjectMonsterFire m_monster;

    void Awake()
    {
        m_monster = gameObject.transform.parent.GetComponent<ObjectMonsterFire>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_monster == null || m_monster.IsDead())
            return;
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            m_monster.Dead();
        }
    }
}
