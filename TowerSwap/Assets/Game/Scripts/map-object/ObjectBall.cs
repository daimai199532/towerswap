﻿
using System;
using Spine;
using Spine.Unity;
using UnityEngine;

public class ObjectBall : MonoBehaviour
{
    [SerializeField] private PlayerType m_playerType = PlayerType.Red;
    [SerializeField] private BoxCollider2D m_body;
    [SerializeField] private BoxCollider2D m_top;
    [SerializeField] private Effector2D m_effector;
    [SerializeField] private SkeletonAnimation m_spine;

    private bool m_dead;

    void Start()
    {
        //GameController.UpdateTarget(TargetType.Ball);
        m_dead = false;
        SoundManager.PlaySound3D(GameConstant.AUDIO_BALL_HELP, 10, false, transform.position);
        TrackEntry entry = m_spine.AnimationState.SetAnimation(0, "help", true);
        entry.Complete += OnAnimComplete;
    }

    private void OnAnimComplete(TrackEntry trackEntry)
    {
        if(trackEntry.Animation.Name != "help")
            return;
        SoundManager.PlaySound3D(GameConstant.AUDIO_BALL_HELP, 10, false, transform.position);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_dead)
            return;
        SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_SAVE_BALL, 5, false, transform.position);
        m_dead = true;
        m_body.gameObject.SetActive(false);
        m_top.enabled = false;
        m_effector.enabled = false;
        //
        GameController.UpdatePoint(1, m_playerType);
        m_spine.AnimationState.SetAnimation(0, "hit", false);
        m_spine.AnimationState.Complete += (entry) =>
        {            
            Destroy(gameObject);
        };        
    }

}
