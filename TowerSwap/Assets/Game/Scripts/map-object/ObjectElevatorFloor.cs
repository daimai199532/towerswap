﻿
using UnityEngine;

public class ObjectElevatorFloor : MonoBehaviour
{
    void OnTriggerStay2D(Collider2D collider)
    {
        switch(collider.tag)
        {
            case GameConstant.TAG_PLAYER:
                DoPlayerEnter(collider);
                break;
            case GameConstant.TAG_OBJECT_BOX:
                DoBoxEnter(collider);
                break;
            case GameConstant.TAG_MONSTER_MOVE_GROUND:
                DoMonsterEnter(collider);
                break;
        }
    }

    void OnTriggerExit2D(Collider2D collider)
    {
        switch(collider.tag)
        {
            case GameConstant.TAG_PLAYER:
                DoPlayerExit(collider);
                break;
            case GameConstant.TAG_OBJECT_BOX:
                DoBoxExit(collider);
                break;
            case GameConstant.TAG_MONSTER_MOVE_GROUND:
                DoMonsterExit(collider);
                break;
        }  
    }

    void DoPlayerEnter(Collider2D collider)
    {
        Player comp = collider.GetComponent<Player>();
        if(comp == null)
            return;
        if (collider.transform.parent != comp.parent)
            return;
        collider.transform.SetParent(transform, true);
        collider.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.None;
    }

    void DoBoxEnter(Collider2D collider)
    {
        ObjectBox comp = collider.GetComponent<ObjectBox>();
        if(comp == null)
            return;
        if (collider.transform.parent != comp.parent)
            return;
        collider.transform.SetParent(transform, true);
        collider.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.None;
    }

    void DoMonsterEnter(Collider2D collider)
    {
        ObjectMonsterFollow comp = collider.GetComponent<ObjectMonsterFollow>();
        if(comp == null)
            return;
        if (collider.transform.parent != comp.parent)
            return;
        collider.transform.SetParent(transform, true);
        collider.GetComponent<Rigidbody2D>().interpolation = RigidbodyInterpolation2D.None;
    }

    void DoPlayerExit(Collider2D collider)
    {
        Player comp = collider.GetComponent<Player>();
        if(comp == null)
            return;
        if (collider.transform.parent != transform)
            return;
        comp.Reset();
    }

    void DoBoxExit(Collider2D collider)
    {
        ObjectBox comp = collider.GetComponent<ObjectBox>();
        if(comp == null)
            return;
        if (collider.transform.parent != transform)
            return;
        comp.Reset();
    }

    void DoMonsterExit(Collider2D collider)
    {
        ObjectMonsterFollow comp = collider.GetComponent<ObjectMonsterFollow>();
        if(comp == null)
            return;
        if (collider.transform.parent != transform)
            return;
        comp.Reset();
    }
}
