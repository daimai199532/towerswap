﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class ObjectTeleport : MonoBehaviour
{
    private static Action<string, ObjectTeleport, GameObject, Vector2> m_trigger;

    [SerializeField] private string m_privateKey = "PRIVATE_KEY";
    [SerializeField] private string m_publicKey = "PUBLIC_KEY";
    [SerializeField] private Transform m_showPoint;
    [SerializeField] private Transform m_startShowPoint;
    [SerializeField] private BoxCollider2D m_safeZone;

    private List<GameObject> m_ignoreObj;
    private List<GameObject> m_growingObj;

    void Start()
    {
        m_trigger += OnTrigger;
        m_ignoreObj = new List<GameObject>();
        m_growingObj = new List<GameObject>();
    }

    void OnDestroy()
    {
        m_trigger -= OnTrigger;
    }

    private void OnTrigger(string key, ObjectTeleport sender, GameObject go, Vector2 velocity)
    {
        if(this == sender || m_privateKey != key || m_growingObj.Contains(go) || m_ignoreObj.Contains(go))
            return;
        m_growingObj.Add(go); 
        Vector2 direction = (m_showPoint.position - m_startShowPoint.position).normalized; 
        Collider2D collider = go.GetComponent<Collider2D>();  
        Vector3 baseScale = go.transform.localScale;
        go.transform.position = m_showPoint.position;
        go.transform.localScale = Vector3.zero;    
        float lastX = 0;
        go.transform.DOScale(baseScale, 0.1f).OnUpdate(()=>{
            float bonusX = collider.bounds.extents.x*direction.x;
            go.transform.position = go.transform.position + new Vector3(bonusX - lastX, 0, 0);
            lastX = bonusX;            
        }).OnComplete(()=>{
            if(collider.bounds.Intersects(m_safeZone.bounds))
                m_ignoreObj.Add(go);
            else
                RemoveIgnore(go);
            if(!collider.bounds.Intersects(sender.m_safeZone.bounds))
                sender.RemoveIgnore(go);
            m_growingObj.Remove(go);
        });
        // if(go.tag == GameConstant.TAG_BULLET)
        // {
        //     direction.y *= -1;
        //     go.GetComponent<ObjectMonsterFireBullet>().Reset(direction);
        // }else
        // {
        //     go.GetComponent<Rigidbody2D>().velocity = velocity.magnitude*direction;
        // }
        go.GetComponent<Rigidbody2D>().velocity = velocity.magnitude*direction;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {    
        if(m_ignoreObj.Contains(collision.gameObject) || m_growingObj.Contains(collision.gameObject))
            return;

        switch(collision.gameObject.tag)
        {
            case GameConstant.TAG_PLAYER:
                Player player = collision.gameObject.GetComponent<Player>();
                if(player == null)
                    return;
                break;
            case GameConstant.TAG_OBJECT_BOX:
                ObjectBox box = collision.gameObject.GetComponent<ObjectBox>();
                if(box == null)
                    return;
                break;
            case GameConstant.TAG_MONSTER_MOVE_GROUND:
                ObjectMonsterFollow monster = collision.gameObject.GetComponent<ObjectMonsterFollow>();
                if(monster == null)
                    return;
                break;
            // case GameConstant.TAG_BULLET:
            //     break;
            default:
                return;
        }
        if(!m_ignoreObj.Contains(collision.gameObject))
            m_ignoreObj.Add(collision.gameObject); 
        m_trigger?.Invoke(m_publicKey, this, collision.gameObject, collision.GetComponent<Rigidbody2D>().velocity);
        //m_ignoreObj.Remove(collision.gameObject);
    }

    public void RemoveIgnore(GameObject go)
    {
        m_ignoreObj.Remove(go);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(m_startShowPoint.position, m_showPoint.position);
    }
}
