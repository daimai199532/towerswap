﻿
using UnityEngine;

public class ObjectFlaxChild : MonoBehaviour
{
    private ObjectFlax m_flax;

    void Awake()
    {
        m_flax = gameObject.transform.parent.GetComponent<ObjectFlax>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject == null || m_flax == null)
            return;
        if (collision.tag == GameConstant.TAG_PLAYER)
        {
            GameController.UpdateHealth(-1 * m_flax.GetDamage());
        }
    }
}
