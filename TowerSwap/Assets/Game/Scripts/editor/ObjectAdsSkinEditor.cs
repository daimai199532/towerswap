﻿

using UnityEditor;


[CustomEditor(typeof(ObjectAdsSkin))]
public class ObjectAdsSkinEditor : Editor
{
    private SerializedProperty m_skin;
    void OnEnable()
    {
        m_skin = serializedObject.FindProperty("m_skin");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_skin);
        serializedObject.ApplyModifiedProperties();
        if (EditorGUI.EndChangeCheck())
            UpdateSkin();
    }

    void UpdateSkin()
    {
        var t = (target as ObjectAdsSkin);
        t.UpdateSkin();
    }
}
