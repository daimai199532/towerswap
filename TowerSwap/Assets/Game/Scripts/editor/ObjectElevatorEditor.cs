﻿
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ObjectElevator))]
public class ObjectElevatorEditor : Editor
{
    private SerializedProperty m_key;
    private SerializedProperty m_time;
    private SerializedProperty m_height;
    private SerializedProperty m_topToBot;

    void OnEnable()
    {
        m_key = serializedObject.FindProperty("m_key");
        m_time = serializedObject.FindProperty("m_time");
        m_height = serializedObject.FindProperty("m_height");
        m_topToBot = serializedObject.FindProperty("m_topToBot");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(m_key);
        EditorGUILayout.PropertyField(m_time);
        serializedObject.ApplyModifiedProperties();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_height);
        serializedObject.ApplyModifiedProperties();
        if (EditorGUI.EndChangeCheck())
            Resize();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_topToBot);
        serializedObject.ApplyModifiedProperties();
        if (EditorGUI.EndChangeCheck())
            UpdateDirection();
    }

    void Resize()
    {
        ObjectElevator t = target as ObjectElevator;        
        SpriteRenderer renderer = t.transform.Find("frame").GetComponent<SpriteRenderer>();
        renderer.size = new Vector2(renderer.size.x, t.GetHeight());
    }

    void UpdateDirection()
    {
        ObjectElevator t = target as ObjectElevator;
        Transform floor = t.transform.Find("floor");
        floor.localPosition = t.IsTop() ? t.GetTopPos() : t.GetBotPos();
    }
}
