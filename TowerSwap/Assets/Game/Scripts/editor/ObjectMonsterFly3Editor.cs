﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ObjectMonsterFly3))]
public class ObjectMonsterFly3Editor : Editor
{
    private SerializedProperty m_path;
    private SerializedProperty m_moveTime;
    private SerializedProperty m_healthReduce;
    private SerializedProperty m_bulletSpeed;
    private SerializedProperty m_timeShoot;
    private SerializedProperty m_damage;

    void OnEnable()
    {
        m_path = serializedObject.FindProperty("m_path");
        m_moveTime = serializedObject.FindProperty("m_moveTime");
        m_healthReduce = serializedObject.FindProperty("m_healthReduce");
        m_bulletSpeed = serializedObject.FindProperty("m_bulletSpeed");
        m_timeShoot = serializedObject.FindProperty("m_timeShoot");
        m_damage = serializedObject.FindProperty("m_damage");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(m_moveTime);
        EditorGUILayout.PropertyField(m_healthReduce);
        EditorGUILayout.PropertyField(m_path);
        EditorGUILayout.PropertyField(m_bulletSpeed);
        EditorGUILayout.PropertyField(m_timeShoot);
        EditorGUILayout.PropertyField(m_damage);
        serializedObject.ApplyModifiedProperties();
    }

    void OnSceneGUI()
    {
        ObjectMonsterFly3 t = (target as ObjectMonsterFly3);
        Vector3[] path = t.GetPath();
        Handles.color = Color.red; 
        Handles.DrawPolyLine(path);
        for(int i = 0; i < path.Length; i++)
        {
            EditorGUI.BeginChangeCheck();
            Vector2 pos = Handles.PositionHandle(path[i], Quaternion.identity);
            if(EditorGUI.EndChangeCheck())
            {                
                Undo.RecordObject(t, "change path");
                t.UpdatePath(pos, i);                
            }
        }
        serializedObject.ApplyModifiedProperties();
    }
}
