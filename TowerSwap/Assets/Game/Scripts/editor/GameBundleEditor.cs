﻿using System;
using System.IO;
using System.Text;
using UnityEditor;
using UnityEngine;

public class GameBundleEditor : EditorWindow
{    private BuildTarget m_buildTarget =  BuildTarget.Android;
    private string m_pathOutput = "";

    // Add menu named "My Window" to the Window menu
    [MenuItem("Window/Game Utility")]
    static void Init()
    {
        // Get existing open window or if none, make a new one:
        GameBundleEditor window = (GameBundleEditor)EditorWindow.GetWindow(typeof(GameBundleEditor));
        window.Show();        
    }

    void OnGUI()
    {   
        EditorGUILayout.BeginHorizontal();
            GUILayout.Label("Build Target:", GUILayout.MaxWidth(100f));        
            m_buildTarget = (BuildTarget) EditorGUILayout.EnumPopup(m_buildTarget);
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
        m_pathOutput = EditorGUILayout.TextField("Output Path", m_pathOutput);
        EditorGUILayout.BeginHorizontal();
            EditorGUILayout.Space();
            if (GUILayout.Button("Browse", GUILayout.MaxWidth(75f)))
                BrowseForFolder();
        EditorGUILayout.EndHorizontal();
        EditorGUILayout.Space();
        if(GUILayout.Button("Encode Resource"))
        {
            EditorApplication.delayCall += ExecuteBuild;            
        }
        EditorGUILayout.Space();
        if(GUILayout.Button("Rollback Resource"))
        {
            EditorApplication.delayCall += RollbackResource;            
        }
    }

    void RollbackResource()
    {
        string sourceDir = Path.Combine(Application.dataPath, "Game/Temp");
        string destDir = Path.Combine(Application.dataPath, "Game/Resources");
        sourceDir = sourceDir.Replace("\\", "/");
        destDir = destDir.Replace("\\", "/");
        //check folder empty
        bool isEmpty = true;     
        foreach (string filePath in Directory.GetFiles(sourceDir, "*.*", SearchOption.AllDirectories))
        {
            if(!filePath.ToLower().Contains(".meta"))
            {
                isEmpty = false;
                break;
            }
        }
        if(isEmpty)
        {
            EditorUtility.DisplayDialog("Warning", "Couldn't find any files", "OK");
            return;
        }
        //clear folder resource
        Debug.Log("Rollback Resource: Clear resources folder");
        if (Directory.Exists(destDir))
        {
            FileUtil.DeleteFileOrDirectory(destDir);
        }
        else
            Directory.CreateDirectory(destDir);
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        //move assets from temp to resource
        Debug.Log("Rollback Resource: Move assets from Temp to Resources");
        foreach (string folderPath in Directory.GetDirectories(sourceDir, "*", SearchOption.TopDirectoryOnly))
        {    
            string realPath = folderPath.Replace("\\", "/");
            string newPath = folderPath.Replace(sourceDir, destDir).Replace("\\", "/");
            FileUtil.MoveFileOrDirectory(realPath, newPath);
        }
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        //clean folder temp
        Debug.Log("Rollback Resource: Clean temp folder");
        FileUtil.DeleteFileOrDirectory(sourceDir);
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        Debug.Log("Rollback Resource: Finish");
    }

    private void ExecuteBuild()
    {
        string tempDir = Path.Combine(Application.dataPath, "Game/Temp");
        string baseDestDir = Path.Combine(Application.dataPath, "Game/Resources");        
        string destDir = Path.Combine(baseDestDir, "bundles");
        tempDir = tempDir.Replace("\\", "/");
        baseDestDir = baseDestDir.Replace("\\", "/");
        destDir = destDir.Replace("\\", "/");
        //
        bool isEmpty = true;     
        foreach (string filePath in Directory.GetFiles(tempDir, "*.*", SearchOption.AllDirectories))
        {
            if(!filePath.ToLower().Contains(".meta"))
            {
                isEmpty = false;
                break;
            }
        }
        //output path   
        Debug.Log("Endcode Resource: Set output path");     
        if (string.IsNullOrEmpty(m_pathOutput))
            BrowseForFolder();
        if (string.IsNullOrEmpty(m_pathOutput)) //in case they hit "cancel" on the open browser
        {
            Debug.LogError("AssetBundle Build: No valid output path for build.");
            return;
        }
        //clear output folder
        DirectoryInfo di = new DirectoryInfo(m_pathOutput);
        foreach (FileInfo file in di.GetFiles())
        {
            file.Delete(); 
        }
        foreach (DirectoryInfo dir in di.GetDirectories())
        {
            dir.Delete(true); 
        }
        //clear temp
        if(isEmpty)
        {
            Debug.Log("Endcode Resource: Clear temp folder");
            if (Directory.Exists(tempDir))
            {
                FileUtil.DeleteFileOrDirectory(tempDir);
            }
            else
                Directory.CreateDirectory(tempDir);
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
            //move resource to temp
            Debug.Log("Endcode Resource: Move resource to temp folder");
            foreach (string folderPath in Directory.GetDirectories(baseDestDir, "*", SearchOption.TopDirectoryOnly))
            {    
                string realPath = folderPath.Replace("\\", "/");
                string newPath = folderPath.Replace(baseDestDir, tempDir).Replace("\\", "/");
                FileUtil.MoveFileOrDirectory(realPath, newPath);
            }
            AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        }        
        //clear resources folder
        Debug.Log("Endcode Resource: Clear resources folder");
        if (Directory.Exists(baseDestDir))
        {
            FileUtil.DeleteFileOrDirectory(baseDestDir);
        }
        else
            Directory.CreateDirectory(baseDestDir);
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        //check output path
        Debug.Log("Endcode Resource: Create output path");
        if (!Directory.Exists(m_pathOutput))
            Directory.CreateDirectory(m_pathOutput);
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        //build bundle
        Debug.Log("Endcode Resource: Build bundles");
        BuildPipeline.BuildAssetBundles(m_pathOutput, BuildAssetBundleOptions.ChunkBasedCompression, m_buildTarget); 
        //copy bundle to resource
        Debug.Log("Endcode Resource: Copy bundle to resource");
        DirectoryCopyBundle(m_pathOutput, destDir);
        //copy avatar
        Debug.Log("Endcode Resource: Copy avatar to resource");
        EncodeAvatar();
        //
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        Debug.Log("Endcode Resource: Finish");
    }

    private void DirectoryCopyBundle(string sourceDirName, string destDirName)
    {
        // If the destination directory doesn't exist, create it.
        if (!Directory.Exists(destDirName))
        {
            Directory.CreateDirectory(destDirName);
        }
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        foreach (string filePath in Directory.GetFiles(sourceDirName, "*.*", SearchOption.AllDirectories))
        {
            var fileName = Path.GetFileName(filePath);
            if(fileName.ToLower().Contains(".manifest"))
                continue;
            string newFilePath = Path.Combine(destDirName, fileName + ".txt");
            EncodeAsset(filePath, newFilePath);
            //File.Copy(filePath, newFilePath, true);
        }        
    }


    void EncodeAsset(string path, string newPath)
    {
        byte[] data = File.ReadAllBytes(path);
        //string strBase64 = Convert.ToBase64String(data);
        //string encodeData = GameUtils.EncryptString(strBase64, GameConstant.DECODE_STRING, 256);
        data = GameUtils.Encrypt(data, GameConstant.ENCRYPT_KEY, GameConstant.ENCRYPT_IV);
        //File.WriteAllText(newPath, encodeData, Encoding.UTF8);
        File.WriteAllBytes(newPath, data);
    }

    void EncodeAvatar()
    {
        string sourceDir = Path.Combine(Application.dataPath, "Game/Temp/avatars");
        string destDir = Path.Combine(Application.dataPath, "Game/Resources/avatars");
        sourceDir = sourceDir.Replace("\\", "/");
        destDir = destDir.Replace("\\", "/");
        if (!Directory.Exists(destDir))
        {
            Directory.CreateDirectory(destDir);
        }
        AssetDatabase.Refresh(ImportAssetOptions.ForceUpdate);
        foreach (string filePath in Directory.GetFiles(sourceDir, "*.*", SearchOption.AllDirectories))
        {
            var fileDirName = Path.GetDirectoryName(filePath).Replace("\\", "/");
            var fileName = Path.GetFileName(filePath);            
            if(fileName.ToLower().Contains(".meta"))
                continue;
            string newFilePath = Path.Combine(fileDirName.Replace(sourceDir, destDir), fileName +".txt");
            EncodeAsset(filePath, newFilePath);
        }
    }

    private void BrowseForFolder()
    {
        var newPath = EditorUtility.OpenFolderPanel("Bundle Folder", m_pathOutput, string.Empty);
        if (!string.IsNullOrEmpty(newPath))
        {            
            var gamePath = System.IO.Path.GetFullPath(".");
            gamePath = gamePath.Replace("\\", "/");
            if (newPath.StartsWith(gamePath) && newPath.Length > gamePath.Length)
                newPath = newPath.Remove(0, gamePath.Length+1);
            m_pathOutput = newPath;
        }
    }
}
