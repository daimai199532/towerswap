﻿
using UnityEditor;

[CustomEditor(typeof(ObjectFan))]
public class ObjectFanEditor : Editor
{
    private SerializedProperty m_height;

    void OnEnable()
    {
        m_height = serializedObject.FindProperty("m_height");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_height);
        serializedObject.ApplyModifiedProperties();
        if (EditorGUI.EndChangeCheck())
            ResizeHeight();
    }

    void ResizeHeight()
    {
        var t = (target as ObjectFan);
        t.UpdateHeight();
    }
}
