﻿
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ObjectIsland))]
public class ObjectIslandEditor : Editor
{
    private SerializedProperty m_path;
    private SerializedProperty m_spriteType;
    private SerializedProperty m_moveTime;

    void OnEnable()
    {
        m_path = serializedObject.FindProperty("m_path");
        m_spriteType = serializedObject.FindProperty("m_spriteType");
        m_moveTime = serializedObject.FindProperty("m_moveTime");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(m_path);
        EditorGUILayout.PropertyField(m_moveTime);
        serializedObject.ApplyModifiedProperties();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_spriteType);
        serializedObject.ApplyModifiedProperties();
        if (EditorGUI.EndChangeCheck())
            UpdateSprite();
    }

    void OnSceneGUI()
    {
        ObjectIsland t = (target as ObjectIsland);
        if(t == null)
            return;
        Vector3[] path = t.GetPath();
        Handles.color = Color.red; 
        Handles.DrawPolyLine(path);
        for(int i = 0; i < path.Length; i++)
        {
            EditorGUI.BeginChangeCheck();
            Vector2 pos = Handles.PositionHandle(path[i], Quaternion.identity);
            if(EditorGUI.EndChangeCheck())
            {                
                Undo.RecordObject(t, "change path");
                t.UpdatePath(pos, i);                
            }
        }
        serializedObject.ApplyModifiedProperties();
    }

    void UpdateSprite()
    {
        var t = (target as ObjectIsland);
        t.UpdateSprite();
    }
}
