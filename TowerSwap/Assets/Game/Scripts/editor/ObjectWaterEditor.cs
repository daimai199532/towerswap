﻿
using UnityEditor;

[CustomEditor(typeof(ObjectWater))]
public class ObjectWaterEditor : Editor
{
    private SerializedProperty m_width;
    private SerializedProperty m_animSpeed;
    private SerializedProperty m_noticeHeight;
    private SerializedProperty m_healthReduce;
    private SerializedProperty m_type;
    private SerializedProperty m_force;

    void OnEnable()
    {
        m_width = serializedObject.FindProperty("m_width");
        m_noticeHeight = serializedObject.FindProperty("m_noticeHeight");
        m_healthReduce = serializedObject.FindProperty("m_healthReduce");
        m_type = serializedObject.FindProperty("m_type");
        m_animSpeed = serializedObject.FindProperty("m_animSpeed");
        m_force = serializedObject.FindProperty("m_force");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(m_animSpeed);
        EditorGUILayout.PropertyField(m_type);
        EditorGUILayout.PropertyField(m_healthReduce);    
        EditorGUILayout.PropertyField(m_force);       
        serializedObject.ApplyModifiedProperties();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_width);
        serializedObject.ApplyModifiedProperties();
        if (EditorGUI.EndChangeCheck())
            ResizeWidth();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_noticeHeight);
        serializedObject.ApplyModifiedProperties();
        if (EditorGUI.EndChangeCheck())
            UpdateNotice();
    }

    void ResizeWidth()
    {
        var t = (target as ObjectWater);
        t.UpdateWidth();
    }

    void UpdateNotice()
    {
        var t = (target as ObjectWater);
        t.UpdateNotice();
    }
}
