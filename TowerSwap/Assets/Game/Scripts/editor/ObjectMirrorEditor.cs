﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ObjectMirror))]
public class ObjectMirrorEditor : Editor
{
    private SerializedProperty m_key;
    private SerializedProperty m_startAngle;
    private SerializedProperty m_maxAngle;
    private SerializedProperty m_rotateSpeed;
    private SerializedProperty m_loop;

    void OnEnable()
    {
        m_key = serializedObject.FindProperty("m_key");
        m_startAngle = serializedObject.FindProperty("m_startAngle");
        m_maxAngle = serializedObject.FindProperty("m_maxAngle");
        m_rotateSpeed = serializedObject.FindProperty("m_rotateSpeed");
        m_loop = serializedObject.FindProperty("m_loop");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(m_key);        
        EditorGUILayout.PropertyField(m_rotateSpeed);
        EditorGUILayout.PropertyField(m_loop);
        EditorGUILayout.PropertyField(m_maxAngle);
        serializedObject.ApplyModifiedProperties();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_startAngle);
        serializedObject.ApplyModifiedProperties();
        if (EditorGUI.EndChangeCheck())
            UpdateRotate();
    }

    void UpdateRotate()
    {
        ObjectMirror t = (target as ObjectMirror);
        if(t == null)
            return;
        t.UpdateRotate();
    }
}
