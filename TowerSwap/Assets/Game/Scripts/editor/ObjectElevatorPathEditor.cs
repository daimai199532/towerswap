﻿
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ObjectElevatorPath))]
public class ObjectElevatorPathEditor : Editor
{
    private SerializedProperty m_key;
    private SerializedProperty m_moveTime;
    private SerializedProperty m_path;   
    

    void OnEnable()
    {
        m_path = serializedObject.FindProperty("m_path");
        m_key = serializedObject.FindProperty("m_key");
        m_moveTime = serializedObject.FindProperty("m_moveTime");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUILayout.PropertyField(m_key);
        EditorGUILayout.PropertyField(m_moveTime);
        EditorGUILayout.PropertyField(m_path);        
        serializedObject.ApplyModifiedProperties();
    }

    void OnSceneGUI()
    {
        ObjectElevatorPath t = (target as ObjectElevatorPath);
        if(t == null)
            return;
        Vector3[] path = t.GetPath();
        if(path == null || path.Length < 1)
            return;
        Handles.color = Color.red; 
        Handles.DrawPolyLine(path);
        for(int i = 0; i < path.Length; i++)
        {
            EditorGUI.BeginChangeCheck();
            Vector2 pos = Handles.PositionHandle(path[i], Quaternion.identity);
            if(EditorGUI.EndChangeCheck())
            {                
                Undo.RecordObject(t, "change path");
                t.UpdatePath(pos, i);                
            }
        }
        serializedObject.ApplyModifiedProperties();
    }
}
