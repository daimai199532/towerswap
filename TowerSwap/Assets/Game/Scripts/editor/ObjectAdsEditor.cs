﻿
using UnityEditor;

[CustomEditor(typeof(ObjectAds))]
public class ObjectAdsEditor : Editor
{
    private SerializedProperty m_spriteType;
    private SerializedProperty m_quantity;

    void OnEnable()
    {
        m_spriteType = serializedObject.FindProperty("m_spriteType");
        m_quantity = serializedObject.FindProperty("m_quantity");
    }

    public override void OnInspectorGUI()
    {
        serializedObject.Update();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_spriteType);
        serializedObject.ApplyModifiedProperties();
        if (EditorGUI.EndChangeCheck())
            UpdateSprite();
        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(m_quantity);
        serializedObject.ApplyModifiedProperties();
        if (EditorGUI.EndChangeCheck())
            UpdateText();
    }

    void UpdateSprite()
    {
        var t = (target as ObjectAds);
        t.UpdateSprite();
    }

    void UpdateText()
    {
        var t = (target as ObjectAds);
        t.UpdateText();
    }
}
