﻿
using System.Collections;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PanelRivive : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_textTime;

    void Start()
    {
        
    }

    void OnEnable()
    {
        AdsManager.Instance.ShowBanner();
    }

    void OnDisable()
    {
        AdsManager.Instance.HideBanner();
    }
    
    void OnDestroy()
    {
        AdsManager.Instance.HideBanner();
    }

    public void SetTime(int time)
    {
        StartCoroutine(CountDown(time));
    }

    public void CloseOnclick()
    {   
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        StopAllCoroutines();
        gameObject.transform.localScale = Vector3.zero;
        StartCoroutine(DelayQuit());
    }

    IEnumerator DelayQuit()
    {
        GameController.FailLevel();
        yield return new WaitForSeconds(0.5f);
        GameController.Quit(QuitGameReason.Fail);
    }

    public void RiviveOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        StopAllCoroutines();
        if(Application.isEditor)
        {
            GameController.Rivive();
            gameObject.SetActive(false);
        }
        else
        {
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_RIVIVE, null, ()=>{
                gameObject.SetActive(false);
            }); 
        }
    }

    IEnumerator CountDown(int time)
    {
        while(time > 0)
        {
            m_textTime.text = "(" + time + ")";
            yield return new WaitForSeconds(1);
            time--;
        }
        GameController.FailLevel();
        GameController.Quit(QuitGameReason.Fail);
    }
}
