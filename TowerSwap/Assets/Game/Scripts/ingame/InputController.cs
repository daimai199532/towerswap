﻿using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputController : MonoBehaviour
{
    public static InputController instance;
    public static Action<PlayerType> leftAction;
    public static Action<PlayerType> rightAction;
    public static Action<bool, PlayerType> jumpAction;
    public static Action<PlayerType> releaseMove;
    public static Action<PlayerType> switchAction;

    [SerializeField] private Button m_buttonLeft;
    [SerializeField] private Button m_buttonRight;
    [SerializeField] private Button m_buttonJump;
    [SerializeField] private Button m_buttonZoom;
    [SerializeField] private Button m_buttonSwitch;
    [SerializeField] private TextMeshProUGUI m_textGuide;
    [SerializeField] private GameObject m_zoomGuide;


    private bool m_stop;
    private bool m_leftEnter;
    private bool m_rightEnter;
    private bool m_jumpEnter;
    private PlayerType m_playerType;

    void Awake()
    {
        if(instance == null)
            instance = this;
        GameController.stopPlayerEvent += Stop;
        GameController.resumePlayerEvent += Resume;
        GameController.levelLoadedEvent += OnLevelReady;
        GameController.readyEvent += OnPlayReady;
        //
        m_playerType = PlayerType.Blue;
        //guide
        bool activeGuide = !MainModel.pressedZoom && MainModel.GetMapLevel() == 1;
        m_textGuide.text = "";//"Press and hold zoom button to have more vision";
        m_textGuide.gameObject.SetActive(activeGuide);
        m_zoomGuide.SetActive(activeGuide);
    }
    void Start()
    {
        m_stop = true;
        m_leftEnter = false;
        m_rightEnter = false;
        m_jumpEnter = false;                
    }
    IEnumerator DelayStart()
    {
        yield return null;
        yield return null;
        SwitchPlayer();
        switchAction?.Invoke(m_playerType);
        if(MainModel.GetMapLevel() < 1)
            gameObject.SetActive(false);
    }

    void OnDestroy()
    {
        GameController.stopPlayerEvent -= Stop;
        GameController.resumePlayerEvent -= Resume;
        GameController.levelLoadedEvent -= OnLevelReady;
        GameController.readyEvent -= OnPlayReady;
    }

    private void OnPlayReady()
    {
        m_stop = false;
    }

    private void OnLevelReady()
    {        
        StartCoroutine(DelayStart());
    }

    public void OnLeftPointerEnter(BaseEventData eventData)
    {
        m_leftEnter = true;
    }

    public void OnRightPointerEnter(BaseEventData eventData)
    {
        m_rightEnter = true;
    }

    public void OnJumpPointerEnter(BaseEventData eventData)
    {
        m_jumpEnter = true;
    }

    public void OnLeftPointerExit(BaseEventData eventData)
    {
        m_leftEnter = false;
    }

    public void OnRightPointerExit(BaseEventData eventData)
    {
        m_rightEnter = false;
    }

    public void OnJumpPointerExit(BaseEventData eventData)
    {
        m_jumpEnter = false;
    }

    void FixedUpdate()
    {
        if (m_stop)
            return;
        if ((Input.touchCount > 0 && m_leftEnter) || (Input.GetKey(KeyCode.LeftArrow) && MainModel.mapLevel > 0))
            leftAction?.Invoke(m_playerType);
        if((Input.touchCount > 0 && m_rightEnter) || (Input.GetKey(KeyCode.RightArrow) && MainModel.mapLevel > 0))
            rightAction?.Invoke(m_playerType);
        if ((Input.touchCount > 0 && m_jumpEnter) || ((Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.UpArrow)) && MainModel.mapLevel > 0))
            jumpAction?.Invoke(true, m_playerType);
        else
            jumpAction?.Invoke(false, m_playerType);
        if(!m_rightEnter && !m_leftEnter && !Input.GetKey(KeyCode.LeftArrow) && !Input.GetKey(KeyCode.RightArrow) && MainModel.mapLevel > 0)
            releaseMove?.Invoke(m_playerType);
        if(Input.GetKeyDown(KeyCode.Tab) && MainModel.mapLevel > 0)
            SwitchOnclick();
    }

    public void Stop()
    {
        m_stop = true;
    }

    public void Resume()
    {
        m_stop = false;
    }

    public void SwitchOnclick()
    {
        if(m_stop)
            return;
        SwitchPlayer();
        switchAction?.Invoke(m_playerType);
    }

    public void OnZoomPointerDown(BaseEventData eventData)
    {
        GameController.ZoomCamera(MapConfig.maxCameraRatio, 1);
        m_textGuide.gameObject.SetActive(false);
        m_zoomGuide.SetActive(false);
        MainModel.pressedZoom = true;
    }
    public void OnZoomPointerUp(BaseEventData eventData)
    {
        GameController.ZoomCamera(-1, 1);
    }

    public void SwitchPlayer()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_SWITCH_CHARACTER, false);
        GameObject red = transform.Find("button_switch/icon-red").gameObject;
        GameObject blue = transform.Find("button_switch/icon-blue").gameObject;
        red.SetActive(false);
        blue.SetActive(false);
        Color color = Color.black;
        switch(m_playerType)
        {
            case PlayerType.Red:
                blue.SetActive(true);
                m_playerType = PlayerType.Blue;
                ColorUtility.TryParseHtmlString("#00FFFF", out color);                
                break;
            case PlayerType.Blue:
                red.SetActive(true);
                m_playerType = PlayerType.Red;
                color = Color.red;
                break;
        }
        color.a = 0.471f;
        ColorBlock cb = m_buttonLeft.colors;
        cb.normalColor = color;
        cb.selectedColor = color;
        m_buttonLeft.colors = cb;
        m_buttonRight.colors = cb;
        m_buttonJump.colors = cb;
        m_buttonZoom.colors = cb;
        m_buttonSwitch.colors = cb;
    }

    public PlayerType GetPlayerType()
    {
        return m_playerType;
    }
}
