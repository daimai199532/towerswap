﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PanelPause : MonoBehaviour
{
    [SerializeField] private Toggle m_music;
    [SerializeField] private Toggle m_sound;
    [SerializeField] private Toggle m_vibration;

    void Start()
    {
        m_music.isOn = SoundManager.IsMusicOn();
        m_sound.isOn = SoundManager.IsSoundOn();
    }

    void OnEnable()
    {
        AdsManager.Instance.ShowBanner();
        Time.timeScale = 0;
        MainModel.paused = true;
    }

    void OnDisable()
    {
        AdsManager.Instance.HideBanner();
        Time.timeScale = 1;
        MainModel.paused = false;
    }

    void OnDestroy()
    {
        AdsManager.Instance.HideBanner();
    }
    
    public void RestartOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        gameObject.SetActive(false);
        GameController.RestartGame();
    }

    public void HomeOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        gameObject.SetActive(false);
        GameController.QuitLevel();        
    }

    public void CloseOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        gameObject.SetActive(false);
    }

    public void SkipLevelOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(Application.isEditor)
        {
            GameController.SkipLevel();
            gameObject.SetActive(false);
        }
        else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_SKIP_LEVEL_INGAME, null, ()=>{
                gameObject.SetActive(false);
            });
    }

    public void MusicOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        SoundManager.MuteMusic(!m_music.isOn);
        m_music.transform.GetChild(0).gameObject.active = !m_music.isOn;
    }

    public void SoundOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        SoundManager.MuteSound(!m_sound.isOn);
        m_sound.transform.GetChild(0).gameObject.active = !m_sound.isOn;
    }

    public void VibrationOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        m_vibration.transform.GetChild(0).gameObject.active = !m_vibration.isOn;
    }

}
