﻿
using UnityEngine;
using DG.Tweening;
using System;
using System.Collections.Generic;

public class CameraMovement : MonoBehaviour
{
    private const float DESIGN_SIZE = 4.21875f;
    [SerializeField] private LayerMask m_borderLayerMask;
    [SerializeField] Transform m_background;

    private Transform m_target;
    public float smoothTime = 0.3F;
    private Vector3 velocity = Vector3.zero;
    private float m_targetY;

    private float m_lockMinX;
    private float m_lockMaxX;
    private float m_lockMinY;
    private float m_lockMaxY;
    private float m_zAxis;
    private Vector2 m_finishPoint;
    private Vector2? m_finishPointRed;
    private Vector2? m_finishPointBlue;
    private bool m_stop;
    private float m_baseSize;
    private object m_tweener;
    private List<float> m_ratios;
    private Camera m_camera;

    void Awake()
    {
        m_camera = GetComponent<Camera>();
        m_stop = false;     
        m_ratios = new List<float>(){1};
        m_lockMaxX = float.MaxValue;
        m_lockMaxY = float.MaxValue;
        m_lockMinX = float.MinValue;
        m_lockMinY = float.MinValue;
        m_baseSize = m_camera.orthographicSize;
        m_zAxis = transform.position.z;
        m_background.localScale = new Vector3(1.4f*m_baseSize/DESIGN_SIZE, 1.4f*m_baseSize/DESIGN_SIZE, 1);
        //
        GameController.finishEvent += OnFinish;
        GameController.updateTargetYCameraEvent += OnUpdateTargetY;
        GameController.zoomCameraEvent += OnZoom;
        GameController.shakeCameraEvent += OnShake;
        GameController.updateCameraTargetEvent += OnUpdateTarget;
    }
    
    void OnDestroy()
    {
        GameController.finishEvent -= OnFinish;
        GameController.updateTargetYCameraEvent -= OnUpdateTargetY;
        GameController.zoomCameraEvent -= OnZoom;
        GameController.shakeCameraEvent -= OnShake;
        GameController.updateCameraTargetEvent -= OnUpdateTarget;
        if(m_tweener != null)
            DOTween.Kill(m_tweener);
    }

    private void OnUpdateTarget(Transform target)
    {
        m_target = target;
    }

    private void OnShake()
    {
        transform.DOShakeRotation(0.2f, 1, 30, 1);
    }

    private void OnZoom(float ratio, float time)
    {    
        if(ratio < 0)
        {
            if(m_ratios.Count > 1)
                m_ratios.RemoveAt(m_ratios.Count-1);
            ratio = m_ratios[m_ratios.Count-1];            
        }else
            m_ratios.Add(ratio);  
        if(m_tweener != null)
            DOTween.Kill(m_tweener);
        float size = m_camera.orthographicSize;
        m_tweener = DOTween.To(()=>size,  x => size = x, ratio*m_baseSize, time).OnUpdate(()=>{
            if(m_camera != null)
                m_camera.orthographicSize = size;
            if(m_background != null)
                m_background.localScale = new Vector3(1.4f*size/DESIGN_SIZE, 1.4f*size/DESIGN_SIZE, 1);
        });        
    }

    private void OnUpdateTargetY(float y)
    {
        m_targetY = y;
    }

    private void OnFinish(Vector2 finishPoint, PlayerType playerType)
    {
        // switch(playerType)
        // {
        //     case PlayerType.Red:
        //         m_finishPointRed = finishPoint;
        //         break;
        //     case PlayerType.Blue:
        //         m_finishPointBlue = finishPoint;
        //         break;
        // }
        // if(m_finishPointBlue != null && m_finishPointRed != null)
        // {
        //     m_finishPoint = new Vector3((m_finishPointRed.Value.x + m_finishPointBlue.Value.x)/2f, (m_finishPointRed.Value.y+m_finishPointBlue.Value.y)/2f);
        //     m_stop = true;
        // }
        m_finishPoint = m_target.position;
        m_stop = true;
    }

    void Update()
    {
        if (m_target == null)
            return;

        float width2 = m_camera.orthographicSize * Screen.width / Screen.height;
        Vector3 targetPosition = m_stop ? new Vector3(m_finishPoint.x, m_finishPoint.y) : m_target.TransformPoint(Vector3.zero);
        targetPosition.z = m_zAxis;
        //down
        RaycastHit2D raycast = Physics2D.Raycast(transform.position, Vector2.down, Mathf.Infinity, m_borderLayerMask);
        if (raycast.collider != null)
            m_lockMinY = raycast.point.y + m_camera.orthographicSize;
        else
            m_lockMinY = float.MinValue;
        //left
        raycast = Physics2D.Raycast(transform.position, Vector2.left, Mathf.Infinity, m_borderLayerMask);
        if (raycast.collider != null)
            m_lockMinX = raycast.point.x + width2;
        else
            m_lockMinX = float.MinValue;
        //top
        raycast = Physics2D.Raycast(transform.position, Vector2.up, Mathf.Infinity, m_borderLayerMask);
        if (raycast.collider != null)
            m_lockMaxY = raycast.point.y - m_camera.orthographicSize;
        else
            m_lockMaxY = float.MaxValue;
        //right
        raycast = Physics2D.Raycast(transform.position, Vector2.right, Mathf.Infinity, m_borderLayerMask);
        if (raycast.collider != null)
            m_lockMaxX = raycast.point.x - width2;
        else
            m_lockMaxX = float.MaxValue;

        if (targetPosition.y < m_targetY)
            m_targetY = targetPosition.y;
        else if ((targetPosition.y - m_targetY) >= 4)
            m_targetY = targetPosition.y;
        targetPosition.y = m_targetY + 1;
        if (targetPosition.y > m_lockMaxY)
            targetPosition.y = m_lockMaxY;
        else if (targetPosition.y < m_lockMinY)
            targetPosition.y = m_lockMinY;
        if (targetPosition.x < m_lockMinX)
            targetPosition.x = m_lockMinX;
        else if (targetPosition.x > m_lockMaxX)
            targetPosition.x = m_lockMaxX;        
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
    }
}
