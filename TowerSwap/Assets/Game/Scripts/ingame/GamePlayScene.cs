﻿
using System;
using DG.Tweening;
using Spine;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GamePlayScene : MonoBehaviour
{
    public static Transform pointTarget;
    public static Transform keyTarget;

    [SerializeField] private GameObject m_panelPause;
    [SerializeField] private GameObject m_panelRivive;
    [SerializeField] private GameObject m_maskObject;
    [SerializeField] private RectTransform m_mask;
    [SerializeField] private Image m_healthProgress;
    [SerializeField] private GameObject m_redPoint;
    [SerializeField] private GameObject m_bluePoint;
    [SerializeField] private SkeletonGraphic m_redEffectPoint;
    [SerializeField] private SkeletonGraphic m_blueEffectPoint;

    //[SerializeField] private Image m_bossTarget;
    [SerializeField] private Transform m_keyTarget;
    [SerializeField] private SkeletonGraphic m_effectKey;
    [SerializeField] private TextMeshProUGUI m_textKey;
    [SerializeField] private TextMeshProUGUI m_textHeart;
    [SerializeField] private GameObject m_noVideoAds;
    [SerializeField] private TextMeshProUGUI m_textLevel;
    [SerializeField] private GameObject m_objectTreasureKeys;
    [SerializeField] private Sprite[] m_spriteKeys;
    [SerializeField] private GameObject m_buttonSkipLevel;

    private GameObject m_player;
    private bool m_ready;
    private int m_keyQuantity;
    private int m_maxPoint;
    private Vector3 m_startPosNoVideo;
    private float m_startXButtonSkipLevel;

    void Awake()
    {
        if (!MainController.ConfigLoaded())
            return;
        //
        GameController.initUIEvent += InitUI;
        GameController.updatePointEvent += OnUpdatePoint;
        GameController.updateHealthEvent += OnUpdateHealth;
        GameController.keyPickupEvent += OnActiveKey;
        GameController.levelLoadedEvent += OnReady;
        GameController.reviveEvent += OnShowRevive;
        GameController.updateTreasureKeyEvent += OnUpdateTreasureKey;
        GameController.showSkipLevelEvent += OnShowSkipLevel;
        GameController.quitEvent += OnShowHome;

        MainController.updateHeartEvent += OnUpdateHeart;
        MainController.noticeEvent += OnNoVideoAds;
        MainController.changeSkinEvent += OnChangeSkin;

        m_effectKey.AnimationState.Complete += ActiveKey;
        //
        m_ready = false;
        keyTarget = m_keyTarget;
        m_keyQuantity = 0;
        m_startPosNoVideo = m_noVideoAds.transform.localPosition;
        m_startXButtonSkipLevel = m_buttonSkipLevel.transform.localPosition.x;
    }

    void Start()
    {
        GameController.Init();
    }

    void OnDestroy()
    {
        GameController.initUIEvent -= InitUI;
        GameController.updatePointEvent -= OnUpdatePoint;
        GameController.updateHealthEvent -= OnUpdateHealth;
        GameController.keyPickupEvent -= OnActiveKey;
        GameController.levelLoadedEvent -= OnReady;
        GameController.reviveEvent -= OnShowRevive;
        GameController.updateTreasureKeyEvent -= OnUpdateTreasureKey;
        GameController.showSkipLevelEvent -= OnShowSkipLevel;
        GameController.quitEvent -= OnShowHome;

        MainController.updateHeartEvent -= OnUpdateHeart;
        MainController.noticeEvent -= OnNoVideoAds;
        MainController.changeSkinEvent -= OnChangeSkin;
        //
        m_buttonSkipLevel.transform.DOKill();
    }    

    void Update()
    {
        if(m_player && !m_ready)
            m_mask.position = m_player.transform.position;
    }
                                 
    private void OnShowHome(QuitGameReason reason)
    {
        SceneOut(reason);
    }

    private void OnShowSkipLevel(bool active)
    {
        m_buttonSkipLevel.transform.DOKill();
        if(active)
            m_buttonSkipLevel.transform.DOLocalMoveX(m_startXButtonSkipLevel - 500, 1);
        else
            m_buttonSkipLevel.transform.DOLocalMoveX(m_startXButtonSkipLevel, 1);
    }

    private void OnUpdateTreasureKey(int value)
    {
        Transform last = null;
        Transform effect = m_objectTreasureKeys.transform.Find("effect");
        Transform keys = m_objectTreasureKeys.transform.Find("keys");
        for(int i = 0; i < keys.childCount; i++)
        {
            Transform t = keys.GetChild(i);
            if(i < value)
            {
                t.GetComponent<Image>().sprite = m_spriteKeys[0];
                last = t;
            }else
                t.GetComponent<Image>().sprite = m_spriteKeys[1];
        }
        if(last != null)
        {
            effect.position = last.position;
            effect.gameObject.SetActive(true);
            SkeletonGraphic anim = effect.GetComponent<SkeletonGraphic>();
            anim.AnimationState.Complete -= OnEffectTreasureKeyComplete;
            anim.AnimationState.Complete += OnEffectTreasureKeyComplete;
            anim.AnimationState.SetAnimation(0, "animation", false);
        }
    }

    private void OnEffectTreasureKeyComplete(TrackEntry trackEntry)
    {
        Transform effect = m_objectTreasureKeys.transform.Find("effect");
        effect.gameObject.SetActive(false);
    }

    private void OnChangeSkin(int oldSkin, int skin)
    {
        // m_avatar.sprite = Resources.Load<Sprite>("avatars/avatar-" + skin);
        // m_avatar.SetNativeSize();
    }
    void OnNoVideoAds(string message)
    {
        m_noVideoAds.transform.DOKill();        
        m_noVideoAds.transform.localPosition = m_startPosNoVideo;
        m_noVideoAds.SetActive(true);
        float y = m_noVideoAds.transform.localPosition.y;
        m_noVideoAds.transform.DOLocalMoveY(y - 400, 0.5f).OnComplete(() =>
        {
            m_noVideoAds.transform.DOLocalMoveY(y, 0.5f).SetDelay(2f).OnComplete(() =>
            {
                m_noVideoAds.SetActive(false);
            });
        });
    }

    private void OnUpdateHeart(int value, Vector3? itemPos)
    {
        if(itemPos == null)
            m_textHeart.text = value.ToString();
        else
        {
            Transform item = m_healthProgress.transform.Find("effect");
            item.position = itemPos.Value;
            item.localScale = Vector3.zero;
            item.gameObject.SetActive(true);            
            item.DOScale(Vector3.one, 0.1f).OnComplete(()=>{
            item.DOLocalMove(Vector3.zero, 0.8f).OnComplete(()=>{                    
                    item.DOScale(new Vector3(0,0,1), 0.5f);
                    item.GetComponent<Image>().DOFade(0, 0.5f).OnComplete(()=>{
                        m_textHeart.text = value.ToString();
                        item.gameObject.SetActive(false);
                    }); 
                }).SetEase(Ease.InCubic);
            });
        }
    }

    private void OnShowRevive(int time)
    {
        m_panelRivive.SetActive(true);
        m_panelRivive.GetComponent<PanelRivive>().SetTime(time);
    }

    private void OnReady()
    {
        Canvas canvas = GetComponent<Canvas>();
        canvas.worldCamera = Camera.main;
        ShowMask();
        if(!MainModel.removeAds)
            AdsManager.Instance.LoadInterstitial();
    }

    void ShowMask()
    {
        GameObject[] players = GameObject.FindGameObjectsWithTag(GameConstant.TAG_PLAYER);
        if(players == null || players.Length < 1)
            return;
        foreach(GameObject p in players)
        {
            Player comp = p.GetComponent<Player>();
            if(comp.playerType == PlayerType.Red)
            {
                m_player = p;
                break;
            }
        }
        if(m_player == null)
        {
            GameController.QuitLevel();
            return;
        }
        SceneIn();
    }

    void InitUI(int maxPoint)
    {
        m_maxPoint = maxPoint;
        m_redPoint.SetActive(false);
        m_bluePoint.SetActive(false);
        m_redEffectPoint.gameObject.SetActive(false);
        m_blueEffectPoint.gameObject.SetActive(false);
        m_keyTarget.gameObject.SetActive(false);
        m_effectKey.gameObject.SetActive(false);
        m_textKey.gameObject.SetActive(false);
        // m_avatar.sprite = Resources.Load<Sprite>("avatars/avatar-" + MainModel.currentSkin);
        // m_avatar.SetNativeSize();
        int level = MainModel.GetMapLevel();
        m_textLevel.text = level < 1 ? "TUTORIAL" : ("LEVEL " + MainModel.GetMapLevel());
    }

    private void OnUpdatePoint(int remainTotalPoint, int remainPoint, PlayerType playerType)
    {
        int current = m_maxPoint-remainTotalPoint;
        current = current < 0 ? 0 : current;
        if(current < 1)
            return;
        switch(playerType)
        {
            case PlayerType.Red:
                m_redEffectPoint.gameObject.SetActive(true);
                m_redEffectPoint.AnimationState.SetAnimation(0, "animation", false);
                m_redEffectPoint.AnimationState.Complete -= OnRedEffectPointComplete;
                m_redEffectPoint.AnimationState.Complete += OnRedEffectPointComplete;
                break;
            case PlayerType.Blue:
                m_blueEffectPoint.gameObject.SetActive(true);
                m_blueEffectPoint.AnimationState.SetAnimation(0, "animation", false);
                m_blueEffectPoint.AnimationState.Complete -= OnBlueEffectPointComplete;
                m_blueEffectPoint.AnimationState.Complete += OnBlueEffectPointComplete;
                break;
        }
    }

    private void OnBlueEffectPointComplete(TrackEntry trackEntry)
    {
        m_bluePoint.SetActive(true);
    }

    private void OnRedEffectPointComplete(TrackEntry trackEntry)
    {
        m_redPoint.SetActive(true);
    }

    private void OnUpdateHealth(float percent)
    {
        m_healthProgress.fillAmount = percent;
    }

    private void OnActiveKey(int count)
    {
        if (m_keyQuantity > count)
        {
            m_keyTarget.gameObject.SetActive(count > 0);
            m_textKey.gameObject.SetActive(count > 1);
            m_textKey.text = "X" + count;
        }
        else
        {
            m_effectKey.gameObject.SetActive(true);
            m_effectKey.AnimationState.SetAnimation(0, "animation", false);
        }
        m_keyQuantity = count;
    }

    void ActiveKey(TrackEntry entry)
    {
        m_effectKey.gameObject.SetActive(false);
        m_keyTarget.gameObject.SetActive(m_keyQuantity > 0);
        m_textKey.gameObject.SetActive(m_keyQuantity > 1);
        m_textKey.text = "X" + m_keyQuantity;
    }

    public void ShowPanelPauseOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        m_panelPause.SetActive(true);
    }

    public void RestartOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        TrackingManager.Replay(MainModel.GetMapLevel());
        GameController.Quit(QuitGameReason.Restart);
    }
    public void SkipLevelOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(Application.isEditor)
            MainController.SkipLevel();
        else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_SKIP_LEVEL);
            TrackingManager.SkipLevel(MainModel.GetMapLevel());
    }

    void SceneIn()
    {
        m_mask.position = m_player.transform.position + new Vector3(0, 1);
        m_mask.sizeDelta = Vector2.zero;
        m_maskObject.SetActive(true);
        m_mask.DOSizeDelta(new Vector2(256, 256), 0.8f).SetEase(Ease.InQuart).OnComplete(()=>{
            m_mask.DOSizeDelta(new Vector2(7000, 7000), 0.5f).SetEase(Ease.InQuart).OnComplete(()=>{
                m_player = null;
                SoundManager.PlaySound(GameConstant.AUDIO_MUSIC_GAME, true, true);
                m_maskObject.SetActive(false);
                m_ready = true;
                GameController.Ready();                
            }).SetDelay(0.5f);
        });
    }

    void SceneOut(QuitGameReason reason)
    {
        m_ready = false;
        GameController.StopPlayer();
        m_panelPause.SetActive(false);
        m_panelRivive.SetActive(false);        
        m_maskObject.SetActive(true);
        m_mask.localPosition = Vector3.zero;
        m_mask.DOSizeDelta(new Vector2(256, 256), 0.8f).SetEase(Ease.OutQuart).OnComplete(()=>{
            m_mask.DOSizeDelta(Vector2.zero, 0.5f).SetEase(Ease.OutQuart).OnComplete(()=>{                
                switch (reason)
                {
                    case QuitGameReason.Back: 
                        AdsManager.Instance.ShowInterstitial("back-home", ()=>{
                            GameAssetManager.api.OpenSceneMain();
                        }); 
                        break;
                    case QuitGameReason.Fail:
                        AdsManager.Instance.ShowInterstitial("fail-level", ()=>{
                            GameAssetManager.api.OpenSceneMain();
                        });  
                        break;
                    case QuitGameReason.Win:
                        AdsManager.Instance.ShowInterstitial("win-level", ()=>{
                            GameAssetManager.api.OpenSceneMain();
                        });  
                        break;
                    case QuitGameReason.Restart:
                        AdsManager.Instance.ShowInterstitial("restart-level", ()=>{
                            SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
                        });                        
                        break;
                    case QuitGameReason.Skip:
                        SceneManager.LoadScene(SceneManager.GetActiveScene().name, LoadSceneMode.Single);
                        break;
                }
            }).SetDelay(0.5f);
        });
    }
}
