﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField] private int m_levelStart = 0;
    [SerializeField] private bool m_loadFromCache = false;
    [SerializeField] private string[] m_levelsSortDefault;

    void Awake()
    {
        GameController.loadMapEvent += OnLoad;
        GameController.finishLevelEvent += OnFinish;
    }

    void OnDestroy()
    {
        GameController.loadMapEvent -= OnLoad;
        GameController.finishLevelEvent -= OnFinish;
    }

    private void OnLoad()
    {
        StartLevel();        
    }

    private void OnFinish()
    {
        if (!m_loadFromCache)
            return;
        //MainModel.SaveMapLevel(m_levelStart);
    }

    private void StartLevel()
    {        
        if (m_loadFromCache)
        {
            m_levelStart = MainModel.GetMapLevel();
        }
        else
        {
            MainModel.mapLevel = m_levelStart;
            MainModel.mapSortOrder = m_levelsSortDefault;
        }        
        int tempLevelStart = m_levelStart;
        int count = 0;
        while(tempLevelStart >= m_levelsSortDefault.Length)
        {
            tempLevelStart -= count == 0 ? m_levelsSortDefault.Length : m_levelsSortDefault.Length - 1;
            count++;
        }
        string levelName = "";
        MainModel.levelResult.realMapLevel = tempLevelStart;
        if(MainModel.mapSortOrder.Length > tempLevelStart)
            levelName = MainModel.mapSortOrder[tempLevelStart];
        else if(m_levelsSortDefault.Length > tempLevelStart)
            levelName = m_levelsSortDefault[tempLevelStart];
        else
            levelName = tempLevelStart + "";
        GameObject go = GameAssetManager.api.GetLevel(levelName);
        if(go == null)
        {
            GameController.QuitLevel();
            return;
        }else
        {
            GameObject level = Instantiate(go);
            level.transform.parent = transform;
            //
            GameController.LevelReady();
        }
        //
        TrackingManager.StartLevel(m_levelStart);
    }

    private class LevelComparer : IComparer<GameObject>
    {
        public int Compare(GameObject x, GameObject y)
        {
            int nameX = int.Parse(x.name.Replace("level-", ""));
            int nameY = int.Parse(y.name.Replace("level-", ""));
            return nameX > nameY ? 1 : -1;
        }
    }
}
