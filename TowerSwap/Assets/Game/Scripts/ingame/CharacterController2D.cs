
using System;
using System.Collections;
using UnityEngine;

public class CharacterController2D : MonoBehaviour
{
    [SerializeField] private float m_torque = 100f;
    [SerializeField] private float m_maxVelocity = 20f;
    [SerializeField] private float m_jumpForce = 60000f;
    [SerializeField] private float m_maxForceVelocityY = 30f;

    [SerializeField] private LayerMask m_groundLayer;
    [SerializeField] private LayerMask m_physicLayer;
    [SerializeField] private CircleCollider2D m_collider;
    [SerializeField] private Rigidbody2D m_rigidbody2D;

    private const float MOVEMENT_SMOOTHING = 0.05f;
    private const float TORQUE_SMOOTHING = 0.1f;

    private float m_currentTorque = 0;
	private Vector3 m_velocity = Vector3.zero;
    private bool m_jumping = false;
    //private bool m_isGrounded = false;
    private bool m_stop = false;
    private State m_state;
    private float m_lastJumpY;
    private PlayerType m_playerType;
    private bool m_active;

    private enum State
    {
        Ground,
        Air,
        Free
    }

    public bool isActive{
        get{
            return m_active;
        }
    }

    public Rigidbody2D Rigidbody{
        get{return m_rigidbody2D;}
    }

	void Awake()
	{
        InputController.leftAction += OnMoveLeft;
        InputController.rightAction += OnMoveRight;
        InputController.jumpAction += OnJump;
        InputController.releaseMove += OnReleaseMove;
        InputController.switchAction += OnSwitch;
        GameController.stopPlayerEvent += Stop;
        GameController.resumePlayerEvent += Resume;
        //
        m_playerType = GetComponent<Player>().playerType;
        m_active = false;
    }

    void OnDestroy()
    {
        InputController.leftAction -= OnMoveLeft;
        InputController.rightAction -= OnMoveRight;
        InputController.jumpAction -= OnJump;
        InputController.releaseMove -= OnReleaseMove;
        InputController.switchAction -= OnSwitch;
        GameController.stopPlayerEvent -= Stop;
        GameController.resumePlayerEvent -= Resume;
    }

    private void OnSwitch(PlayerType playerType)
    {  
        if(playerType != m_playerType)
        {
            m_active = false;
            return;
        }
        m_active = true;
        GameController.UpdateCameraTarget(transform);
    }

    private void OnJump(bool active, PlayerType playerType)
    {
        if(playerType != m_playerType)
            return;
        if(active)
            Jump();
    }

    private void OnMoveRight(PlayerType playerType)
    {        
        if(playerType != m_playerType)
            return;
        Move(1);
    }

    
    private void OnMoveLeft(PlayerType playerType)
    {
        if(playerType != m_playerType)
            return;
        Move(-1);
    }

    private void OnReleaseMove(PlayerType playerType)
    {
        if(playerType != m_playerType)
            return;
        m_currentTorque = 0;
        if (m_state == State.Ground)
            m_velocity = Vector3.zero;
    }

    void FixedUpdate()
    {
        if (m_stop)
            return;
        CheckGrounded();
        //if (!m_isGrounded)
        //{
        //    if (m_rigidbody2D.velocity.y < 0)
        //        m_jumping = false;
        //}
        if (m_state == State.Free)
        {
            if (m_rigidbody2D.velocity.y < 0 || Mathf.Abs(transform.position.y - m_lastJumpY) > 0.1f)
                m_jumping = false;
        }
        //RaycastHit2D raycastDown = Physics2D.Raycast(m_collider.bounds.center, Vector2.down, m_collider.bounds.extents.y + 0.1f, m_physicLayer);
        //if (raycastDown.collider != null)
        //    ChangeForceY(true);
        //else
        //   ChangeForceY(false);
        if(m_rigidbody2D.velocity.y > m_maxForceVelocityY)
            m_rigidbody2D.velocity = new Vector2(m_rigidbody2D.velocity.x, m_maxForceVelocityY);
    }

    public void Move(float move)
    {        
        if (m_state == State.Ground)
        {
            m_currentTorque = Mathf.Lerp(m_currentTorque, m_torque, TORQUE_SMOOTHING);
            m_rigidbody2D.AddTorque(-1 * m_currentTorque * move);
            float vx = m_rigidbody2D.velocity.x + move * 2f;
            if (move > 0)
                vx = Mathf.Min(m_maxVelocity, vx);
            else
                vx = Mathf.Max(-1 * m_maxVelocity, vx);
            m_rigidbody2D.velocity = Vector3.SmoothDamp(m_rigidbody2D.velocity, new Vector2(vx, m_rigidbody2D.velocity.y), ref m_velocity, MOVEMENT_SMOOTHING);
        }
        else if (m_state == State.Air)
        {
            m_currentTorque = Mathf.Lerp(m_currentTorque, m_torque, TORQUE_SMOOTHING);
            m_rigidbody2D.AddTorque(-1 * m_currentTorque * move);
        }
        else
        {
            float vx = m_rigidbody2D.velocity.x + move * 1.5f;
            if (move > 0)
                vx = Mathf.Min(m_maxVelocity*1.5f, vx);
            else
                vx = Mathf.Max(-1 * m_maxVelocity*1.5f, vx);
            m_rigidbody2D.velocity = Vector3.SmoothDamp(m_rigidbody2D.velocity, new Vector2(vx, m_rigidbody2D.velocity.y), ref m_velocity, MOVEMENT_SMOOTHING);
        }
    }

    public void Jump()
    {
        //if (!m_isGrounded || m_jumping)
        //    return;
        // if (m_state != State.Ground)
        if (m_state == State.Free || m_jumping)
        {
            if (m_jumping == true && m_state == State.Ground)
            {
                // m_state = State.Free;
                m_jumping = false;
            }
            return;

        }

        //m_isGrounded = false;
        SoundManager.PlaySound(GameConstant.AUDIO_BALL_JUMP, false);
        m_state = State.Free;
        m_jumping = true;
        m_lastJumpY = transform.position.y;
        m_rigidbody2D.AddForce(new Vector2(0f, m_jumpForce));
        
    }
   
    public void CheckGrounded()
    {
         m_state = State.Free;
        float extraHeightText = 0.045f;
        RaycastHit2D raycast = Physics2D.CircleCast(m_collider.bounds.center, m_collider.bounds.extents.y,  Vector2.down, extraHeightText, m_groundLayer);
        if(raycast.collider != null)
        {
            extraHeightText = 0.25f;
            raycast = Physics2D.Raycast(m_collider.bounds.center, Vector2.down, m_collider.bounds.extents.y + extraHeightText, m_groundLayer);
            bool isValidGround = raycast.collider != null && 
                            !(raycast.collider.tag == GameConstant.TAG_CLOUD && m_rigidbody2D.velocity.y > 0) &&
                            !(raycast.collider.tag == GameConstant.TAG_PRESS && m_rigidbody2D.velocity.y > 0) &&
                            !(raycast.collider.tag == GameConstant.TAG_ELEVATOR && m_rigidbody2D.velocity.y > 0);
            if (isValidGround)
            {
                m_state = State.Ground;
                if(raycast.collider.tag == GameConstant.TAG_GROUND)
                    GameController.SavePoint(transform.position, m_playerType);
            }
            else
            {
                extraHeightText = 0.01f;
                raycast = Physics2D.Raycast(m_collider.bounds.center, Vector2.down, m_collider.bounds.extents.y + extraHeightText, m_physicLayer);
                if (raycast.collider != null)
                    m_state = State.Air;
            }
        }else
        {
            extraHeightText = 0.01f;
            raycast = Physics2D.Raycast(m_collider.bounds.center, Vector2.down, m_collider.bounds.extents.y + extraHeightText, m_physicLayer);
            if (raycast.collider != null)
                m_state = State.Air;
        }

        if ((m_state == State.Ground || m_state == State.Air) && m_active)
        {
            //m_rigidbody2D.velocity = new Vector2(m_rigidbody2D.velocity.x, 0);
            GameController.UpdateTargetYCamera(m_rigidbody2D.transform.position.y);
        }
    }

    public void Stop()
    {
        m_stop = true;
        m_rigidbody2D.simulated = false;
        m_rigidbody2D.velocity = Vector2.zero;
        m_rigidbody2D.angularVelocity = 0;
    }

    public void Resume()
    {
        m_stop = false;
        m_rigidbody2D.simulated = true;
    }

    public void AddSpringForce(Vector2 force)
    {
        m_rigidbody2D.velocity = new Vector2(m_rigidbody2D.velocity.x, 0);
        m_rigidbody2D.AddForce(force, ForceMode2D.Force);
    }

    public void LockMove()
    {
        InputController.leftAction -= OnMoveLeft;
        InputController.rightAction -= OnMoveRight;
    }

    public void UnlockMove()
    {
        LockMove();
        InputController.leftAction += OnMoveLeft;
        InputController.rightAction += OnMoveRight;
    }

    public void Reset()
    {
        m_rigidbody2D.interpolation = RigidbodyInterpolation2D.Interpolate;
    }
}
