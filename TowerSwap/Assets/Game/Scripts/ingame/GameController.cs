﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameController
{
    public static Action levelLoadedEvent;
    public static Action readyEvent;
    public static Action<Vector2, PlayerType> finishEvent;
    public static Action finishLevelEvent;
    public static Action<float> updateTargetYCameraEvent;
    public static Action loadMapEvent;
    public static Action<int> initUIEvent;//<max star>
    public static Action<int, int, PlayerType> updatePointEvent;//<remain total, remain, type>
    public static Action<float> updateHealthEvent;//percent
    public static Action<PlayerType, Vector2> loadSavePointEvent;
    public static Action stopPlayerEvent;
    public static Action resumePlayerEvent;
    public static Action<string, bool> triggerEvent;
    public static Action<int> keyPickupEvent;
    public static Action<string, Vector2> keyActiveEvent;
    public static Action<bool> ballHurtEvent;//<isDead>
    public static Action<int> reviveEvent; //<seconds>
    public static Action<float, float> zoomCameraEvent;//<ratio, time>
    public static Action<Transform> updateCameraTargetEvent;
    public static Action activeMagnetEvent;
    public static Action shakeCameraEvent;
    public static Action<PlayerType> switchPlayerEvent;
    public static Action<int> updateTreasureKeyEvent;
    public static Action<bool> showSkipLevelEvent;
    public static Action<QuitGameReason> quitEvent;

    public static void Init()
    {
        MainModel.InitGameInfo();        
        updateHealthEvent?.Invoke(MainModel.GetRemainHealth());
        loadMapEvent?.Invoke();
        updateTreasureKeyEvent?.Invoke(MainModel.levelTreasureKeys);
    }

    public static void UpdatePoint(int value, PlayerType playerType)
    {
        switch(playerType)
        {
            case PlayerType.Red:
                MainModel.redPoint -= value;
                updatePointEvent?.Invoke(MainModel.GetRemainTotalPoint(), MainModel.redPoint, PlayerType.Red);
                break;
            case PlayerType.Blue:
                MainModel.bluePoint -= value;
                updatePointEvent?.Invoke(MainModel.GetRemainTotalPoint(), MainModel.bluePoint, PlayerType.Blue);
                break;
        }        
    }

    public static void UpdateCoin(int value)
    {
        MainModel.levelCoin += value;
    }

    public static void Finish(Vector3 finishPoint, PlayerType playerType)
    {
        finishEvent?.Invoke(finishPoint, playerType);
    }

    public static void FinishLevel()
    {
        finishLevelEvent?.Invoke();
        StopPlayer();
    }

    public static void SavePoint(Vector2 position, PlayerType playerType)
    {
        switch(playerType)
        {
            case PlayerType.Red:
                MainModel.savePointRed = position;
                break;
            case PlayerType.Blue:
                MainModel.savePointBlue = position;
                break;
        }       
    }

    public static void UpdateHealth(int heath)
    {
        if (MainModel.health <= 0)
            return;
        Player p = GameObject.FindObjectOfType<Player>();
        if (p == null)
            return;
        if(heath > 0 || !p.immortal)
        {
            MainModel.health += heath;
            updateHealthEvent?.Invoke(MainModel.GetRemainHealth());
            ballHurtEvent?.Invoke(MainModel.health <= 0);
        }
      
    }

    public static void RestartLevel()
    {
        StopPlayer();
        if(MainModel.totalHeart > 0)
        {
            MainController.UpdateHeart(-1);
            Rivive();
        }else
            reviveEvent?.Invoke(MainModel.S_TIME_RIVIVE);
    }

    public static void Rivive()
    {
        MainModel.ResetSavePoint();
        updateHealthEvent?.Invoke(MainModel.GetRemainHealth());
        loadSavePointEvent?.Invoke(PlayerType.Red, MainModel.savePointRed);
        loadSavePointEvent?.Invoke(PlayerType.Blue, MainModel.savePointBlue);
        //
        TrackingManager.ReviveLevel(MainModel.GetMapLevel());
    }

    public static void ResumePlayer()
    {
        resumePlayerEvent?.Invoke();
    }

    public static void StopPlayer()
    {
        stopPlayerEvent?.Invoke();
    }

    public static void DoTrigger(string key, bool state)
    {
        triggerEvent?.Invoke(key, state);
    }

    public static void PickupKey(string key)
    {
        MainModel.AddKey(key);
        keyPickupEvent?.Invoke(MainModel.keys.Count);
    }

    public static void ActiveKey(string key, Vector2 position)
    {
        if(!MainModel.HasKey(key))
            return;
        keyActiveEvent?.Invoke(key, position);
    }

    public static void RemoveKey(string key)
    {
        MainModel.RemoveKey(key);
        keyPickupEvent?.Invoke(MainModel.keys.Count);
    }

    public static void LevelReady()
    {
        GameObject[] stars = GameObject.FindGameObjectsWithTag(GameConstant.TAG_STAR);    
        foreach(GameObject go in stars)
        {
            ObjectStar comp = go.GetComponent<ObjectStar>();
            switch(comp.playerType)
            {
                case PlayerType.Red:
                    MainModel.redPoint++;
                    break;
                case PlayerType.Blue:
                    MainModel.bluePoint++;
                    break;
            }
        }
        
        initUIEvent?.Invoke(MainModel.redPoint+MainModel.bluePoint);
        updatePointEvent?.Invoke(MainModel.GetRemainTotalPoint(), MainModel.redPoint, PlayerType.Red);
        updatePointEvent?.Invoke(MainModel.GetRemainTotalPoint(), MainModel.bluePoint, PlayerType.Blue);
        levelLoadedEvent?.Invoke();
        MainController.UpdateHeart(0);
    }

    public static void Ready()
    {
        readyEvent?.Invoke();
    }

    public static void CompleteLevel()
    {
        if(MainModel.levelResult.isComplete)
            return;
        MainModel.SaveMapLevel();
        int level = MainModel.GetMapLevel();
       
        MainModel.levelResult.isComplete = true;
        MainModel.levelResult.oldCoin = MainModel.totalCoin;
        MainModel.levelResult.coin = MainModel.levelCoin;
        MainModel.levelResult.mapLevel = level;
        MainModel.UpdateKeyTreasure(MainModel.levelTreasureKeys - MainModel.keyQuantity);
        //apply coin        
        MainModel.UpdateTotalCoin(MainModel.levelCoin * GameConstant.COIN_RATIO);
        //
        if (level == 1)
        {
         
            Quit(QuitGameReason.Restart);
        }
        else
        {
            Quit(QuitGameReason.Win);
        }
       
        //
        TrackingManager.CompleteLevel(level - 1);

    }

    public static void FailLevel()
    {
        int level = MainModel.GetMapLevel();
        MainModel.levelResult.isComplete = false;
        MainModel.levelResult.oldCoin = MainModel.totalCoin;
        MainModel.levelResult.coin = 0;
        MainModel.levelResult.mapLevel = level;
        TrackingManager.FailLevel(level);
    }

    public static void QuitLevel()
    {
        MainModel.levelResult = null;
        Quit(QuitGameReason.Back);
    }

    public static void UpdateTargetYCamera(float y)
    {
        updateTargetYCameraEvent?.Invoke(y);
    }

    public static void ZoomCamera(float ratio, float time)
    {
        zoomCameraEvent?.Invoke(ratio, time);
    }

    public static void ActiveMagnet()
    {
        activeMagnetEvent?.Invoke();
    }

    public static void ShakeCamera()
    {
        shakeCameraEvent?.Invoke();
    }

    public static void UpdateCameraTarget(Transform target)
    {
        updateCameraTargetEvent?.Invoke(target);
    }

    public static void UpdateKeyTreasure(int value)
    {
        MainModel.levelTreasureKeys += value;
        updateTreasureKeyEvent?.Invoke(MainModel.levelTreasureKeys);

    }

    public static void ShowSkipLevel(bool active)
    {
        showSkipLevelEvent?.Invoke(active);
    }

    public static void Quit(QuitGameReason reason)
    {
        quitEvent?.Invoke(reason);
    }

    public static void SkipLevel()
    {
        MainModel.SaveMapLevel();
        Quit(QuitGameReason.Skip);
    }

    public static void RestartGame()
    {
        MainModel.InitGameInfo();
        Quit(QuitGameReason.Restart);
    }
}
