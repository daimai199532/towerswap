﻿
using System;
using System.Collections;
using DG.Tweening;
using Spine;
using Spine.Unity;
using UnityEngine;

public enum PlayerType
{
    Red,
    Blue
}
public class Player : MonoBehaviour
{
    [HideInInspector]
    public Transform parent;

    private enum PlayerState
    {
        Idle,
        Hurt
    }
    [SerializeField] private float m_immortalTime = 0.5f;
    [SerializeField] public PlayerType playerType = PlayerType.Red;
    [SerializeField] private CharacterController2D m_controller;
    [SerializeField] private SkeletonAnimation m_spine;
    [SerializeField] private GameObject m_effectDead;
    [SerializeField] private GameObject m_effectJum;
    [SerializeField] private GameObject m_magnet;
    [SerializeField] private GameObject m_active;
    [SerializeField] private GameObject m_effectActive;

    private PlayerState m_state;
    private Coroutine m_hurtTimer;

    private bool m_boostDamage;
    private bool m_boostHealth;
    private Vector3 m_finishPoint;
    private float m_startImmortal;
    public CharacterController2D controller
    {
        get{
            return m_controller;
        }
    }
    public bool immortal
    {
        get
        {
            return (Time.time - m_startImmortal) < m_immortalTime;
        }
    }
    void Awake()
    {
        parent = transform.parent;
        //GameController.SavePoint(transform.position);
        //
        GameController.finishEvent += OnFinish;
        GameController.finishLevelEvent += OnFinishLevel;
        GameController.loadSavePointEvent += OnResetSavePoint;
        GameController.ballHurtEvent += OnHurt;
        GameController.activeMagnetEvent += OnActiveMagnet;
        MainController.changeSkinEvent += OnChangeSkin;
        InputController.switchAction += OnSwitch; 
    }

    void Start()
    {
        m_spine.AnimationState.Complete += OnAnimComplete;
        //
        Init();
    }

    void OnDestroy()
    {
        GameController.finishEvent -= OnFinish;
        GameController.finishLevelEvent -= OnFinishLevel;
        GameController.loadSavePointEvent -= OnResetSavePoint;
        GameController.ballHurtEvent -= OnHurt;
        m_spine.AnimationState.Complete -= OnAnimComplete;
        GameController.activeMagnetEvent -= OnActiveMagnet;
        MainController.changeSkinEvent -= OnChangeSkin;
        InputController.switchAction -= OnSwitch; 
        transform.DOKill();
    }

    void Update()
    {
        m_active.transform.rotation = Quaternion.identity;
    }

    private void OnSwitch(PlayerType pType)
    {
        if(playerType != pType)
        {
            GetComponent<Player>().Active(false);
            GetComponent<AudioListener>().enabled = false;
            return;
        }
        GetComponent<AudioListener>().enabled = true;
        Active(true);
    }

    private void OnChangeSkin(int oldSkin, int skin)
    {
        switch(playerType)
        {
            case PlayerType.Red:
                m_spine.SetSkin(MainModel.skinConfig.GetSkin(skin).red);
                break;
            case PlayerType.Blue:
                m_spine.SetSkin(MainModel.skinConfig.GetSkin(skin).blue);
                break;
        }        
    }

    private void OnActiveMagnet()
    {
        m_magnet.SetActive(true);
    }

    private void OnAnimComplete(TrackEntry trackentry)
    {
        switch (trackentry.Animation.Name)
        {
            case GameConstant.PLAYER_ANIMATION_HURT:
                //m_spine.AnimationState.SetAnimation(0, GameConstant.PLAYER_ANIMATION_IDLE_IN_GAME, true);
                break;
        }
    }

    private void OnHurt(bool isDead)
    {        
        m_spine.AnimationState.SetAnimation(0, GameConstant.PLAYER_ANIMATION_HURT, false);
        m_startImmortal = Time.time;
        m_state = PlayerState.Hurt;
        if(m_hurtTimer != null)
            StopCoroutine(m_hurtTimer);
        if (isDead)
        {
            m_magnet.SetActive(false);
            SoundManager.PlaySound3D(GameConstant.AUDIO_OBJECT_BALL_DEAD, 5, false, transform.position);
            GetComponent<CharacterController2D>().Stop();
            GameObject eff = Instantiate(m_effectDead);
            eff.transform.SetParent(transform.parent, false);
            eff.transform.localPosition = transform.localPosition;
            eff.GetComponent<EffectDead>().onComplete += OnDead;
            m_spine.GetComponent<MeshRenderer>().enabled = false;
            m_active.SetActive(false);
        }
        else
        {
            SoundManager.PlaySound3D(GameConstant.AUDIO_BALL_HURT, 5, false, transform.position);
            m_hurtTimer = StartCoroutine(IHurt());
        }
    }

    private void OnDead()
    {
        GameController.StopPlayer();
        StartCoroutine(IDelayDead());
    }

    private void OnResetSavePoint(PlayerType pType, Vector2 position)
    {
        if(pType != playerType)
            return;
        transform.position = position;
        m_spine.GetComponent<MeshRenderer>().enabled = true;
        m_active.SetActive(m_controller.isActive);
        m_spine.AnimationState.SetAnimation(0, GameConstant.PLAYER_ANIMATION_IDLE_IN_GAME, true);
        StartCoroutine(IResume());
    }

    private void OnFinishLevel()
    {
        SoundManager.PlaySound3D(GameConstant.AUDIO_BALL_COMPLETE, 5, false, transform.position);
        m_spine.AnimationState.SetAnimation(0, GameConstant.PLAYER_ANIMATION_LAUGH, true);
        transform.DOMove(m_finishPoint, 0.5f);
        transform.DOLocalRotate(Vector3.zero, 0.5f).OnComplete(OnExit);
    }

    private void OnFinish(Vector2 finishPoint, PlayerType pType)
    {
        if(playerType != pType)
            return;
        m_finishPoint = finishPoint;
    }

    private void OnExit()
    {
        transform.DOScale(Vector3.zero, 0.5f).SetEase(Ease.InBack).OnComplete(GameController.CompleteLevel);
    }

    IEnumerator IResume()
    {
        yield return new WaitForSeconds(1f);
        GameController.ResumePlayer();
    }

    IEnumerator IHurt()
    {
        yield return new WaitForSeconds(2f);
        m_spine.AnimationState.SetAnimation(0, GameConstant.PLAYER_ANIMATION_IDLE_IN_GAME, true);
    }

    IEnumerator IDelayDead()
    {
        yield return new WaitForSeconds(1f);
        if(m_controller.isActive)
            GameController.RestartLevel();
    }

    private void Init()
    {
        m_magnet.SetActive(false);
        OnChangeSkin(-1, MainModel.currentSkin);
        m_spine.AnimationState.SetAnimation(0, GameConstant.PLAYER_ANIMATION_IDLE_IN_GAME, true);
        m_state = PlayerState.Idle;
    }
    
    void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.collider.tag != GameConstant.TAG_GROUND)
            return;
        Vector3 pos = transform.position;
        bool valid = false;
        for (int i = 0; i < collision.contactCount; i++)
        {
            Vector3 temp = collision.GetContact(i).point;
            float delX = Mathf.Abs(temp.x - transform.position.x);
            float delY = Mathf.Abs(temp.y - transform.position.y);
            if(delX > delY)
                continue;
            valid = true;
            if (Mathf.Abs(pos.y) >= Mathf.Abs(temp.y))
                continue;
            pos = temp;
        }
        if(!valid)
            return;
        GameObject go = Instantiate(m_effectJum);
        go.transform.SetParent(transform.parent, false);
        go.transform.position = pos - new Vector3(0, 0.15f);
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.collider.tag != GameConstant.TAG_GROUND)
            return;
        Vector3 pos = transform.position;
        bool valid = false;
        for (int i = 0; i < collision.contactCount; i++)
        {
            Vector3 temp = collision.GetContact(i).point;
            float delX = Mathf.Abs(temp.x - transform.position.x);
            float delY = Mathf.Abs(temp.y - transform.position.y);
            if (delX > delY)
                continue;
            valid = true;
            if (Mathf.Abs(pos.y) >= Mathf.Abs(temp.y))
                continue;
            pos = temp;
        }
        if (!valid)
            return;
        GameObject go = Instantiate(m_effectJum);
        go.transform.SetParent(transform.parent, false);
        go.transform.position = pos - new Vector3(0, 0.15f);
    }

    void Active(bool active)
    {
        m_active.SetActive(active);
        if(active)
        {
            GameObject eff = Instantiate(m_effectActive);
            eff.transform.parent = transform.parent;
            eff.transform.position = m_effectActive.transform.position;
            eff.transform.localEulerAngles = Vector3.zero;
            eff.SetActive(true);
            Destroy(eff, 2);
        }
    }

    public void PlayAnimLaugh()
    {
         m_spine.AnimationState.SetAnimation(0, GameConstant.PLAYER_ANIMATION_LAUGH, true);
    }

    public void PlayAnimIdle()
    {
         m_spine.AnimationState.SetAnimation(0, GameConstant.PLAYER_ANIMATION_IDLE_IN_GAME, true);
    }
    public void LockMove()
    {
        m_controller.LockMove();
    }
    public void Reset()
    {
        transform.SetParent(parent);
        m_controller.UnlockMove();
        m_controller.Reset();
    }

    public bool IsActive{
        get{
            return m_active.activeSelf;
        }
    }

}
