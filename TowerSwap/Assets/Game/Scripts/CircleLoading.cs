﻿
using System.Collections;
using UnityEngine;

public class CircleLoading : MonoBehaviour
{
    [SerializeField] private GameObject m_fader;

    public void Awake()
    {
        MainController.activeLoadingEvent += OnActive;
    }

    void OnDestroy()
    {
        MainController.activeLoadingEvent -= OnActive;
        StopAllCoroutines();
    }

    private void OnActive(bool active)
    {
        m_fader.SetActive(active);
        StopAllCoroutines();
        if(active)
            StartCoroutine(WaitStop());
    }

    IEnumerator WaitStop()
    {
        yield return new WaitForSeconds(5f);
        m_fader.SetActive(false);
        //AdsManager.Instance.Reset();
    }
}
