﻿
using System.Collections;
using UnityEngine;

public class MapConfig : MonoBehaviour
{
    public static float maxCameraRatio;
    public float maxScaleCamera = 2;
    [SerializeField] private float  m_waitFirstTimeSkip = 20f;
    [SerializeField] private float m_waitAfterFirstTimeSkip = 10f;
    [SerializeField] private float m_visibleTimeSkip = -1f;

    void Start()
    {
        maxCameraRatio = maxScaleCamera;
        if(m_waitFirstTimeSkip >= 0)
            StartCoroutine(WaitingToShowSkip());
        else
            GameController.ShowSkipLevel(true);
    }

    void OnDestroy()
    {
        StopAllCoroutines();
    }

    IEnumerator WaitingToShowSkip()
    {
        yield return new WaitForSeconds(m_waitFirstTimeSkip);
        GameController.ShowSkipLevel(true);
        if(m_visibleTimeSkip >= 0)
            StartCoroutine(WaitingHideSkipLevel());
    }

    IEnumerator WaitingHideSkipLevel()
    {
        yield return new WaitForSeconds(m_visibleTimeSkip);
        GameController.ShowSkipLevel(false);
        StartCoroutine(WaitingToShowSkipAfterFirstTime());
    }

    IEnumerator WaitingToShowSkipAfterFirstTime()
    {
        yield return new WaitForSeconds(m_waitAfterFirstTimeSkip);
        GameController.ShowSkipLevel(true);
        if(m_visibleTimeSkip >= 0)
            StartCoroutine(WaitingHideSkipLevel());
    }
}
