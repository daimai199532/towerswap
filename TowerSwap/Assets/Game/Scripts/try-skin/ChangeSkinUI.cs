﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections;
using Random = UnityEngine.Random;

public class ChangeSkinUI : MonoBehaviour
{
    [SerializeField] private TrySkinPremium m_premium;
    [SerializeField] private TrySkinCoin m_coin;
    [SerializeField] private TrySkinRescue m_rescue;

    private List<object> m_skins;
    private int m_index;

    public static bool ischangeSkin = true;


    void Awake()
    {

        MainController.updateStoreSkinEvent += OnUpdate;
        MainController.changeSkinEvent += OnSelect;
        IAPManager.purchaseResultEvent += OnPurchaseResult;
        //
        MainController.trySkinUIEvent += OnTrySkinUI;
    }

    void Start()
    {
        UpdateSkins();
    }

    void OnEnable()
    {
        StopAllCoroutines();
    
        StartCoroutine(ChangeSkinTimeCount());
    }

    void OnDisable()
    {
        StopAllCoroutines();
   
    }

    void OnDestroy()
    {
        MainController.updateStoreSkinEvent -= OnUpdate;
        MainController.changeSkinEvent -= OnSelect;
        IAPManager.purchaseResultEvent -= OnPurchaseResult;
 
        MainController.trySkinUIEvent -= OnTrySkinUI;

        StopAllCoroutines();
    }

    private void OnTrySkinUI(int index)
    {
       
     
        HideAllUI();

        foreach (PremiumSkinConfig skin in MainModel.storeConfig.premium_skins)
        {
            if (index == skin.skin)
            {
                m_premium.gameObject.SetActive(true);
                m_premium.Init(skin, MainModel.subscription || MainModel.unlockSkins.Contains(skin.skin), skin.skin == MainModel.currentSkin);
                break;
            }
 
        }

        foreach (CoinSkinConfig skin in MainModel.storeConfig.coin_skins)
        {
            if (index == skin.skin)
            {
                m_coin.gameObject.SetActive(true);
                m_coin.Init(skin, MainModel.subscription || MainModel.unlockSkins.Contains(skin.skin), skin.skin == MainModel.currentSkin);
                break;
            }

        }

        foreach(RescueSkinConfig skin in MainModel.storeConfig.rescue_skins)
        {
            if (index == skin.skin)
            {
               m_rescue.gameObject.SetActive(true);
                m_rescue.Init(skin, MainModel.subscription || MainModel.unlockSkins.Contains(skin.skin), skin.skin == MainModel.currentSkin, skin.level < MainModel.GetMapLevel());
                break;
            }

        }
        ischangeSkin = true;
    }

    private void OnPurchaseResult(string arg1, bool arg2)
    {
        UpdateSkins();
    }

    private void OnSelect(int arg1, int arg2)
    {
        UpdateSkins();
    }

    private void OnUpdate(int obj)
    {
        UpdateSkins();
    }

    void UpdateSkins()
    {

        m_skins = new List<object>();
        List<object> unlock = new List<object>();
        List<object> locked = new List<object>();
        object current = null;
        foreach (PremiumSkinConfig skin in MainModel.storeConfig.premium_skins)
        {
            if (MainModel.currentSkin == skin.skin)
            {
                current = skin;
                continue;
            }
            if (MainModel.unlockSkins.Contains(skin.skin))
                unlock.Add(skin);
            else
                locked.Add(skin);
        }
        foreach (CoinSkinConfig skin in MainModel.storeConfig.coin_skins)
        {
            if (MainModel.currentSkin == skin.skin)
            {
                current = skin;
                continue;
            }
            if (MainModel.unlockSkins.Contains(skin.skin))
                unlock.Add(skin);
            else
                locked.Add(skin);
        }
        foreach (RescueSkinConfig skin in MainModel.storeConfig.rescue_skins)
        {
            if (MainModel.currentSkin == skin.skin)
            {
                current = skin;
                continue;
            }
            if (MainModel.unlockSkins.Contains(skin.skin))
                unlock.Add(skin);
            else
                locked.Add(skin);
        }
        m_skins.AddRange(locked);
        m_skins.Add(current);
        m_skins.AddRange(unlock);


        m_index = GetCurrentSkinIndex();
       
        ChangeSkin();
    }

    public void LeftOnclick()
    {
        m_index--;
        m_index = m_index < 0 ? m_skins.Count - 1 : m_index;
        ChangeSkin();

        ischangeSkin = false;
    
    }

    public void RightOnclick()
    {
        m_index++;
        m_index = m_index >= m_skins.Count ? 0 : m_index;
        ChangeSkin();

        ischangeSkin = false;

    
    }
   
    private IEnumerator ChangeSkinTimeCount()
    {
      
        yield return new WaitForSeconds(5f);
        if (ischangeSkin == true)
        {
            int rand = Random.RandomRange(1, m_skins.Count);
            m_index = rand;
           // m_index++;
            m_index = m_index >= m_skins.Count ? 0 : m_index;
            ChangeSkinTime();
            StartCoroutine(ChangeSkinTimeCount());
           
        }
        else
        {
            StopCoroutine(ChangeSkinTimeCount());
        }
       

    }


    int GetCurrentSkinIndex()
    {
       
        for (int i = 5; i < m_skins.Count; i++)
        {
            if (m_skins[i] is PremiumSkinConfig)
            {
                if (((PremiumSkinConfig) m_skins[i]).skin == MainModel.currentSkin)
                    return i;
            }

            if (m_skins[i] is CoinSkinConfig)
            {
                if (((CoinSkinConfig)m_skins[i]).skin == MainModel.currentSkin)
                    return i;
            }

            if (m_skins[i] is RescueSkinConfig)
            {
                if (((RescueSkinConfig)m_skins[i]).skin == MainModel.currentSkin)
                    return i;
            }
        }

        return 0;
    }

   void ChangeSkin()         
    {
        object config = m_skins[m_index];
        if (config is PremiumSkinConfig)
        {
            MainController.TrySkinUI(((PremiumSkinConfig)config).skin);
        }

        if (config is CoinSkinConfig)
        {
            MainController.TrySkinUI(((CoinSkinConfig)config).skin);
        }

        if (config is RescueSkinConfig)
        {
            MainController.TrySkinUI(((RescueSkinConfig)config).skin);
        }
    }
    void ChangeSkinTime()       //
    {
        HideAllUI();
        object config = m_skins[m_index];
        if (config is PremiumSkinConfig)
        {
            m_premium.gameObject.SetActive(true);
            m_premium.Init(((PremiumSkinConfig)config), MainModel.subscription || MainModel.unlockSkins.Contains(((PremiumSkinConfig)config).skin), ((PremiumSkinConfig)config).skin == MainModel.currentSkin);
            MainController.TrySkinUI(((PremiumSkinConfig)config).skin);
        }

        if (config is CoinSkinConfig)
        {
            m_coin.gameObject.SetActive(true);
            m_coin.Init(((CoinSkinConfig)config), MainModel.unlockSkins.Contains(((CoinSkinConfig)config).skin), ((CoinSkinConfig)config).skin == MainModel.currentSkin);
            MainController.TrySkinUI(((CoinSkinConfig)config).skin);
        }

        if (config is RescueSkinConfig)
        {
            m_rescue.gameObject.SetActive(true);
            m_rescue.Init(((RescueSkinConfig)config), MainModel.unlockSkins.Contains(((RescueSkinConfig)config).skin), ((RescueSkinConfig)config).skin == MainModel.currentSkin, ((RescueSkinConfig)config).level < MainModel.GetMapLevel());
            MainController.TrySkinUI(((RescueSkinConfig)config).skin);
        }
    }

    void HideAllUI()
    {
        m_premium.gameObject.SetActive(false);
        m_coin.gameObject.SetActive(false);
        m_rescue.gameObject.SetActive(false);

    }
}
