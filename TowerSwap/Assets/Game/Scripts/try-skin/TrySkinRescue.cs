﻿
using Spine.Unity;
using TMPro;
using UnityEngine;

public class TrySkinRescue : MonoBehaviour
{
    [SerializeField] private GameObject m_buttonSelect;
    [SerializeField] private GameObject m_buttonGetFree;
    [SerializeField] private GameObject m_buttonTry;
    [SerializeField] private TextMeshProUGUI m_textUnlock;

    private RescueSkinConfig m_config;

    void Awake()
    {
        MainController.updateStoreSkinEvent += OnUpdate;
        MainController.changeSkinEvent += OnSelect;
    }

    void OnDestroy()
    {
        MainController.updateStoreSkinEvent -= OnUpdate;
        MainController.changeSkinEvent -= OnSelect;
    }

    private void OnUpdate(int skin)
    {
        if (m_config.skin != skin)
            return;
        Init(m_config, MainModel.unlockSkins.Contains(m_config.skin), MainModel.currentSkin == m_config.skin, true);
    }

    private void OnSelect(int oldSkin, int newSkin)
    {
        if (m_config.skin != oldSkin && m_config.skin != newSkin)
            return;
        Init(m_config, MainModel.unlockSkins.Contains(m_config.skin), MainModel.currentSkin == m_config.skin, true);
    }

    public void Init(RescueSkinConfig config, bool active, bool selected, bool unlock)
    {
        m_config = config;
        m_textUnlock.gameObject.SetActive(false);
        m_buttonSelect.SetActive(false);
        m_buttonGetFree.SetActive(false);
        m_buttonTry.SetActive(false);
        if (!selected)
        {
            if(active)
                m_buttonSelect.SetActive(true);
            else
                m_buttonTry.SetActive(true);
        }
        //
        m_textUnlock.text = "UNLOCK\nAT LEVEL " + config.level;
    }

    public void OnClickGetFree()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if (Application.isEditor)
            MainController.BuyCoinSkin(0, m_config.skin);
        else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_GET_SKIN, m_config.skin);
        //
        TrackingManager.WatchAdsUnlockSkin(m_config.skin);
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickTry()
    {
       
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if (Application.isEditor)
        {
            MainController.TrySkin(m_config.skin);
          
        }
        else
        {
           
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_TRY_SKIN, m_config.skin, () => {
                //
              
                TrackingManager.WatchAdMenuTrySkin(m_config.skin);
            });
        }
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickAds()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        AdsManager.Instance.ShowVideoReward(GameConstant.ADS_COINS_500);
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickSelect()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.SelectSkin(m_config.skin);
        ChangeSkinUI.ischangeSkin = false;
    }
}
