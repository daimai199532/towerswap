﻿
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TrySkinCoin : MonoBehaviour
{
    [SerializeField] private GameObject m_buttonSelect;
    [SerializeField] private Button m_buttonBuy;
    [SerializeField] private GameObject m_buttonTry;
    [SerializeField] private TextMeshProUGUI m_textPrice;

    private CoinSkinConfig m_config;

    void Awake()
    {
        MainController.updateStoreSkinEvent += OnUpdate;
        MainController.changeSkinEvent += OnSelect;
    }

    void OnDestroy()
    {
        MainController.updateStoreSkinEvent -= OnUpdate;
        MainController.changeSkinEvent -= OnSelect;
    }

    private void OnUpdate(int skin)
    {
        if (m_config.skin != skin)
            return;
        Init(m_config, MainModel.unlockSkins.Contains(m_config.skin), MainModel.currentSkin == m_config.skin);
    }

    private void OnSelect(int oldSkin, int newSkin)
    {
        if (m_config.skin != oldSkin && m_config.skin != newSkin)
            return;
        
        Init(m_config, MainModel.unlockSkins.Contains(m_config.skin), MainModel.currentSkin == m_config.skin);
    }

    public void Init(CoinSkinConfig config, bool active, bool selected)
    {
        m_config = config;
        m_buttonSelect.SetActive(false);
        m_buttonBuy.gameObject.SetActive(false);
        m_buttonTry.SetActive(false);
        if (!selected)
        {
            if (active)
                m_buttonSelect.SetActive(true);
            else
                m_buttonTry.SetActive(true);
        }
        //
        m_textPrice.text = GameUtils.CoinToString(config.price);
    }

    public void OnClickBuy()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.BuyCoinSkin(m_config.price, m_config.skin);
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickTry()
    {
     
       
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(Application.isEditor)
        {
            MainController.TrySkin(m_config.skin);
       
          
        }   
        else
        {
           
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_TRY_SKIN, m_config.skin, () => {
                //
              
                TrackingManager.WatchAdMenuTrySkin(m_config.skin);
            });
            
        }
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickAds()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        AdsManager.Instance.ShowVideoReward(GameConstant.ADS_COINS_500);
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickSelect()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.SelectSkin(m_config.skin);
        ChangeSkinUI.ischangeSkin = false;
    }
}
