﻿
using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;

public class TrySkinPremium : MonoBehaviour
{
    [SerializeField] private GameObject m_buttonSelect;
    [SerializeField] private Button m_buttonBuy;
    [SerializeField] private GameObject m_buttonTry;

    private PremiumSkinConfig m_config;

    void Awake()
    {
        MainController.updateStoreSkinEvent += OnUpdate;
        MainController.changeSkinEvent += OnSelect;
        IAPManager.purchaseResultEvent += OnPurchaseResult;
    }

    void OnDestroy()
    {
        MainController.updateStoreSkinEvent -= OnUpdate;
        MainController.changeSkinEvent -= OnSelect;
        IAPManager.purchaseResultEvent -= OnPurchaseResult;
    }

    private void OnPurchaseResult(string productId, bool status)
    {
        if (!status || productId != IAPManager.PRODUCT_ID_VIP_SUBSCRIPTION)
            return;
        Init(m_config, MainModel.subscription, MainModel.currentSkin == m_config.skin);
    }

    private void OnUpdate(int skin)
    {
        if (m_config.skin != skin)
            return;
        Init(m_config, MainModel.subscription, MainModel.currentSkin == m_config.skin);
    }

    private void OnSelect(int oldSkin, int newSkin)
    {
        if (m_config.skin != oldSkin && m_config.skin != newSkin)
            return;
        Init(m_config, MainModel.subscription, MainModel.currentSkin == m_config.skin);
        
    }

    public void Init(PremiumSkinConfig config, bool active, bool selected)
    {
        m_config = config;
        m_buttonBuy.gameObject.SetActive(false);
        m_buttonSelect.SetActive(false);
        m_buttonTry.SetActive(false);
        if (!selected)
        {
            if (active)
                m_buttonSelect.SetActive(true);
            else
                m_buttonTry.SetActive(true);
        }
    }

    public void OnClickBuy()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.OpenPopup(PopupType.Subscription);
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickTry()
    {
       
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if (Application.isEditor)
        {
            MainController.TrySkin(m_config.skin);
           
          
        }
        else
        {
           
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_TRY_SKIN, m_config.skin, () => {
                //
              
                TrackingManager.WatchAdMenuTrySkin(m_config.skin);
            });
        }
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickAds()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        AdsManager.Instance.ShowVideoReward(GameConstant.ADS_COINS_500);
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickSelect()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.SelectSkin(m_config.skin);
        ChangeSkinUI.ischangeSkin = false;
    }
}
