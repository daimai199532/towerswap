﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class Tutorial : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_text;
    [SerializeField] private RectTransform m_hand;
    [SerializeField] private Image m_mask;
    [SerializeField] private RectTransform m_buttonLeft;
    [SerializeField] private RectTransform m_buttonRight;
    [SerializeField] private RectTransform m_buttonJump;
    [SerializeField] private RectTransform m_buttonSwitch;
    [SerializeField] private RectTransform m_buttonZoom;
    [SerializeField] private InputController m_inputController;

    private int m_step;

    private bool m_stop;
    private bool m_leftEnter;
    private bool m_rightEnter;
    private bool m_jumpEnter;
    private PlayerType m_playerType;

    void Start()
    {      
        int level = MainModel.GetMapLevel();
        if(level > 2)
        {
            Destroy(gameObject);
            return;
        }
        //
        GameController.levelLoadedEvent += OnLevelLoaded;
        ObjectHome.readyFinishEvent += OnReadySwitch;
        //
        m_step = 0;
        m_stop = false;
        m_playerType = PlayerType.Blue;
        SwitchPlayer(); 
        if(level > 0)
            gameObject.SetActive(false);       
    }

    void OnDestroy()
    {
        ObjectHome.areaEvent -= OnPlayerFinish;
        GameController.levelLoadedEvent -= OnLevelLoaded;
        ObjectHome.readyFinishEvent -= OnReadySwitch;
    }

    private void OnReadySwitch(bool ready, PlayerType playerType)
    {
        if(MainModel.GetMapLevel() < 1)
            return;
        if( m_inputController.GetPlayerType() != playerType)
        {
            Destroy(gameObject);
            return;
        }
        m_inputController.gameObject.SetActive(false);
        gameObject.SetActive(true);
        m_step = 1;
        DoStep();
    }

    private void OnLevelLoaded()
    {
        ObjectHome.areaEvent += OnPlayerFinish;
        //
        DoStep();
    }

    private void OnPlayerFinish(bool status, PlayerType playerType)
    {
        if(!status)
            return;
        if(playerType == PlayerType.Red)
            m_step = 1;
        else
            m_step = 5;
        DoStep();
    }

    void DoStep()
    {
        HideAllInput();
        switch(m_step)
        {
            case 0://move red
                m_text.text = "Collect <color=\"red\">Red Key</color> to unlock <color=\"red\">Red Door";
                m_buttonRight.gameObject.SetActive(true);
                m_hand.position = m_buttonRight.position + new Vector3(1.6f, 1.7f);
                break;
            case 1://switch ball
                m_text.text = "Switch to <#00FFFF>Blue Ball";
                m_buttonSwitch.gameObject.SetActive(true);
                m_hand.localEulerAngles = new Vector3(0, 0, 235);
                m_hand.position = m_buttonSwitch.position + new Vector3(-0.9f, 0.9f);
                break;
            case 2: //jump ball
                m_text.text = "Jump to collect <#00FFFF>Blue Key</color> to unlock <#00FFFF>Blue Door";
                m_buttonJump.gameObject.SetActive(true);
                m_hand.position = m_buttonJump.position + new Vector3(-2, 2);
                break;
            case 3:
                //m_text.text = "Press and hold zoom button to have more vision";
                m_buttonZoom.gameObject.SetActive(false);
                //m_hand.position = m_buttonZoom.position + new Vector3(-0.7f, 0.7f);
                m_step = 4;
                DoStep();
                break;
            case 4://move blue
                m_text.text = "Go to <#00FFFF>Blue Home</color> to complete";
                m_buttonLeft.gameObject.SetActive(true);
                m_hand.localEulerAngles = new Vector3(0, 0, 135);
                m_hand.position = m_buttonLeft.position + new Vector3(1.6f, 1.7f);
                break;
            case 5:
                Color c = m_mask.color;
                c.a = 0;
                m_mask.color = c;
                m_hand.gameObject.SetActive(false);
                break;
        }
    }

    void HideAllInput()
    {
        m_buttonLeft.gameObject.SetActive(false);
        m_buttonRight.gameObject.SetActive(false);
        m_buttonJump.gameObject.SetActive(false);
        m_buttonSwitch.gameObject.SetActive(false);
        m_buttonZoom.gameObject.SetActive(false);
        Color c = m_mask.color;
        c.a = 0.5f;
        m_mask.color = c;
    }

    public void OnLeftPointerEnter(BaseEventData eventData)
    {
        m_leftEnter = true;
    }

    public void OnRightPointerEnter(BaseEventData eventData)
    {
        m_rightEnter = true;
    }

    public void OnJumpPointerEnter(BaseEventData eventData)
    {
        m_jumpEnter = true;
    }

    public void OnLeftPointerExit(BaseEventData eventData)
    {
        m_leftEnter = false;
    }

    public void OnRightPointerExit(BaseEventData eventData)
    {
        m_rightEnter = false;
    }

    public void OnJumpPointerExit(BaseEventData eventData)
    {
        m_jumpEnter = false;
    }

    void Update()
    {
        if (m_stop)
            return;
        if (((Input.touchCount > 0 && m_leftEnter) || Input.GetKey(KeyCode.LeftArrow)) &&  m_step == 4)
        {
            Color c = m_mask.color;
            c.a = 0;
            m_mask.color = c;
            InputController.leftAction?.Invoke(m_playerType);
        }
        if(((Input.touchCount > 0 && m_rightEnter) || Input.GetKey(KeyCode.RightArrow)) && m_step == 0)
        {
            Color c = m_mask.color;
            c.a = 0;
            m_mask.color = c;
            InputController.rightAction?.Invoke(m_playerType);
        }
        if (((Input.touchCount > 0 && m_jumpEnter) || (Input.GetKey(KeyCode.Space) || Input.GetKey(KeyCode.UpArrow))) && m_step == 2)
        {
            Color c = m_mask.color;
            c.a = 0;
            m_mask.color = c;            
            InputController.jumpAction?.Invoke(true, m_playerType);
            StartCoroutine(DelayZoom());          
        }else
            InputController.jumpAction?.Invoke(false, m_playerType);

        if(!m_rightEnter && !m_leftEnter && !Input.GetKey(KeyCode.RightArrow) && !Input.GetKey(KeyCode.LeftArrow))
            InputController.releaseMove?.Invoke(m_playerType);
        if(Input.GetKeyDown(KeyCode.Tab))
            SwitchOnclick();
    }

    public void SwitchOnclick()
    {
        if(m_step != 1)
            return;
        SwitchPlayer();
        InputController.switchAction?.Invoke(m_playerType);
        if(MainModel.GetMapLevel() == 0)
            StartCoroutine(DelayJump());
        else
        {
            m_inputController.SwitchPlayer();
            m_inputController.gameObject.SetActive(true);
            Destroy(gameObject);
        }
    }

    public void OnZoomPointerDown(BaseEventData eventData)
    {
        if(m_step != 3)
            return;
        m_hand.gameObject.SetActive(false);
        Color c = m_mask.color;
        c.a = 0;
        m_mask.color = c;
        GameController.ZoomCamera(MapConfig.maxCameraRatio, 1);
    }
    public void OnZoomPointerUp(BaseEventData eventData)
    {
        if(m_step != 3)
            return;
        GameController.ZoomCamera(-1, 1);
        StartCoroutine(DelayMoveFinish());
    }

    IEnumerator DelayJump()
    {
        m_hand.gameObject.SetActive(false);
        Color c = m_mask.color;
        c.a = 0;
        m_mask.color = c;
        m_step = 2;
        yield return new WaitForSeconds(1f);
        m_hand.gameObject.SetActive(true);        
        DoStep();        
    }

    IEnumerator DelayZoom()
    {
        m_hand.gameObject.SetActive(false);
        Color c = m_mask.color;
        c.a = 0;
        m_mask.color = c;
        m_step = 3;
        yield return new WaitForSeconds(1f);
        m_hand.gameObject.SetActive(true);
        DoStep();
    }

    IEnumerator DelayMoveFinish()
    {
        m_step = 4;
        yield return new WaitForSeconds(1f);
        m_hand.gameObject.SetActive(true);        
        DoStep();
    }

    void SwitchPlayer()
    {
        GameObject red = m_buttonSwitch.Find("icon-red").gameObject;
        GameObject blue = m_buttonSwitch.Find("icon-blue").gameObject;
        red.SetActive(false);
        blue.SetActive(false);
        Color color = Color.black;
        switch(m_playerType)
        {
            case PlayerType.Red:
                blue.SetActive(true);
                m_playerType = PlayerType.Blue;
                ColorUtility.TryParseHtmlString("#00FFFF", out color);                
                break;
            case PlayerType.Blue:
                red.SetActive(true);
                m_playerType = PlayerType.Red;
                color = Color.red;
                break;
        }
        color.a = 0.471f;
        ColorBlock cb = m_buttonLeft.GetComponent<Button>().colors;
        cb.normalColor = color;
        cb.selectedColor = color;
        m_buttonLeft.GetComponent<Button>().colors = cb;
        m_buttonRight.GetComponent<Button>().colors = cb;
        m_buttonJump.GetComponent<Button>().colors = cb;
        m_buttonSwitch.GetComponent<Button>().colors = cb;
    }

}
