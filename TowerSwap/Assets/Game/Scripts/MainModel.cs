﻿
using System;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Random = UnityEngine.Random;

public class MainModel
{
    public const int S_TIME_RIVIVE = 10;
    public const int S_MAX_HEALTH = 100;

    public static bool configLoaded = false;
    public static StoreConfig storeConfig;
    public static SkinConfig skinConfig;
    public static bool paused;

    //remote config
    public static bool activeAdsLevel0 = true;
    public static bool forcePlayBegin = false;
    public static long timeDelayNothanks = 0;
    public static string[] mapSortOrder = new string[0];
    public static string[] levelsSpecialShop = new string[0];
    
    public static long levelShowInterstitial = 1;
    public static long levelShowPopupRemoveAds = -1;
    public static long levelShowPopupRateUs = -1;
    public static long levelShowLuckySpin = 8;
    public static long levelShowX2Reward = 5;
    public static long levelShowInterstitialResume = 5;
    public static bool showAdBanner = false;
    //
    public static string[] listSpecialSkin = new string[0];


    //user info
    public static int currentSkin = 1;
    public static List<int> unlockSkins = new List<int>();
    public static int totalCoin = 0;
    public static int totalHeart = 0;
    public static int spinQuatity = 0;
    public static int keyQuantity = 0;
    //
    public static int totalspinCount = 0;
    public static int spinAdsDayCount = 0;
    //
    public static int specialskin1 = 0;
    public static int specialskin2 = 0;
    public static int specialskin3 = 0;
    public static int specialskin4 = 0;
    public static int specialskin5 = 0;


    public static bool subscription = false;
    public static bool removeAds = false;

    public static int mapLevel = 0;
    public static int health = 0;
    public static int redPoint = 0;
    public static int bluePoint = 0;
    public static int levelCoin = 0;
    public static int levelTreasureKeys = 0;
    public static List<string> keys;
    public static Vector2 savePointRed;
    public static Vector2 savePointBlue;

    public static LevelResult levelResult;
    public static bool pressedZoom = false;

    public static void LoadConfig(string store, string skins)
    {
        storeConfig = JsonUtility.FromJson<StoreConfig>(store);
        skinConfig = JsonUtility.FromJson<SkinConfig>(skins);
        configLoaded = true;
        LoadUserInfo();
    }

    public static void InitGameInfo()
    {
        levelCoin = 0;
        redPoint = 0;
        bluePoint = 0;
        keys = new List<string>();
        health = S_MAX_HEALTH;
        levelTreasureKeys = keyQuantity;
        levelResult = new LevelResult();
    }

    public static float GetRemainHealth()
    {
        return 1f*health / S_MAX_HEALTH;
    }

    public static void ResetSavePoint()
    {
        health = S_MAX_HEALTH;
    }

    public static void LoadUserInfo()
    {
        //skins
        currentSkin = PlayerPrefs.GetInt(GameConstant.PLAYER_PREF_CURRENT_SKIN, 1);
        string[] skins = PlayerPrefs.GetString(GameConstant.PLAYER_PREF_UNLOCK_SKINS, "1").Split(',');
        unlockSkins = new List<int>();
        foreach (string s in skins)
        {
            unlockSkins.Add(int.Parse(s));
        }
        //other
        totalHeart = PlayerPrefs.GetInt(GameConstant.PLAYER_PREF_USER_HEART, 0);
        totalCoin = PlayerPrefs.GetInt(GameConstant.PLAYER_PREF_USER_COIN, 0);
        subscription = PlayerPrefs.GetInt(GameConstant.PLAYER_PREF_SUBSCRIPTION, 0) == 1;
        removeAds = PlayerPrefs.GetInt(GameConstant.PLAYER_PREF_REMOVE_ADS, 0) == 1;
        spinQuatity = PlayerPrefs.GetInt(GameConstant.PLAYER_PREF_SPIN_QUANTITY, 1);
        keyQuantity = PlayerPrefs.GetInt(GameConstant.PLAYER_PREF_KEY_QUANTITY, 0);
        //
        totalspinCount = PlayerPrefs.GetInt(GameConstant.PLAYER_COUNT_SPIN, 0);
        spinAdsDayCount = PlayerPrefs.GetInt(GameConstant.PLAYER_COUNT_ADS_SPIN_DAY, 0);
        //
        specialskin1 = PlayerPrefs.GetInt(GameConstant.PLAYER_SPECIAL_SKIN1, 0);
        specialskin2 = PlayerPrefs.GetInt(GameConstant.PLAYER_SPECIAL_SKIN2, 0);
        specialskin3 = PlayerPrefs.GetInt(GameConstant.PLAYER_SPECIAL_SKIN3, 0);
        specialskin4 = PlayerPrefs.GetInt(GameConstant.PLAYER_SPECIAL_SKIN4, 0);
        specialskin5 = PlayerPrefs.GetInt(GameConstant.PLAYER_SPECIAL_SKIN5, 0);
    }

    public static void SaveAllInfo()
    {
        //current skin
        PlayerPrefs.SetInt(GameConstant.PLAYER_PREF_CURRENT_SKIN, currentSkin);
        //unlock skin
        StringBuilder arr = new StringBuilder();
        foreach(int s in unlockSkins)
        {
            arr.Append(s).Append(",");
        }
        if(arr.Length > 0)
        {
            arr.Remove(arr.Length-1,1);
            PlayerPrefs.SetString(GameConstant.PLAYER_PREF_UNLOCK_SKINS, arr.ToString());
        }
        //
        PlayerPrefs.SetInt(GameConstant.PLAYER_PREF_USER_HEART, totalHeart);
        PlayerPrefs.SetInt(GameConstant.PLAYER_PREF_USER_COIN, totalCoin);
        PlayerPrefs.SetInt(GameConstant.PLAYER_PREF_SUBSCRIPTION, subscription ? 1 : 0);
        PlayerPrefs.SetInt(GameConstant.PLAYER_PREF_REMOVE_ADS, removeAds ? 1 : 0);
        PlayerPrefs.SetInt(GameConstant.PLAYER_PREF_SPIN_QUANTITY, spinQuatity);
        PlayerPrefs.SetInt(GameConstant.PLAYER_PREF_KEY_QUANTITY, keyQuantity);
        //
        PlayerPrefs.SetInt(GameConstant.PLAYER_COUNT_SPIN, totalspinCount);
        PlayerPrefs.SetInt(GameConstant.PLAYER_COUNT_ADS_SPIN_DAY, spinAdsDayCount);
        //
         PlayerPrefs.SetInt(GameConstant.PLAYER_SPECIAL_SKIN1, specialskin1);
         PlayerPrefs.SetInt(GameConstant.PLAYER_SPECIAL_SKIN2, specialskin2);
         PlayerPrefs.SetInt(GameConstant.PLAYER_SPECIAL_SKIN3, specialskin3);
         PlayerPrefs.SetInt(GameConstant.PLAYER_SPECIAL_SKIN4, specialskin4);
         PlayerPrefs.SetInt(GameConstant.PLAYER_SPECIAL_SKIN5, specialskin5);
        //
        PlayerPrefs.Save();
    }

    public static void BuySkin(int skin)
    {
        unlockSkins.Add(skin);
        SaveAllInfo();
    }

    public static bool HasKey(string key)
    {
        return keys.Contains(key);
    }

    public static void AddKey(string key)
    {
        keys.Add(key);
    }

    public static void RemoveKey(string key)
    {
        keys.Remove(key);
    }

    public static void Subscribe(bool active)
    {
        subscription = active;
        SaveAllInfo();
    }

    public static void RemoveAds()
    {
        removeAds = true;
        SaveAllInfo();
    }

    public static int GetMapLevel()
    {
        string key = GameConstant.PLAYER_PREF_MAP_LEVEL;
        mapLevel = PlayerPrefs.GetInt(key, -1) + 1;
        return mapLevel;
    }

    public static void SaveMapLevel(int level = -1)
    {    
        if(level < 0)
        {
            PlayerPrefs.SetInt(GameConstant.PLAYER_PREF_MAP_LEVEL, mapLevel);
            PlayerPrefs.Save();
            mapLevel++;
        }else
        {
            PlayerPrefs.SetInt(GameConstant.PLAYER_PREF_MAP_LEVEL, level);
            PlayerPrefs.Save();
            mapLevel = level + 1;
        }        
    }
    public static void GetSpecialSkin(int id, ref int count)
    {
        switch (id.ToString())
        {
            case "1":
                count = PlayerPrefs.GetInt(GameConstant.PLAYER_SPECIAL_SKIN1);
                break;
            case "2":
                count = PlayerPrefs.GetInt(GameConstant.PLAYER_SPECIAL_SKIN2);
                break;
            case "3":
                count = PlayerPrefs.GetInt(GameConstant.PLAYER_SPECIAL_SKIN3);
                break;
            case "4":
                count = PlayerPrefs.GetInt(GameConstant.PLAYER_SPECIAL_SKIN4);
                break;
            case "5":
                count = PlayerPrefs.GetInt(GameConstant.PLAYER_SPECIAL_SKIN5);
                break;
        }
    }
    public static void UpdateCountAdsEvent(int id)
    {
        switch (id.ToString())
        {
            case "1":
               specialskin1++;
             //   PlayerPrefs.SetInt(GameConstant.PLAYER_SPECIAL_SKIN1, specialskin1);
                break;
            case "2":
                specialskin2++;
             //   PlayerPrefs.SetInt(GameConstant.PLAYER_SPECIAL_SKIN1, specialskin2);
                break;
            case "3":
                specialskin3++;
               // PlayerPrefs.SetInt(GameConstant.PLAYER_SPECIAL_SKIN1, specialskin3);
                break;
            case "4":
                specialskin4++;
              //  PlayerPrefs.SetInt(GameConstant.PLAYER_SPECIAL_SKIN1, specialskin4);
                break;
            case "5":
               specialskin5++;
               // PlayerPrefs.SetInt(GameConstant.PLAYER_SPECIAL_SKIN1, specialskin5);
                break;
        }
        SaveAllInfo();
    }

    public static void UpdateHeart(int heart)
    {
        totalHeart += heart;
        SaveAllInfo();
    }

    public static void UpdateTotalCoin(int coin)
    {
        totalCoin += coin;
        SaveAllInfo();
    }

    //
    public static void UpdateAdsCount(int Count)
    {
        spinAdsDayCount += Count; 
        SaveAllInfo();
    }
    public static void  UpdateTotalCountSpin(int Count)
    {
        totalspinCount += Count;
        SaveAllInfo();
    }
        


    public static bool CheckForcePlayTutorial()
    {
        return forcePlayBegin && GetMapLevel() == 0;
    }

    public static int GetRemainTotalPoint()
    {
        return redPoint + bluePoint;
    }
    public static int GetUnlockRescueSkin()
    {
        int level = GetMapLevel();
        foreach(RescueSkinConfig s in storeConfig.rescue_skins)
        {
            if(s.level == (level - 1))
                return s.skin;
        }
        return -1;
    }

    public static void UpdateKeyTreasure(int count)
    {
        keyQuantity += count;
        SaveAllInfo();
    }

    public static bool CanShowInterstitial()
    {
        return !removeAds && GetMapLevel() > levelShowInterstitial;
    }
    public static int GetRandomSkin()
    {
        List<PremiumSkinConfig> skins = MainModel.storeConfig.premium_skins;
        List<PremiumSkinConfig> remain = new List<PremiumSkinConfig>();
        foreach (PremiumSkinConfig skin in skins)
        {
            if (MainModel.unlockSkins.Contains(skin.skin))
                continue;
            remain.Add(skin);
        }
        int ran = Random.Range(0, remain.Count);
        if (ran >= remain.Count)
            return skins[0].skin;
        return remain[ran].skin;


    }
}

public class LevelResult
{
    public bool isComplete;
    public int mapLevel;
    public int realMapLevel;
    public int oldCoin;
    public int coin;
    public int heart;
}

[Serializable]
public class BoostDamageItem
{
    [SerializeField] public int price;
    [SerializeField] public int ratio;
    [SerializeField] public bool active;
}

[Serializable]
public class BoostHealthItem
{
    [SerializeField] public int price;
    [SerializeField] public int ratio;
    [SerializeField] public bool active;
}

[Serializable]
public class SkinConfig
{
    public List<SkinInfo> skins;

    public SkinInfo GetSkin(int id)
    {
        foreach(SkinInfo sk in skins)
        {
            if(sk.id == id)
                return sk;
        }
        return skins[0];
    }
}

[Serializable]
public class SkinInfo
{
    public int id;
    public string red;
    public string blue;
}

[Serializable]
public class StoreConfig
{
    public List<PremiumSkinConfig> premium_skins;
    public List<CoinSkinConfig> coin_skins;
    public List<RescueSkinConfig> rescue_skins;
    public List<PackageConfig> package;
    public List<SpecialConfig> special_skins;
}

[Serializable]
public class PremiumSkinConfig
{
    public int id;
    public int skin;
    public int coin_ads;
}

[Serializable]
public class PackageConfig
{
    public int id;
    public string name;
    public string description;
}

[Serializable]
public class CoinSkinConfig
{
    public int id;
    public int price;
    public int skin;
    public int coin_ads;
}

[Serializable]
public class RescueSkinConfig
{
    public int id;
    public int skin;
    public int level;
    public int coin_ads;
}
[Serializable]
public class SpecialConfig
{
    public int id;
    public int price;
    public int skin;
    public int coin_ads;
}

