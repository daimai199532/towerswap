using System.Collections;
using UnityEngine;

public class UIHelper : MonoBehaviour
{
    [SerializeField] private float m_bunnyWidth = 100f;    
    //left
    [SerializeField] private RectTransform[] m_leftObj;
    //right
    [SerializeField] private RectTransform[] m_rightObj;

    void Start()
    {
#if UNITY_IOS
        bool noBunny = UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone4 || 
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone4S ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone5 ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone5S ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone5C ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone6 ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone6Plus ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone6S ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone6SPlus ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone7 ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone7Plus ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone8 ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhone8Plus ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPhoneSE1Gen ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPad1Gen ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPad2Gen ||
                        UnityEngine.iOS.Device.generation == UnityEngine.iOS.DeviceGeneration.iPad3Gen ||
                        UnityEngine.iOS.Device.generation.ToString().Contains("iPad");
        if(!noBunny)
            StartCoroutine(Init());
#endif
    }
    IEnumerator Init()
    {
        yield return null;
        yield return null;
        if(Screen.orientation == ScreenOrientation.LandscapeLeft)
        {
            foreach(RectTransform rect in m_leftObj)
            {
                rect.anchoredPosition += new Vector2(m_bunnyWidth, 0);
            }
        }else if(Screen.orientation == ScreenOrientation.LandscapeRight)
        {
            foreach(RectTransform rect in m_rightObj)
            {
                rect.anchoredPosition += new Vector2(m_bunnyWidth, 0);
            }
        }
    }
}
