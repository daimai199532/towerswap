﻿
public enum TargetType
{
    Star,
    Ball
}

public enum PopupType
{
    Store,
    DailyReward,
    LuckySpin,
    Subscription,
    RemoveAds,
    RateUs,
    UnlockSkin,
    Treasure,
    Shop,
    SpecialSkin,
    LeaderBoard
}

public enum QuitGameReason
{
    Back,
    Fail,
    Win,
    Restart,
    Skip
}

public static class GameConstant
{
    public delegate void GameCallback();

    public const string MESSAGE_NO_ADS = "No Video ads are available at the moment";
    public const string MESSAGE_LEADER_BOARD_LOGIN = "LeaderBoard is not ready";

    public static readonly byte[] ENCRYPT_KEY = {0x6e, 0x68, 0x75, 0x76, 0x67, 0x74, 0x66, 0x62, 0x68, 0x74, 0x66, 0x67, 0x62, 0x68, 0x74, 0x64};       
    public static readonly byte[] ENCRYPT_IV = {0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    public static readonly byte[] ENSCRIPT_INPUT = {0x1E,0xA0,0x35,0x3A,0x7D,0x29,0x47,0xD8,0xBB,0xC6,0xAD,0x6F,0xB5,0x2F,0xCA,0x84};
    public const string DECODE_STRING = "GxV7M%!G3+%TxBX%";
    public const int COIN_RATIO = 5;
    public const int BONUS_HEART_ADS = 1;
    public const int BONUS_COIN_ADS = 500;

    public const string URL_POLICY = "https://sites.google.com/view/mgif-studio/";
    public const string URL_TERM = "https://sites.google.com/view/mgif-studio/tos";

    public const string TAG_PLAYER = "Player";
    public const string TAG_COIN = "coin";
    public const string TAG_STAR = "star";
    public const string TAG_GROUND = "ground";
    public const string TAG_OBJECT_BOX = "object-box";
    public const string TAG_WALL = "wall";
    public const string TAG_MONSTER_MOVE_GROUND = "monster-move-ground";
    public const string TAG_MAGNET = "magnet";
    public const string TAG_CLOUD = "cloud";
    public const string TAG_ELEVATOR = "elevator";
    public const string TAG_MIRROR = "mirror";
    public const string TAG_LASER_RECEIVER = "laser-receiver";
    public const string TAG_PRESS = "press";

    public const string PLAYER_PREF_MAP_LEVEL = "map-level";
    public const string PLAYER_PREF_CURRENT_SKIN = "current-skin";
    public const string PLAYER_PREF_UNLOCK_SKINS = "unlock-skins";
    public const string PLAYER_PREF_USER_COIN = "user-coin";
    public const string PLAYER_PREF_USER_HEART = "user-heart";
    public const string PLAYER_PREF_SUBSCRIPTION = "subscription";
    public const string PLAYER_PREF_REMOVE_ADS = "remove-ads";
    public const string PLAYER_PREF_LUCKY_SPIN_START_TIME = "lucky-spin-start-time";
    public const string PLAYER_PREF_LUCKY_SPIN_COUNTDOWN_TIME = "lucky-spin-countdown-time";
    public const string PLAYER_PREF_MUSIC = "music-setting";
    public const string PLAYER_PREF_SOUND = "sound-setting";
    public const string PLAYER_PREF_UNLOCK_SPECIAL_OFFER = "unlock-special-offer";
    public const string PLAYER_PREF_NEXT_MAP_BOSS = "next-map-boss";
    public const string PLAYER_PREF_PREV_MAP_BOSS = "prev-map-boss";
    public const string PLAYER_PREF_NEXT_BOSS_NAME = "next-boss-name";
    public const string PLAYER_PREF_SPIN_QUANTITY = "spin-quantity";
    public const string PLAYER_PREF_KEY_QUANTITY = "key-quantity";
    //
    public const string PLAYER_COUNT_SPIN = "count-spin";
    public const string PLAYER_COUNT_ADS_SPIN_DAY = "count-spin-ads-day";
    //
    public const string PLAYER_SPECIAL_SKIN1 = "special-skin1";
    public const string PLAYER_SPECIAL_SKIN2 = "special-skin2";
    public const string PLAYER_SPECIAL_SKIN3 = "special-skin3";
    public const string PLAYER_SPECIAL_SKIN4 = "special-skin4";
    public const string PLAYER_SPECIAL_SKIN5 = "special-skin5";
    
    //  public const string PLAYER_KEY_COUNT_SPIN = "key-count-spin";

    //ball animation
    public const string PLAYER_ANIMATION_IDLE_IN_GAME = "1_idle_ingame";
    public const string PLAYER_ANIMATION_IDLE_IN_HOME = "1_idle_inhome";
    public const string PLAYER_ANIMATION_HURT = "2_hurt";
    public const string PLAYER_ANIMATION_LAUGH = "3_laugh";
    public const string PLAYER_ANIMATION_SAD = "4_sad";
    public const string PLAYER_ANIMATION_SKINBUY = "5_skinbuy";
    public const string PLAYER_ANIMATION_REVIVE = "6_revive";
    public const string PLAYER_ANIMATION_GET_ADS1 = "7_getads1";
    public const string PLAYER_ANIMATION_GET_ADS2 = "8_getads2";
    public const string PLAYER_ANIMATION_LIKE = "9_like";
    public const string PLAYER_ANIMATION_RIGHT_HAND = "tayphai";
    public const string PLAYER_ANIMATION_LEFT_HAND = "taytrai";

    //ads
    public const string ADS_HEART = "ads-heart";
    public const string ADS_COINS_500 = "ads-coins-500";
    public const string ADS_DAILY_REWARD_X2 = "ads-coins-daily-x2";
    public const string ADS_TRY_SKIN = "ads-try-skin";
    public const string ADS_GET_SKIN = "ads-get-skin";
    public const string ADS_RIVIVE = "ads-rivive";
    public const string ADS_SKIP_LEVEL = "ads-skip-level";
    public const string ADS_SKIP_LEVEL_INGAME = "ads-skip-level-ingame";
    public const string ADS_X2_LEVEL_REWARD = "ads-x2-level-reward";
    public const string ADS_GAME_HEART = "ads-game-heart";
    public const string ADS_GAME_COIN = "ads-game-coin";
    public const string ADS_GAME_MAGNET = "ads-game-magnet";
    public const string ADS_GAME_BOOST_DAMAGE = "ads-game-boost-damage";
    public const string ADS_GAME_BOOST_HEALTH = "ads-game-boost-health";
    public const string ADS_GAME_TRY_SKIN = "ads-game-try-skin";
    public const string ADS_LUCKY_SPIN = "ads-lucky-spin";
    public const string ADS_HEART_ONLY = "ads-heart-only";
    public const string ADS_COIN_ONLY = "ads-coin-only";
    public const string ADS_COIN_HEART = "ads-coin-heart";
    public const string ADS_GET_KEY_TREASURE = "ads-get-key-treasure";
    public const string ADS_SPECIAL_SHOP_BUY_SKIN = "ads-special-shop-buy-skin";
    public const string ADS_SPECIAL_SHOP_BUY_COIN = "ads-special-shop-buy-coin";

    //sound
    public const string AUDIO_MUSIC_HOME = "music-main";
    public const string AUDIO_MUSIC_GAME = "music-game-2";
    public const string AUDIO_INTRO = "intro";
    public const string AUDIO_CLICK = "click";
    public const string AUDIO_OBJECT_BULLET = "object-bullet";
    public const string AUDIO_OBJECT_DOOR_SWITCH = "object-door-switch";
    public const string AUDIO_OBJECT_SWITCH = "object-switch";
    public const string AUDIO_OBJECT_CLOUD_IN = "object-cloud-in";
    public const string AUDIO_OBJECT_CLOUD_OUT = "object-cloud-out";
    public const string AUDIO_OBJECT_SPRINGS = "object-loxo";
    public const string AUDIO_OBJECT_SAVE_BALL = "object-save-ball";
    public const string AUDIO_OBJECT_ENEMI_SHOOT = "object-enemi-shoot";
    public const string AUDIO_BALL_HURT = "ball-hurt";
    public const string AUDIO_OBJECT_ELEVATOR = "object-elevator";
    public const string AUDIO_OBJECT_BALL_DEAD = "ball-dead";
    public const string AUDIO_EAT_ITEM = "collect-item";
    public const string AUDIO_OBJECT_SWITCH_PRESS = "object-switch-press";
    public const string AUDIO_HOME_OPEN = "home_open";
    public const string AUDIO_HOME_WHEEL = "home-wheel";
    public const string AUDIO_BALL_COMPLETE = "ball-complete";
    public const string AUDIO_OBJECT_CHECK_POINT = "object-check-point";
    public const string AUDIO_BALL_JUMP = "ball-jump";
    public const string AUDIO_BALL_HURT_ENEMI = "ball-hurt-enemi";
    public const string AUDIO_BALL_HELP = "ball-help";
    public const string AUDIO_EAT_COIN = "collect-coin";
    public const string AUDIO_VICTORY = "victory";
    public const string AUDIO_RAIN_COIN = "rain-coin";
    public const string AUDIO_MAGNET = "magnet";
    public const string AUDIO_GAMEOVER = "gameover";
    public const string AUDIO_BONUS_HEART = "bonus-heart";
    public const string AUDIO_SPIN_START = "spin-start";
    public const string AUDIO_SPIN_WIN = "spin-win";
    public const string AUDIO_OBJECT_MACE = "object-mace";
    public const string AUDIO_SWITCH_CHARACTER = "switch-char";
}
