﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using Firebase.Extensions;
using Firebase.RemoteConfig;
using UnityEngine;

public class RemoteConfig : MonoBehaviour
{
    private const string KEY_SHOW_INTER_AT_LEVEL = "SHOW_INTER_AT_LEVEL";
    private const string KEY_MAP_SORT_ORDER = "MAP_SORT_ORDER";
    private const string KEY_TIME_DELAY_NOTHANKS = "TIME_DELAY_NOTHANKS";
    private const string KEY_FORCE_PLAY_BEGIN = "FORCE_PLAY_BEGIN";
    private const string KEY_ACTIVE_ADS_LEVEL_0 = "ACTIVE_ADS_LEVEL_0";
    private const string KEY_SHOW_POPUP_REMOVE_ADS = "LEVEL_SHOW_POPUP_REMOVE_ADS";
    private const string KEY_SHOW_POPUP_RATE_US = "LEVEL_SHOW_POPUP_RATE_US";
    private const string KEY_SHOW_LUCKY_SPIN = "LEVEL_SHOW_LUCKY_SPIN";
    private const string KEY_SHOW_X2_LEVEL_REWARD = "LEVEL_SHOW_X2_LEVEL_REWARD";
    private const string KEY_SHOW_INTER_ON_RESUME_AT_LEVEL = "SHOW_INTER_ON_RESUME_AT_LEVEL";
    private const string KEY_SHOW_SPECIAL_SHOP = "SHOW_SPECIAL_SHOP";
    private const string KEY_SHOW_ADS_BANNER = "SHOW_ADS_BANNER";
    private const string KEY_SHOW_SPECIAL_SKINS = "SPECIAL_SKINS";

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        SetDefault();
    }

    void Start()
    {
        FetchDataAsync();
    }

    void SetDefault()
    {
        Dictionary<string, object> defaults = new Dictionary<string, object>();
        defaults.Add(KEY_SHOW_INTER_AT_LEVEL, 1);
        defaults.Add(KEY_MAP_SORT_ORDER, "");
        defaults.Add(KEY_TIME_DELAY_NOTHANKS, 2);
        defaults.Add(KEY_FORCE_PLAY_BEGIN, true);
        defaults.Add(KEY_ACTIVE_ADS_LEVEL_0, false);
        defaults.Add(KEY_SHOW_POPUP_REMOVE_ADS, -1);
        defaults.Add(KEY_SHOW_POPUP_RATE_US, -1);
        defaults.Add(KEY_SHOW_LUCKY_SPIN, 8);
        defaults.Add(KEY_SHOW_X2_LEVEL_REWARD, 5);
        defaults.Add(KEY_SHOW_INTER_ON_RESUME_AT_LEVEL, 5);
        defaults.Add(KEY_SHOW_SPECIAL_SHOP, "");
        defaults.Add(KEY_SHOW_ADS_BANNER, false);
        //
        defaults.Add(KEY_SHOW_SPECIAL_SKINS, "");
        FirebaseRemoteConfig.DefaultInstance.SetDefaultsAsync(defaults);
    }

    public Task FetchDataAsync()
    {
        Task fetchTask = FirebaseRemoteConfig.DefaultInstance.FetchAsync(TimeSpan.Zero);
        return fetchTask.ContinueWithOnMainThread(FetchComplete);
    }

    private Task FetchComplete(Task task)
    {
        Task fetchTask = FirebaseRemoteConfig.DefaultInstance.ActivateAsync();
        return fetchTask.ContinueWithOnMainThread(ParseData);
    }

    private void ParseData(Task task)
    {
        ConfigValue value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_SHOW_INTER_AT_LEVEL);
        MainModel.levelShowInterstitial = value.LongValue;
        //
        value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_MAP_SORT_ORDER);
        string[] levels = value.StringValue.Trim() == "" ? new string[0] : value.StringValue.Split(',');
        MainModel.mapSortOrder = levels;
        //
        value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_SHOW_SPECIAL_SHOP);
        string[] levelsShop = value.StringValue.Trim() == "" ? new string[0] : value.StringValue.Split(',');
        MainModel.levelsSpecialShop = levelsShop;
        //
        value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_TIME_DELAY_NOTHANKS);
        MainModel.timeDelayNothanks = value.LongValue;
        //
        value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_FORCE_PLAY_BEGIN);
        MainModel.forcePlayBegin = value.BooleanValue;
        //
        value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_ACTIVE_ADS_LEVEL_0);
        MainModel.activeAdsLevel0 = value.BooleanValue;
        //
        value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_SHOW_POPUP_REMOVE_ADS);
        MainModel.levelShowPopupRemoveAds = value.LongValue;
        //
        value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_SHOW_POPUP_RATE_US);
        MainModel.levelShowPopupRateUs = value.LongValue;
        //
        value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_SHOW_LUCKY_SPIN);
        MainModel.levelShowLuckySpin = value.LongValue;
        //
        value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_SHOW_X2_LEVEL_REWARD);
        MainModel.levelShowX2Reward = value.LongValue;
        //
        value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_SHOW_INTER_ON_RESUME_AT_LEVEL);
        MainModel.levelShowInterstitialResume = value.LongValue;
        //
        value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_SHOW_ADS_BANNER);
        MainModel.showAdBanner = value.BooleanValue;
        //
        value = FirebaseRemoteConfig.DefaultInstance.GetValue(KEY_SHOW_SPECIAL_SKINS);
        string[] specialskin = value.StringValue.Trim() == "" ? new string[0] : value.StringValue.Split(',');
        MainModel.listSpecialSkin = specialskin;
    }
}
