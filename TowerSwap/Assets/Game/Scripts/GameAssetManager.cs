﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameAssetManager : MonoBehaviour
{
    public static GameAssetManager api;
    public static Action<float> updateEvent;
    public static Action completeEvent;
    private byte[] m_seceneMain;
    private byte[] m_sceneLevel;
    private byte[] m_levels;
    private AssetBundle m_bundleSceneMain;
    private AssetBundle m_bundleSceneLevel;
    private AssetBundle m_bundleLevel;

    private Dictionary<int, Sprite> m_avatars;
    private int m_loadCount;

    void Awake()
    {
        if(api == null)
            api = this;
        m_avatars = new Dictionary<int, Sprite>();
        DontDestroyOnLoad(gameObject);
    }

    void Start()
    {        
#if UNITY_EDITOR   
        DOTween.To(() => 0, x =>updateEvent?.Invoke(x), 1f, 1f).SetEase(Ease.Linear)
            .OnComplete(() =>
            {
                completeEvent?.Invoke();
            });
#else
        m_loadCount = 0;
        StartCoroutine(LoadBundleSceneHome());
        StartCoroutine(LoadBundleSceneLevel());
        StartCoroutine(LoadBundleLevel());
#endif        
    }

    IEnumerator LoadBundleSceneHome()
    {
        yield return null;        
        ResourceRequest request = Resources.LoadAsync<TextAsset>("bundles/scene-main");
        yield return request; 
        byte[] data = (request.asset as TextAsset).bytes;
        m_seceneMain = GameUtils.Decrypt(data, GameConstant.ENCRYPT_KEY, GameConstant.ENCRYPT_IV);  
        Resources.UnloadAsset(request.asset);         
        DecodeBundleComplete();  
    }

    IEnumerator LoadBundleSceneLevel()
    {
        yield return null;      
        yield return null;  
        ResourceRequest request = Resources.LoadAsync<TextAsset>("bundles/scene-level");
        yield return request;   
        byte[] data = (request.asset as TextAsset).bytes;
        m_sceneLevel = GameUtils.Decrypt(data, GameConstant.ENCRYPT_KEY, GameConstant.ENCRYPT_IV);  
        Resources.UnloadAsset(request.asset); 
        DecodeBundleComplete();     
    }

    IEnumerator LoadBundleLevel()
    {
        yield return null;
        yield return null;
        yield return null;
        ResourceRequest request = Resources.LoadAsync<TextAsset>("bundles/levels");
        yield return request;
        byte[] data = (request.asset as TextAsset).bytes;
        m_levels = GameUtils.Decrypt(data, GameConstant.ENCRYPT_KEY, GameConstant.ENCRYPT_IV);  
        Resources.UnloadAsset(request.asset);
        DecodeBundleComplete();     
    }

    IEnumerator BuildBundle()
    {
        AssetBundleCreateRequest request = AssetBundle.LoadFromMemoryAsync(m_seceneMain);
        yield return request;
        m_bundleSceneMain = request.assetBundle;
        request = AssetBundle.LoadFromMemoryAsync(m_sceneLevel);
        yield return request;
        m_bundleSceneLevel = request.assetBundle;
        request = AssetBundle.LoadFromMemoryAsync(m_levels);
        yield return request;
        m_bundleLevel = request.assetBundle;
        //
        m_seceneMain = null;
        m_sceneLevel = null;
        m_levels = null;
        completeEvent?.Invoke();
    }

    void DecodeBundleComplete()
    {
        m_loadCount++;
        if(m_loadCount > 2)
            StartCoroutine(BuildBundle());
    }

    public void OpenSceneLevel()
    {
        StartCoroutine(LoadSceneLevel());
    }

    public void OpenSceneMain()
    {
        StartCoroutine(LoadSceneMain());
    }

    IEnumerator LoadSceneMain()
    {
#if UNITY_EDITOR
        yield return null;
        SceneManager.LoadScene("Main", LoadSceneMode.Single);        
#else
        yield return null;
        AssetBundle bundle = m_bundleSceneMain;
        if(bundle.isStreamedSceneAssetBundle)
        {
            string[] scenePaths = bundle.GetAllScenePaths();
            string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePaths[0]);
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);            
        }
#endif  
    }

    IEnumerator LoadSceneLevel()
    {
#if UNITY_EDITOR
        yield return null;
        SceneManager.LoadScene("Level", LoadSceneMode.Single);        
#else
        yield return null;
        AssetBundle bundle = m_bundleSceneLevel;
        if(bundle.isStreamedSceneAssetBundle)
        {
            string[] scenePaths = bundle.GetAllScenePaths();
            string sceneName = System.IO.Path.GetFileNameWithoutExtension(scenePaths[0]);
            SceneManager.LoadScene(sceneName, LoadSceneMode.Single);            
        }
#endif  
    }

    public void GetAvatar(int skin, Image image, bool nativeSize = true)
    {
        if(m_avatars.ContainsKey(skin))
        {
            image.sprite = m_avatars[skin];
            if(nativeSize)
                image.SetNativeSize();
            return;
        }
        StartCoroutine(IGetAvatar(skin, image, nativeSize));
    }

    IEnumerator IGetAvatar(int skin, Image image, bool nativeSize = true)
    {
        ResourceRequest request = Resources.LoadAsync<Sprite>("avatars/avatar-" + skin);
        yield return request;
        if(request.asset == null)
            yield break;
        Sprite sp = null;
#if UNITY_EDITOR   
        sp = request.asset as Sprite;             
#else
        byte[] data = (request.asset as TextAsset).bytes;
        data = GameUtils.Decrypt(data, GameConstant.ENCRYPT_KEY, GameConstant.ENCRYPT_IV);  
        Resources.UnloadAsset(request.asset); 
        Texture2D texture = new Texture2D(10, 10);
        texture.LoadImage(data);
        sp = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), Vector2.zero, 1);        
#endif   
        if(sp != null)
            m_avatars.Add(skin, sp);
        image.sprite = sp;   
        if(nativeSize)
            image.SetNativeSize();
    }

    public GameObject GetLevel(string levelName)
    {
#if UNITY_EDITOR
        string levelPath = "levels/level-" + levelName;
        GameObject go = Resources.Load<GameObject>(levelPath);
        return go;
#else   
        AssetBundle bundle = m_bundleLevel;//AssetBundle.LoadFromMemory(m_levels);
        GameObject go = bundle.LoadAsset<GameObject>("level-" + levelName);
        return go;
#endif    
    }
}
