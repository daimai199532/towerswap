using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

public enum LeaderBoard
{
    Level,
    Coin,
    Skin
}

public class LeaderBoardManager : MonoBehaviour
{
    public static Action loginEvent;
    public static Action<LeaderBoard, RankResponse> rankEvent;

    public static LeaderBoardManager api;

    private const string KEY_USER = "leader_board_user";
    private const string KEY_READY = "leader_board_ready";
    private const string BASE_URL = "https://platformer.mgif.net";

    private bool m_firsTime;
    private bool m_login;
    private LoginInfo m_user;

    public bool ready
    {
        get
        {
            return m_login && m_firsTime;
        }
    }

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if (api == null)
            api = this;
        m_login = false;
    }

    void Start()
    {
        LoadData();
        if (!m_login)
            StartCoroutine(ILogin());
    }

    public void LoadBoard(LeaderBoard type)
    {
        StartCoroutine(GetBoard(type));
    }

    public void UpdateData()
    {
        //StartCoroutine(IUpdateUserInfo());
        StartCoroutine(IUpdateUserData());
    }

    IEnumerator ILogin()
    {
        Debug.Log("=========Leader Board - Start login...");
        LoginRequest request = new LoginRequest()
        {
            user_id = m_user == null ? "" : m_user.user_id
        };
        var www = new UnityWebRequest(GetUrl("/user/login"), "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonUtility.ToJson(request));
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        www.SetRequestHeader("Content-Type", "application/json");
        yield return www.SendWebRequest();
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("=========Leader Board - Login error::" + www.error);
            StartCoroutine(ReLogin());
        }
        else
        {
            Debug.Log("=========Leader Board - Login result::" + www.downloadHandler.text);
            ResponseInfo<LoginInfo> data = JsonUtility.FromJson<ResponseInfo<LoginInfo>>(www.downloadHandler.text);
            if (data != null)
            {
                if (data.status != 1 || data.code != 0)
                {
                    m_login = false;
                    StartCoroutine(ReLogin());
                }
                else
                {
                    m_login = true;
                    m_user = data.data;
                    SaveData();
                    //
                    UpdateData();
                    //
                    loginEvent?.Invoke();
                }
            }
        }
    }

    IEnumerator ReLogin()
    {
        yield return new WaitForSeconds(1f);
        StartCoroutine(ILogin());
    }

    IEnumerator IUpdateUserInfo()
    {
        if (m_user == null)
            yield break;
        Debug.Log("=========Leader Board - Start update info...");
        UpdateInfoRequest request = new UpdateInfoRequest()
        {
            user_id = m_user.user_id,
            name = m_user.name,
            avatar = m_user.avatar
        };
        var www = new UnityWebRequest(GetUrl("/user/update"), "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonUtility.ToJson(request));
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        www.SetRequestHeader("Content-Type", "application/json");
        yield return www.SendWebRequest();
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("=========Leader Board - Update info error::" + www.error);
        }
        else
        {
            Debug.Log("=========Leader Board - Update info result::" + www.downloadHandler.text);
        }
    }

    IEnumerator IUpdateUserData()
    {
        if (m_user == null)
            yield break;
        Debug.Log("=========Leader Board - Start update data...");
        UpdateDataRequest request = new UpdateDataRequest()
        {
            user_id = m_user.user_id,
            total_criteria = new UserDataInfo()
            {
                coin = MainModel.totalCoin,
                level = MainModel.GetMapLevel(),
                point = 0,
                skin = (MainModel.unlockSkins.Count)
            }
        };
        var www = new UnityWebRequest(GetUrl("/leaderboard/update"), "POST");
        byte[] bodyRaw = Encoding.UTF8.GetBytes(JsonUtility.ToJson(request));
        www.uploadHandler = (UploadHandler)new UploadHandlerRaw(bodyRaw);
        www.downloadHandler = (DownloadHandler)new DownloadHandlerBuffer();
        www.SetRequestHeader("Content-Type", "application/json");
        yield return www.SendWebRequest();
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("=========Leader Board - Update data error::" + www.error);
        }
        else
        {
            ResponseInfo<LoginInfo> data = JsonUtility.FromJson<ResponseInfo<LoginInfo>>(www.downloadHandler.text);
            if (data.status != 1 || data.code != 0)
            {

            }
            else
            {
                m_firsTime = true;
                SaveData();
            }
            Debug.Log("=========Leader Board - Update data result::" + www.downloadHandler.text);
        }
    }

    IEnumerator GetBoard(LeaderBoard type)
    {
        if (m_user == null || !m_login)
        {
            rankEvent?.Invoke(type, null);
            yield break;
        }
        Debug.Log("=========Leader Board - Start get board...");
        string typeStr = "";
        switch (type)
        {
            case LeaderBoard.Level:
                typeStr = "level";
                break;
            case LeaderBoard.Coin:
                typeStr = "coin";
                break;
            case LeaderBoard.Skin:
                typeStr = "skin";
                break;
        }
        string param = "&user_id=" + m_user.user_id + "&order_by=" + typeStr;
        //var www = new UnityWebRequest(GetUrl("/leaderboard/update", param), "GET");
        var www = UnityWebRequest.Get(GetUrl("/leaderboard/list", param));
        www.SetRequestHeader("Content-Type", "application/json");
        yield return www.SendWebRequest();
        if (www.result != UnityWebRequest.Result.Success)
        {
            Debug.Log("=========Leader Board - Get board error::" + www.error);
            rankEvent?.Invoke(type, null);
        }
        else
        {
            Debug.Log("=========Leader Board - Get board result::" + www.downloadHandler.text);
            ResponseInfo<RankResponse> data = JsonUtility.FromJson<ResponseInfo<RankResponse>>(www.downloadHandler.text);
            if (data != null)
            {
                if (data.status != 1 || data.code != 0)
                    rankEvent?.Invoke(type, null);
                else
                {
                    data.data.FakeReatime(type);
                    rankEvent?.Invoke(type, data.data);
                }
            }
        }
    }

    void SaveData()
    {
        if (m_user != null)
        {
            PlayerPrefs.SetString(KEY_USER, JsonUtility.ToJson(m_user));
            if (m_firsTime)
                PlayerPrefs.SetInt(KEY_READY, 1);
        }
    }

    void LoadData()
    {
        if (PlayerPrefs.HasKey(KEY_USER))
            m_user = JsonUtility.FromJson<LoginInfo>(PlayerPrefs.GetString(KEY_USER, "{}"));
        m_firsTime = PlayerPrefs.HasKey(KEY_READY);
    }

    string GetUrl(string api, string param = "")
    {
        return BASE_URL + api + "?package_name=" + Application.identifier + param;
    }

}
[Serializable]
public class LoginRequest
{
    public string user_id;
}

[Serializable]
public class UpdateInfoRequest
{
    public string user_id;
    public string name;
    public int avatar;
}

[Serializable]
public class UpdateDataRequest
{
    public string user_id;
    public UserDataInfo total_criteria;
}
[Serializable]
public class RankResponse
{
    public List<RankInfo> top_users;
    public RankInfo current_user;

    public void FakeReatime(LeaderBoard type)
    {
        current_user.criteria.coin = MainModel.totalCoin;
        current_user.criteria.level = MainModel.GetMapLevel();
        current_user.criteria.skin = MainModel.unlockSkins.Count;
        int baseSize = top_users.Count;
        bool found = false;
        for (int i = 0; i < top_users.Count; i++)
        {
            if (top_users[i].name == current_user.name)
            {
                found = true;
                top_users[i] = current_user;
                break;
            }
        }
        if (!found)
            top_users.Add(current_user);
        switch (type)
        {
            case LeaderBoard.Level:
                top_users.Sort((n1, n2) => n2.criteria.level.CompareTo(n1.criteria.level));
                break;
            case LeaderBoard.Coin:
                top_users.Sort((n1, n2) => n2.criteria.coin.CompareTo(n1.criteria.coin));
                break;
        }
        if (top_users.Count > baseSize)
            top_users.RemoveAt(top_users.Count - 1);
        for (int i = 0; i < top_users.Count; i++)
        {
            RankInfo r = top_users[i];
            r.rank = i + 1;
        }
    }
}

[Serializable]
public class RankInfo
{
    public string name;
    public string avatar;
    public int rank;
    public UserDataInfo criteria;
}

[Serializable]
public class UserDataInfo
{
    public int level;
    public int point;
    public int skin;
    public int coin;
}

[Serializable]
public class ResponseInfo<T>
{

    public int status;
    public int code;
    public string message;
    public T data;
}

[Serializable]
public class LoginInfo
{
    public string user_id;
    public string name;
    public int avatar;

}
