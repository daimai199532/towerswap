﻿
using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScene : MonoBehaviour
{
    [SerializeField] private Slider m_process;
    [SerializeField] private TextAsset m_storeConfig;
    [SerializeField] private TextAsset m_skinConfig;

    void Start()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
        Application.targetFrameRate = Screen.currentResolution.refreshRate;
        QualitySettings.vSyncCount = 0;
        HackBug();
        MainController.LoadConfig(m_storeConfig.text, m_skinConfig.text);
        //simulate load
        GameAssetManager.updateEvent += OnProcessUpdate;
        GameAssetManager.completeEvent += OnLoadResourceComplete;
        //
        m_process.value = 0;
#if !UNITY_EDITOR
        m_process.DOValue(1f, 2f).SetEase(Ease.Linear);
#endif
        TrackingManager.LoadingStart();
    }

    void OnDestroy()
    {
        GameAssetManager.updateEvent -= OnProcessUpdate;
        GameAssetManager.completeEvent -= OnLoadResourceComplete;
        m_process.DOKill();
    }

    private void OnLoadResourceComplete()
    {
        AdsManager.Instance.LoadBaner();
        if(MainModel.CheckForcePlayTutorial())
            GameAssetManager.api.OpenSceneLevel();
        else
            GameAssetManager.api.OpenSceneMain();
        TrackingManager.LoadingEnd();
    }

    private void OnProcessUpdate(float process)
    {
#if UNITY_EDITOR
        m_process.value = process;
#endif
    }

    void HackBug()
    {
        // These classes won't be linked away because of the code,
        // but we also won't have to construct unnecessarily either,
        // hence the if statement with (hopefully) impossible
        // runtime condition.
        //
        // This is to resolve crash at CultureInfo.CurrentCulture
        // when language is set to Thai. See
        // https://github.com/xamarin/Xamarin.Forms/issues/4037
        if (Environment.CurrentDirectory == "_never_POSSIBLE_")
        {
            new System.Globalization.ChineseLunisolarCalendar();
            new System.Globalization.HebrewCalendar();
            new System.Globalization.HijriCalendar();
            new System.Globalization.JapaneseCalendar();
            new System.Globalization.JapaneseLunisolarCalendar();
            new System.Globalization.KoreanCalendar();
            new System.Globalization.KoreanLunisolarCalendar();
            new System.Globalization.PersianCalendar();
            new System.Globalization.TaiwanCalendar();
            new System.Globalization.TaiwanLunisolarCalendar();
            new System.Globalization.ThaiBuddhistCalendar();
            new System.Globalization.UmAlQuraCalendar();
        }
    }

}
