﻿
using System;
using UnityEngine;
using UnityEngine.SceneManagement;

using Random = UnityEngine.Random;

public class MainController
{
    public static Action<PopupType, object> openPopupEvent;
    public static Action<PopupType> closePopupEvent;
    public static Action showMainUIEvent;
    public static Action<int> updateCoinEvent;
    public static Action<int, Vector3?> updateHeartEvent;
    public static Action<int> updateStoreSkinEvent;//skin
    public static Action<int, int> changeSkinEvent;//<old skin. new skin>
    public static Action<LevelResult> updateLevelResultEvent;
    public static Action claimX2DailyRewardEvent;
    public static Action showDailyRewardEvent;
    public static Action<string> noticeEvent;
    public static Action<int> bonusHeartEvent;
    public static Action<int> bonusCoinEvent;
    public static Action<int> bonusSkinEvent;
    public static Action resetUiEvent;//after claim x2 level reward
    public static Action<bool> activeLoadingEvent;
    public static Action<int> trySkinUIEvent;
    public static Action updateSpinEvent;
    public static Action doLuckySpinEvent;
    public static Action updateKeyTreasureEvent;
    //
    public static Action<int> updateCountAdsEvent;
    public static Action<int> testEvent;
    public static Action updateCountLuckySpinEvent;

    public static void LoadConfig(string storeConfig, string skinConfig)
    {
        MainModel.LoadConfig(storeConfig, skinConfig);
        IAPManager.Instance.Init();
    }

    public static void UpdateUI()
    {
      
        updateCoinEvent?.Invoke(MainModel.totalCoin);
       
        changeSkinEvent?.Invoke(0, MainModel.currentSkin);
        updateLevelResultEvent?.Invoke(MainModel.levelResult);
        updateHeartEvent?.Invoke(MainModel.totalHeart, null);
        updateSpinEvent?.Invoke();
        updateKeyTreasureEvent?.Invoke();

        //
      // updateCountAdsEvent?.Invoke();
     
       TrySkinUI(MainModel.GetRandomSkin());
    }

   
    public static void OpenPopup(PopupType type)
    {
        switch (type)
        {
            case PopupType.Store:
                openPopupEvent?.Invoke(type, MainModel.storeConfig);
                break;
            default:
                openPopupEvent?.Invoke(type, null);
                break;
        }
        
    }

    public static void ClosePopup(PopupType type)
    {
        closePopupEvent?.Invoke(type);
    }

    public static bool ConfigLoaded()
    {
        if(MainModel.configLoaded)
            return true;
        SceneManager.LoadScene("Loading", LoadSceneMode.Single);
        return false;
    }

    public static void UpdateCoin(int coin)
    {
        MainModel.UpdateTotalCoin(coin);
        updateCoinEvent?.Invoke(MainModel.totalCoin);
    }

    public static void UpdateHeart(int heart, Vector3? itemPos = null)
    {
        MainModel.UpdateHeart(heart);
        updateHeartEvent?.Invoke(MainModel.totalHeart, itemPos);
    }

    public static void BuyCoinSkin(int coin, int skin)
    {
        if(MainModel.totalCoin < coin)
            return;
        MainModel.BuySkin(skin);
        UpdateCoin(coin*-1);
        updateStoreSkinEvent?.Invoke(skin);
        //
        TrackingManager.UnlockSkin(skin);
    }
    public static void TrySkin(int skin)
    {
        SelectSkin(skin);
        //
        TrackingManager.WatchAdsTrySkin(skin);
    }

    public static void SelectSkin(int skin)
    {
        int oldSkin = MainModel.currentSkin;
        MainModel.currentSkin = skin;
        changeSkinEvent?.Invoke(oldSkin, skin);
        updateStoreSkinEvent?.Invoke(skin);
    }

    public static void ShowMainUI()
    {
        showMainUIEvent?.Invoke();
    }

    public static void ClaimX2DailyReward()
    {
        claimX2DailyRewardEvent?.Invoke();
    }

    public static void SubscribeVip(bool active)
    {
        MainModel.Subscribe(active);
    }

    public static void RemoveAds()
    {
        MainModel.RemoveAds();
    }

    public static void SkipLevel()
    {
        MainModel.SaveMapLevel();        
    }

    public static void X2LevelReward()
    {
        BonusCoin(MainModel.levelResult.coin*GameConstant.COIN_RATIO);
        resetUiEvent?.Invoke();
    }

    public static void CheckAndShowDailyReward()
    {
        showDailyRewardEvent?.Invoke();
    }

    public static void ShowNotice(string message)
    {
        noticeEvent?.Invoke(message);
    }

    public static void BonusHeart(int heart)
    {
        MainModel.UpdateHeart(heart);
        bonusHeartEvent?.Invoke(heart);
    }

    public static void BonusCoin(int coin)
    {
        MainModel.UpdateTotalCoin(coin);
        bonusCoinEvent?.Invoke(coin);
    }

    public static void ActiveLoading(bool active)
    {
        activeLoadingEvent?.Invoke(active);
    }

    public static void TrySkinUI(int skin)
    {
       
        trySkinUIEvent?.Invoke(skin);
    
    }

    public static void UpdateSpin(int quantity)
    {
       
        MainModel.spinQuatity += quantity;
        MainModel.SaveAllInfo();
        updateSpinEvent?.Invoke();
    }
   
    public static void DoLuckySpin()
    {
        doLuckySpinEvent?.Invoke();
    }

    public static void UpdateKeyTreasure(int key)
    {
        MainModel.UpdateKeyTreasure(key);
        updateKeyTreasureEvent?.Invoke();
    }
    public static void UpdateCountAdsEvent(int id)
    {
        MainModel.UpdateCountAdsEvent(id);
        updateCountAdsEvent?.Invoke(id);

    }
    public static void UpdateCountLuckySpin()
    {
        MainModel.UpdateTotalCountSpin(1);
        updateCountLuckySpinEvent?.Invoke();
      
    }
    public static void UpdateCountSpinAdsDay()
    {
        MainModel.UpdateAdsCount(1);
    }

}
