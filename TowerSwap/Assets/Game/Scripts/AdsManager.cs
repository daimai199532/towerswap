﻿
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static GameConstant;

public class AdsManager : MonoBehaviour
{
    public static AdsManager Instance;

    private string m_currentAds;
    private object m_adsData;
    private bool m_rewarded;
    private bool m_adsShowing;
    private GameCallback m_doAction;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);
        if(Instance == null)
            Instance = this;
        Init();
    }

    void Init()
    {
        IronSourceEvents.onRewardedVideoAdOpenedEvent += OnRewardedVideoAdOpened;
        IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
        IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
        IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
        IronSourceEvents.onInterstitialAdClosedEvent += OnInterstitialAdClosed;
        IronSourceEvents.onInterstitialAdOpenedEvent += OnInterstitialAdOpened;
        IronSourceEvents.onInterstitialAdLoadFailedEvent += OnInterstitialAdLoadFailed;
        IronSourceEvents.onImpressionSuccessEvent += ImpressionSuccessEvent;
        IronSourceEvents.onBannerAdLoadedEvent += onBannerAdLoadedEvent;
        IronSourceEvents.onBannerAdLoadFailedEvent += BannerAdLoadFailedEvent;
        //
        IronSource.Agent.shouldTrackNetworkState(true);
        IronSource.Agent.setMetaData("AppLovin_AgeRestrictedUser", "false");
        IronSource.Agent.setMetaData("AdMob_TFCD", "false");
        IronSource.Agent.setMetaData("AdMob_TFUA", "false");
        IronSource.Agent.setMetaData("AdColony_COPPA", "false");
        IronSource.Agent.setMetaData("UnityAds_COPPA", "false");        
        //
        m_adsShowing = false; 
    }

    void OnApplicationPause(bool isPaused) {  
        if(!isPaused && MainModel.CanShowInterstitial())
            AdsManager.Instance.ShowInterstitial("resume");  
        IronSource.Agent.onApplicationPause(isPaused);      
    }

    public void LoadInterstitial()
    {
        IronSource.Agent.loadInterstitial();        
    }

    public void LoadBaner()
    {
        if(!MainModel.showAdBanner)
            return;
        Debug.Log("=========ADS:: BANNER LOADING...");
        //IronSource.Agent.loadBanner(new IronSourceBannerSize(320, 50), IronSourceBannerPosition.BOTTOM);
        IronSource.Agent.loadBanner(IronSourceBannerSize.BANNER, IronSourceBannerPosition.BOTTOM);
    }

    public void ShowBanner()
    {
        if(!MainModel.showAdBanner)
            return;
        IronSource.Agent.displayBanner();
    }

    public void HideBanner()
    {
        if(!MainModel.showAdBanner)
            return;
        IronSource.Agent.hideBanner();
    } 

    //////////////////////////////////BANNER//////////////////////////////////////////    
    private void onBannerAdLoadedEvent()
    {
        Debug.Log("=========ADS:: BANNER LOADED");
        HideBanner();
    }

    void BannerAdLoadFailedEvent (IronSourceError error) 
    {
        Debug.Log("=========ADS:: BANNER LOAD FAIL::" + error.getDescription());
        StartCoroutine(DelayLoadBanner());
    }

    IEnumerator DelayLoadBanner()
    {
        yield return new WaitForSeconds(0.5f);
        LoadBaner();
    }
    ////////////////////////////////////////INSTERTITIAL//////////////////////////////////
    public void ShowInterstitial(string localtion, GameCallback doAction = null)
    {
        if(m_adsShowing || !MainModel.CanShowInterstitial() || !IronSource.Agent.isInterstitialReady())
        {
            if(doAction != null)
                doAction?.Invoke();
            return;
        }
        m_doAction = doAction;
        m_adsShowing = true;
        MainController.ActiveLoading(true);
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            SoundManager.MuteMusic(true, false);
            SoundManager.MuteSound(true, false);
            StartCoroutine(WaitInterstitial());
        }
        IronSource.Agent.showInterstitial();
    }

    IEnumerator WaitInterstitial()
    {
        yield return new WaitForSeconds(3f);       
        m_adsShowing = false;
        MainController.ActiveLoading(false);
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            SoundManager.MuteMusic(false, false);
            SoundManager.MuteSound(false, false);
        }
        LoadInterstitial();
        if(m_doAction != null)
            m_doAction?.Invoke();
        m_doAction = null;
    }

    private void OnInterstitialAdOpened()
    {
        if(Application.platform == RuntimePlatform.IPhonePlayer)
            StopAllCoroutines();
    }

    private void OnInterstitialAdLoadFailed(IronSourceError obj)
    {
        StopAllCoroutines();
        m_adsShowing = false;
        MainController.ActiveLoading(false);
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            SoundManager.MuteMusic(false, false);
            SoundManager.MuteSound(false, false);
        }
        LoadInterstitial();
        if(m_doAction != null)
            m_doAction?.Invoke();
        m_doAction = null;
    }    

    private void OnInterstitialAdClosed()
    {
        StopAllCoroutines();
        MainController.ActiveLoading(false);
        LoadInterstitial();
        m_adsShowing = false;
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            SoundManager.MuteMusic(false, false);
            SoundManager.MuteSound(false, false);
        }
        if(m_doAction != null)
            m_doAction?.Invoke();
        m_doAction = null;
    }

    /////////////////////////////////VIDEO-REWARD///////////////////////////////////////
    public void ShowVideoReward(string placement, object data = null, GameCallback doAction = null)
    {
        if (m_adsShowing || !IronSource.Agent.isRewardedVideoAvailable())
        {
            MainController.ShowNotice(GameConstant.MESSAGE_NO_ADS);
            return;
        }
        m_doAction = doAction;
        m_adsShowing = true;
        MainController.ActiveLoading(true);
        m_currentAds = placement;
        m_adsData = data;
        m_rewarded = false;
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            SoundManager.MuteMusic(true, false);
            SoundManager.MuteSound(true, false);
            StartCoroutine(WaitVideoReward());
        }
        IronSource.Agent.showRewardedVideo();        
    }

    IEnumerator WaitVideoReward()
    {
        yield return new WaitForSeconds(3f);
        m_adsShowing = false;
        MainController.ActiveLoading(false);
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            SoundManager.MuteMusic(false, false);
            SoundManager.MuteSound(false, false);
        }      
        if(m_doAction != null)
            m_doAction?.Invoke();
        m_doAction = null;          
    }

    private void OnRewardedVideoAdOpened()
    {
        if(Application.platform == RuntimePlatform.IPhonePlayer)
            StopAllCoroutines();
    }

    void RewardedVideoAdClosedEvent()
    {
        StopAllCoroutines();
        m_adsShowing = false;
        MainController.ActiveLoading(false);
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            SoundManager.MuteMusic(false, false);
            SoundManager.MuteSound(false, false);
        }
        if(!m_rewarded)
            return;        
        switch (m_currentAds)
        {
            case GameConstant.ADS_HEART:
                MainController.BonusHeart(GameConstant.BONUS_HEART_ADS);
                break;
            case GameConstant.ADS_COINS_500:
                MainController.BonusCoin(GameConstant.BONUS_COIN_ADS);
                break;
            case GameConstant.ADS_DAILY_REWARD_X2:
                MainController.ClaimX2DailyReward();
                break;
            case GameConstant.ADS_TRY_SKIN:
                MainController.TrySkin((int)m_adsData);
                break;
            case GameConstant.ADS_RIVIVE:
                GameController.Rivive();
                break;
            case GameConstant.ADS_SKIP_LEVEL:
                MainController.SkipLevel();
                break;
            case GameConstant.ADS_SKIP_LEVEL_INGAME:
                GameController.SkipLevel();
                break;
            case GameConstant.ADS_X2_LEVEL_REWARD:
                MainController.X2LevelReward();
                break;
            // case GameConstant.ADS_GAME_HEART:
            //     MainController.UpdateHeart((int)m_adsData);
            //     break;
            case GameConstant.ADS_GAME_COIN:
                GameController.UpdateCoin((int)m_adsData);
                break;
            case GameConstant.ADS_GAME_MAGNET:
                GameController.ActiveMagnet();
                break;
            case GameConstant.ADS_GAME_TRY_SKIN:
                MainController.TrySkin((int)m_adsData);
                break;
            case GameConstant.ADS_GET_SKIN:
                MainController.BuyCoinSkin(0, (int)m_adsData);
                break;
            case GameConstant.ADS_LUCKY_SPIN:
                MainController.DoLuckySpin();
                break;
            case GameConstant.ADS_COIN_ONLY:
                MainController.UpdateCoin((int)m_adsData);
                break;
            case GameConstant.ADS_HEART_ONLY:
                MainController.UpdateHeart((int)m_adsData);
                break;
            case GameConstant.ADS_COIN_HEART:
                Dictionary<string, int> data = m_adsData as Dictionary<string, int>;
                MainController.UpdateCoin(data["coin"]);
                MainController.UpdateHeart(data["heart"]);
                break;
            case GameConstant.ADS_GET_KEY_TREASURE:
                MainController.UpdateKeyTreasure(3);
                break;
            case GameConstant.ADS_SPECIAL_SHOP_BUY_SKIN:
                MainController.BuyCoinSkin(0, (int)m_adsData);
                break;
            case GameConstant.ADS_SPECIAL_SHOP_BUY_COIN:
                MainController.UpdateCoin((int)m_adsData);
                break;
        }
        if(m_doAction != null)
            m_doAction?.Invoke();
        m_doAction = null;
    }

    void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
    {
        m_rewarded = true;
    }
    
    void RewardedVideoAdShowFailedEvent(IronSourceError error)
    {
        StopAllCoroutines();
        MainController.ActiveLoading(false);
        m_adsShowing = false;
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            SoundManager.MuteMusic(false, false);
            SoundManager.MuteSound(false, false);
        }   
        if(m_doAction != null)
            m_doAction?.Invoke();
        m_doAction = null;    
    }
    /////////////////////////////////////////////////////////////////////////////////
    public void Reset()
    {
        StopAllCoroutines();
        m_adsShowing = false;
        m_doAction = null;
        if(Application.platform == RuntimePlatform.IPhonePlayer)
        {
            SoundManager.MuteMusic(false, false);
            SoundManager.MuteSound(false, false);
        } 
        if(m_doAction != null)
            m_doAction?.Invoke();
        m_doAction = null; 
    }

    private void ImpressionSuccessEvent(IronSourceImpressionData impressionData)
    {
        TrackingManager.AdsImpression(impressionData);
    }
}
