﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using System;
using Random = UnityEngine.Random;

public class PopupLuckySpin : MonoBehaviour
{
    [SerializeField] private GameObject m_spin;
    //[SerializeField] private SkeletonGraphic m_spinBorder;
    [SerializeField] private Button m_buttonSpin;
    [SerializeField] private Button m_buttonSpinAds;
    [SerializeField] private TextMeshProUGUI m_textSpinCount;
    [SerializeField] private Collider2D m_arrow;
    [SerializeField] private Collider2D[] m_rewards;
    //
    [SerializeField] private GameObject m_result;
    [SerializeField] private TextMeshProUGUI m_textResult;
    [SerializeField] private GameObject m_buttonX2Reward;
    [SerializeField] private GameObject m_buttonNoThank;
    [SerializeField] private GameObject m_buttonGot;
    //
    [SerializeField] private GameObject m_resultRoll;
    [SerializeField] private TextMeshProUGUI m_textResultRoll;
    [SerializeField] private GameObject m_buttonNoThankRoll;
    

    private bool m_spining;
    private string m_reward;
    private GameObject m_soundSpin;

    [SerializeField] public GameObject m_btBack;


    [SerializeField] public GameObject arrow;
    [SerializeField] public Image slider;
    private const string KEY_TIME = "daily-rewards-time";
    private double m_time;
    private bool m_btAds=false;

    // toa do ban dau cua mui ten
    private int arrowPosX = 10;
     private  int arrowPosY = 55;

    void OnEnable()
    {
        MainController.updateSpinEvent += OnUpdateSpin;
        MainController.doLuckySpinEvent += OnSpin;
        //
        MainController.updateCountLuckySpinEvent += UpdateCountSpin;
        //
        m_spining = false;
        m_btBack.SetActive(true);
        //m_spinBorder.AnimationState.SetAnimation(0, "idle", true);
        m_spin.transform.localEulerAngles = Vector3.zero;
        m_result.SetActive(false);
        OnUpdateSpin();
        //
        LoadRewards();
      
    }

    void OnDisable()
    {
        MainController.updateSpinEvent -= OnUpdateSpin;
        MainController.doLuckySpinEvent -= OnSpin;
        //
        MainController.updateCountLuckySpinEvent -= UpdateCountSpin;
        // TinhTime();
    }

    private void OnSpin()
    {
        Spin();
       
    }

    private void OnUpdateSpin()
    {
        m_textSpinCount.text = "("+MainModel.spinQuatity+")";
        m_buttonSpin.gameObject.SetActive(MainModel.spinQuatity > 0);
        m_buttonSpinAds.gameObject.SetActive(MainModel.spinQuatity < 1);
        UpdateSpin();

    }

    public void SpinOnclick()
    {
       // m_btBack.SetActive(false);

        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.UpdateSpin(-1);
         Spin();
      
    }

    public void SpinAdsOnclick()
    {
        m_btAds = true;
        //m_btBack.SetActive(false);


        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if (Application.isEditor)
             MainController.DoLuckySpin();
        else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_LUCKY_SPIN);
        TrackingManager.LuckySpin();


    }

    public void CloseOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        m_spin.transform.DOKill();
        Destroy(m_soundSpin);
        MainController.ClosePopup(PopupType.LuckySpin);
        m_buttonX2Reward.SetActive(true);
    }

    public void X2RewardOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        switch(m_reward)
        {
            case "1":
                AdsManager.Instance.ShowVideoReward(GameConstant.ADS_COIN_ONLY, 100); 
                break;
            case "2":
                AdsManager.Instance.ShowVideoReward(GameConstant.ADS_HEART_ONLY, 1);
                break;
            case "3":
                AdsManager.Instance.ShowVideoReward(GameConstant.ADS_COIN_ONLY, 500); 
                break;
            case "4":
                AdsManager.Instance.ShowVideoReward(GameConstant.ADS_HEART_ONLY, 2);
                break;
            case "5":
                AdsManager.Instance.ShowVideoReward(GameConstant.ADS_COIN_ONLY, 700); 
                break;
            case "7":
                AdsManager.Instance.ShowVideoReward(GameConstant.ADS_COIN_ONLY, 1000);
                break;
            case "8":
                AdsManager.Instance.ShowVideoReward(GameConstant.ADS_HEART_ONLY, 3);
                break; 
        }      
        m_result.SetActive(false);    
        TrackingManager.LuckySpinX2();

        //
        ShowResubltSpin();
    }
   
    public void GotOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        m_result.SetActive(false);
        //
        ShowResubltSpin();
    }
    public void ClamOnClick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        m_resultRoll.SetActive(false);
        if (MainModel.totalspinCount >= 20)
            MainModel.totalspinCount = 0;

    }
    void Spin()
    {
      //  Debug.Log("Da goi ===================================================================");
        if(m_spining)
            return;
        m_soundSpin = SoundManager.PlaySound(GameConstant.AUDIO_SPIN_START, false);
        m_buttonSpin.interactable = false;
        m_buttonSpinAds.interactable = false; 
        m_spining = true;
        m_btBack.SetActive(false);


        //m_spinBorder.AnimationState.SetAnimation(0, "action", true);   
        int reward = Random.Range(60, 71);
        float angle = -reward*45;
        m_spin.transform.DOBlendableLocalRotateBy(new Vector3(0,0,angle), 6f, RotateMode.FastBeyond360).SetEase(Ease.InOutQuint).OnComplete(SpinComplete);

        //
        MainController.UpdateCountLuckySpin();
        //
       // Debug.Log(MainModel.AdsCount + "=============================================");
        UpdateTextArrow();
    }
    void UpdateCountSpin()
    {
       // Debug.Log(m_spining+ "Da goi 1 ===================================================================" + m_btAds);
        //
        if (m_btAds && m_spining)
        {
            MainController.UpdateCountSpinAdsDay();
            //MainModel.UpdateAdsCount(1);
            m_btAds = false;

        }
        //  Debug.Log( "=============================================== update count spin");
        UpdateSpin(); // update anim countspin
        UpdateTextArrow(); // update text count spin
    }

    void SpinComplete()
    {

        m_spining = false;
        m_btBack.SetActive(true);
        //m_spinBorder.AnimationState.SetAnimation(0, "idle", true);
        m_buttonSpin.interactable = true;
        m_buttonSpinAds.interactable = true;
        foreach (Collider2D child in m_rewards)
        {
            if(m_arrow.bounds.Intersects(child.bounds))
            {
                m_reward = child.name;
                break;
            }
        }   
        ShowResult(m_reward); 
    }
    void UpdateTextArrow()
    {
       
        slider.GetComponent<Image>().fillAmount = MainModel.totalspinCount * 0.05f;
        m_buttonSpinAds.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "SPIN\n" + MainModel.spinAdsDayCount + "/5";
        arrow.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = MainModel.totalspinCount.ToString();
        arrow.GetComponent<RectTransform>().anchoredPosition = new Vector2(arrowPosX + MainModel.totalspinCount * 34, arrowPosY);
    }
    void ShowResubltSpin()
    {
        int a = MainModel.totalspinCount;
        if (a == 2 || a == 5 || a == 10 || a == 20)
        {
            m_resultRoll.SetActive(true);
            GameObject panel = m_resultRoll.transform.Find("contentRoll").gameObject;
            panel.transform.localScale = Vector3.zero;
            panel.transform.DOScale(Vector3.one, 0.25f).SetEase(Ease.OutBack);

            Transform iconHeartBig = panel.transform.Find("icon-heart-big-Roll");   
            Transform iconCoinBig = panel.transform.Find("icon-coin-big-Roll");
            Image iconSkin = panel.transform.Find("icon-skin-Roll").GetComponent<Image>();
            iconHeartBig.gameObject.SetActive(false);
            iconCoinBig.gameObject.SetActive(false);
            iconSkin.gameObject.SetActive(false);

            m_buttonX2Reward.SetActive(false);
            m_buttonNoThankRoll.SetActive(true);

            switch (a.ToString())
            {
                case "2":
                    MainController.UpdateCoin(500);
                    iconCoinBig.gameObject.SetActive(true);
                    m_textResultRoll.text = "YOU GOT\n+500 COIN";
                    break;
                case "5":
                    MainController.UpdateHeart(5);
                    iconHeartBig.gameObject.SetActive(true);
                    m_textResultRoll.text = "YOU GOT\n+4 HEART";
                    break;
                case "10":

                    iconSkin.gameObject.SetActive(true);
                    GameAssetManager.api.GetAvatar(28, iconSkin);
                    MainController.BuyCoinSkin(0, 28);
                    iconSkin.SetNativeSize();
                    m_textResultRoll.text = "YOU GOT\n 1 SKIN";
                    break;
                case "20":
                    iconSkin.gameObject.SetActive(true);
                    GameAssetManager.api.GetAvatar(32, iconSkin);
                    MainController.BuyCoinSkin(0, 32);
                    iconSkin.SetNativeSize();
                    m_textResultRoll.text = "YOU GOT\n 1 SKIN";
                    break;
            }
        }

    }

    void ShowResult(string reward)
    {
       // m_btBack.SetActive(true);

        SoundManager.PlaySound(GameConstant.AUDIO_SPIN_WIN, false);
        m_result.SetActive(true);
        GameObject panel = m_result.transform.Find("content").gameObject;
        panel.transform.localScale = Vector3.zero;
        panel.transform.DOScale(Vector3.one, 0.25f).SetEase(Ease.OutBack);
        //
        Transform iconHeart = panel.transform.Find("icon-heart");
        Transform iconHeartBig = panel.transform.Find("icon-heart-big");
        Transform iconCoin = panel.transform.Find("icon-coin");
        Transform iconCoinBig = panel.transform.Find("icon-coin-big");
        Transform iconCoinHeart = panel.transform.Find("icon-coin-heart");
        Image iconSkin = panel.transform.Find("icon-skin").GetComponent<Image>();
        //
        iconHeart.gameObject.SetActive(false);
        iconHeartBig.gameObject.SetActive(false);
        iconCoin.gameObject.SetActive(false);
        iconCoinBig.gameObject.SetActive(false);
        iconCoinHeart.gameObject.SetActive(false);
        iconSkin.gameObject.SetActive(false);
        //
        m_buttonX2Reward.SetActive(true);
        m_buttonNoThank.SetActive(true);
        m_buttonGot.SetActive(false);
        switch(reward)
        {
            case "1":
                MainController.UpdateCoin(100);
                iconCoin.gameObject.SetActive(true);
                m_textResult.text = "YOU GOT\n+100 COINS"; 
                break;
            case "2":
                MainController.UpdateHeart(1);
                iconHeart.gameObject.SetActive(true);
                m_textResult.text = "YOU GOT\n+1 HEART";
                break;
            case "3":
                MainController.UpdateCoin(500);
                iconCoin.gameObject.SetActive(true);
                m_textResult.text = "YOU GOT\n+500 COINS"; 
                break;
            case "4":
                MainController.UpdateHeart(2);
                iconHeart.gameObject.SetActive(true);
                m_textResult.text = "YOU GOT\n+2 HEART";
                break;
            case "5":
                MainController.UpdateCoin(700);
                iconCoin.gameObject.SetActive(true);
                m_textResult.text = "YOU GOT\n+700 COINS"; 
                break;
            case "6":
                m_buttonX2Reward.SetActive(false);
                m_buttonNoThank.SetActive(false);
                m_buttonGot.SetActive(true);
                int skin = UnlockRandomSkin();
                iconSkin.gameObject.SetActive(true);
                GameAssetManager.api.GetAvatar(skin, iconSkin);
                iconSkin.SetNativeSize();
                m_textResult.text = "YOU GOT\n 1 SKIN"; 
                break;
            case "7":
                MainController.UpdateCoin(1000);
                iconCoin.gameObject.SetActive(true);
                m_textResult.text = "YOU GOT\n+1000 COINS";
                break;
            case "8":
                MainController.UpdateHeart(3);
                iconHeart.gameObject.SetActive(true);
                m_textResult.text = "YOU GOT\n+3 HEART";
                break;  
        }
        //
        UpdateSpin();
    }

    int UnlockRandomSkin()
    {
        int ran = Random.Range(0, MainModel.storeConfig.premium_skins.Count);
        MainController.BuyCoinSkin(0, MainModel.storeConfig.premium_skins[ran].skin);
        return MainModel.storeConfig.premium_skins[ran].skin;
    }

    public void UpdateSpin()
    {
        slider.GetComponent<Image>().fillAmount = MainModel.totalspinCount * 0.05f;
        if (MainModel.spinAdsDayCount >= 5)
        {
            m_buttonSpinAds.interactable = false;
           // m_btBack.gameObject.SetActive(true);
           
        }
        if(m_buttonSpinAds.interactable==true)
        {
            m_buttonSpinAds.GetComponent<Animation>().Play("buttonJump");
        }
        else
        {
            m_buttonSpinAds.GetComponent<Animation>().Stop();
        }
       // UpdateTextArrow();
    }
    void LoadRewards()
    {
        arrow.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = MainModel.totalspinCount.ToString();
        arrow.GetComponent<RectTransform>().anchoredPosition = new Vector2(arrowPosX+MainModel.totalspinCount*34, arrowPosY);

        m_buttonSpinAds.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "SPIN\n" + MainModel.spinAdsDayCount + "/5";

       
       // m_time = double.Parse(PlayerPrefs.GetString(KEY_TIME, "0"));
       if (MainModel.spinQuatity > 0)  //  if (HasRewardTime())
        {
        
            MainModel.spinAdsDayCount = 0;
            m_buttonSpinAds.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = "SPIN\n" + MainModel.spinAdsDayCount + "/5";
            m_buttonSpinAds.interactable = true;
        }
    }
    void SaveRewards()
    {
        PlayerPrefs.SetString(KEY_TIME, m_time + "");
    }
    void TinhTime()
    {
        DateTime startTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        DateTime now = DateTime.UtcNow;
        m_time = (now - startTime).TotalSeconds;
        //Debug.Log(m_time);
        SaveRewards();
    }
    public bool HasRewardTime()
    {
        DateTime lastTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        lastTime = lastTime.AddSeconds(m_time).ToLocalTime();
        DateTime now = DateTime.Now;
       // return now.Minute != lastTime.Minute;
         return now.Year != lastTime.Year || now.DayOfYear != lastTime.DayOfYear; //|| now.Minute!= lastTime.Minute;

    }
  
}
