﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using TMPro;
using UnityEngine;

public class TabStoreRescueSkins : MonoBehaviour
{
    [SerializeField] private GameObject m_item;
    [SerializeField] private Transform m_content;
    [SerializeField] private GameObject m_buttonSelect;
    [SerializeField] private GameObject m_buttonGetFree;
    [SerializeField] private GameObject m_buttonTry;
    [SerializeField] private GameObject m_buttonAds;
    [SerializeField] private SkeletonGraphic m_spineRed;
    [SerializeField] private SkeletonGraphic m_spineBlue;
    
    private StoreRescueSkinItem m_selected;

    void Awake()
    {
        MainController.updateStoreSkinEvent += OnUpdate;
        MainController.changeSkinEvent += OnSelect;
    }

    void OnDestroy()
    {
        MainController.updateStoreSkinEvent -= OnUpdate;
        MainController.changeSkinEvent -= OnSelect;
    }

    private void OnUpdate(int skin)
    {
        if(m_selected == null)
            return;
        if(m_selected.config.skin != skin)
            return;
        SelectItem();
    }

    private void OnSelect(int oldSkin, int newSkin)
    {
        if(m_selected == null)
            return;
        if(m_selected.config.skin != oldSkin && m_selected.config.skin != newSkin)
            return;
        SelectItem();
    }

    public void ItemOnclick(StoreRescueSkinItem item)
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(m_selected != null)
            m_selected.Focus(false);
        item.Focus(true);
        m_selected = item;
        SelectItem();
    }

    public void Init(List<RescueSkinConfig> config)
    {
        foreach (Transform child in m_content)
        {
            GameObject.Destroy(child.gameObject);
        }

        m_selected = null;
        StoreRescueSkinItem first = null;
        int mapLevel = MainModel.GetMapLevel();
        foreach (RescueSkinConfig skinConfig in config)
        {
            GameObject go = Instantiate(m_item);
            go.SetActive(true);
            go.transform.SetParent(m_content, false);
            StoreRescueSkinItem comp = go.GetComponent<StoreRescueSkinItem>();
            bool selected = skinConfig.skin == MainModel.currentSkin;
            comp.Init(skinConfig, selected, MainModel.unlockSkins.Contains(skinConfig.skin) || skinConfig.level < mapLevel);
            if(selected)
                ItemOnclick(comp);
            if(first == null)
                first = comp;
        }
        if(m_selected == null && first != null)
            ItemOnclick(first);

    }

    private void SelectItem()
    {
        if(m_selected == null)
            return;
        bool unlock = MainModel.unlockSkins.Contains(m_selected.config.skin);
        bool active = m_selected.config.level < MainModel.GetMapLevel();
        bool selected = MainModel.currentSkin == m_selected.config.skin;
        if (unlock)
        {
            m_buttonSelect.SetActive(!selected);
            m_buttonGetFree.SetActive(false);
            m_buttonTry.SetActive(false);
            if(m_buttonAds != null)
            {
                m_buttonAds.SetActive(true);
                m_buttonAds.transform.Find("text-coin-ads").GetComponent<TextMeshProUGUI>().text = GameUtils.CoinToString(m_selected.config.coin_ads);
            }
        }
        else
        {
            m_buttonSelect.SetActive(false);
            m_buttonGetFree.SetActive(active);
            m_buttonTry.SetActive(true);
            if (m_buttonAds != null)
                m_buttonAds.SetActive(false);
        }
        //
        SkinInfo sk = MainModel.skinConfig.GetSkin(m_selected.config.skin);
        if(m_spineRed != null)
             m_spineRed.SetSkin(sk.red);
        if(m_spineBlue != null)
            m_spineBlue.SetSkin(sk.blue);
    }

    public void OnClickGetFree()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(Application.isEditor)
            MainController.BuyCoinSkin(0, m_selected.config.skin);
        else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_GET_SKIN, m_selected.config.skin);
        //
        TrackingManager.WatchAdsUnlockSkin(m_selected.config.skin);
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickTry()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        AdsManager.Instance.ShowVideoReward(GameConstant.ADS_TRY_SKIN, m_selected.config.skin);
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickAds()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        AdsManager.Instance.ShowVideoReward(GameConstant.ADS_COINS_500);
      
    }

    public void OnclickSelect()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.SelectSkin(m_selected.config.skin);
        ChangeSkinUI.ischangeSkin = false;
    }
}
