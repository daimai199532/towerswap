﻿
using System.Collections.Generic;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TabStoreCoinSkins : MonoBehaviour
{
    [SerializeField] private GameObject m_item;
    [SerializeField] private Transform m_content;    
    [SerializeField] private GameObject m_buttonSelect;
    [SerializeField] private Button m_buttonBuy;
    [SerializeField] private GameObject m_buttonTry;
    [SerializeField] private GameObject m_buttonAds;
    [SerializeField] private SkeletonGraphic m_spineRed;
    [SerializeField] private SkeletonGraphic m_spineBlue;

    private StoreCoinSkinItem m_selected;

    void Awake()
    {
        MainController.updateStoreSkinEvent += OnUpdate;
        MainController.changeSkinEvent += OnSelect;
    }

    void OnDestroy()
    {
        MainController.updateStoreSkinEvent -= OnUpdate;
        MainController.changeSkinEvent -= OnSelect;
    }

    private void OnUpdate(int skin)
    {
        if(m_selected == null)
            return;
        if(m_selected.config.skin != skin)
            return;
        SelectItem();
    }

    private void OnSelect(int oldSkin, int newSkin)
    {
        if(m_selected == null)
            return;
        if(m_selected.config.skin != oldSkin && m_selected.config.skin != newSkin)
            return;
        SelectItem();
    }
    
    public void Init(List<CoinSkinConfig> config)
    {
        foreach (Transform child in m_content)
        {
            GameObject.Destroy(child.gameObject);
        }
        m_selected = null;
        StoreCoinSkinItem first = null;
        foreach (CoinSkinConfig skinConfig in config)
        {
            GameObject go = Instantiate(m_item);
            go.SetActive(true);
            go.transform.SetParent(m_content, false);
            StoreCoinSkinItem comp = go.GetComponent<StoreCoinSkinItem>();
            bool selected = skinConfig.skin == MainModel.currentSkin;
            comp.Init(skinConfig, selected);
            if(selected)
                ItemOnclick(comp);
            if(first == null)
                first = comp;
        }
        if(m_selected == null && first != null)
            ItemOnclick(first);
       
    }

    public void ItemOnclick(StoreCoinSkinItem item)
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(m_selected != null)
            m_selected.Focus(false);
        item.Focus(true);
        m_selected = item;
        SelectItem();
    }

    public void SelectItem()
    {
        if(m_selected == null)
            return;
        bool active = MainModel.unlockSkins.Contains(m_selected.config.skin);
        bool selected = MainModel.currentSkin == m_selected.config.skin;
        m_buttonSelect.SetActive(active && !selected);
        m_buttonBuy.gameObject.SetActive(!active);
        m_buttonTry.SetActive(!active);
        if(m_buttonAds != null)
        {
            m_buttonAds.SetActive(active);
            m_buttonAds.transform.Find("text-coin-ads").GetComponent<TextMeshProUGUI>().text = GameUtils.CoinToString(m_selected.config.coin_ads);
        }
        m_buttonBuy.interactable = m_selected.config.price <= MainModel.totalCoin;
        m_buttonBuy.transform.Find("text-price").GetComponent<TextMeshProUGUI>().text = GameUtils.CoinToString(m_selected.config.price);            
        //
        SkinInfo sk = MainModel.skinConfig.GetSkin(m_selected.config.skin);
        if(m_spineRed != null)
             m_spineRed.SetSkin(sk.red);
        if(m_spineBlue != null)
            m_spineBlue.SetSkin(sk.blue);
    }

    public void OnClickBuy()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.BuyCoinSkin(m_selected.config.price, m_selected.config.skin);
    }

    public void OnclickTry()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        AdsManager.Instance.ShowVideoReward(GameConstant.ADS_TRY_SKIN, m_selected.config.skin);
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickAds()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        AdsManager.Instance.ShowVideoReward(GameConstant.ADS_COINS_500);
    }

    public void OnclickSelect()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.SelectSkin(m_selected.config.skin);
        ChangeSkinUI.ischangeSkin = false;
    }


}
