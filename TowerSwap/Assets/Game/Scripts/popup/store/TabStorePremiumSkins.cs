﻿
using System.Collections.Generic;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TabStorePremiumSkins : MonoBehaviour
{
    [SerializeField] private GameObject m_item;
    [SerializeField] private Transform m_content;

    [SerializeField] private GameObject m_buttonSelect;
    [SerializeField] private Button m_buttonBuy;
    [SerializeField] private GameObject m_buttonTry;
    [SerializeField] private GameObject m_buttonAds;
    [SerializeField] private SkeletonGraphic m_spineRed;
    [SerializeField] private SkeletonGraphic m_spineBlue;

    private StorePremiumSkinItem m_selected;
    private List<PremiumSkinConfig> m_config;

    void Awake()
    {
        MainController.updateStoreSkinEvent += OnUpdate;
        MainController.changeSkinEvent += OnSelect;
        IAPManager.purchaseResultEvent += OnPurchaseResult;
    }    

    void OnDestroy()
    {
        MainController.updateStoreSkinEvent -= OnUpdate;
        MainController.changeSkinEvent -= OnSelect;
        IAPManager.purchaseResultEvent -= OnPurchaseResult;
    }

    private void OnPurchaseResult(string productId, bool status)
    {
        if(!status || productId != IAPManager.PRODUCT_ID_VIP_SUBSCRIPTION)
            return;
        Init(m_config);
    }

    private void OnUpdate(int skin)
    {
        if(m_selected == null)
            return;
        if(m_selected.config.skin != skin)
            return;
        SelectItem();
    }

    private void OnSelect(int oldSkin, int newSkin)
    {
        if(m_selected == null)
            return;
        if(m_selected.config.skin != oldSkin && m_selected.config.skin != newSkin)
            return;
        SelectItem();
    }

    public void ItemOnclick(StorePremiumSkinItem item)
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(m_selected != null)
            m_selected.Focus(false);
        item.Focus(true);
        m_selected = item;
        SelectItem();
    }
    
    public void Init(List<PremiumSkinConfig> config)
    {
        m_config = config;
        foreach (Transform child in m_content)
        {
            GameObject.Destroy(child.gameObject);
        }
        m_selected = null;
        StorePremiumSkinItem first = null;
        foreach (PremiumSkinConfig skinConfig in config)
        {
            GameObject go = Instantiate(m_item);
            go.SetActive(true);
            go.transform.SetParent(m_content, false);
            StorePremiumSkinItem comp = go.GetComponent<StorePremiumSkinItem>();
            bool selected = skinConfig.skin == MainModel.currentSkin;
            go.GetComponent<StorePremiumSkinItem>().Init(skinConfig, MainModel.subscription || MainModel.unlockSkins.Contains(skinConfig.skin), selected);
            if(selected)
                ItemOnclick(comp);
            if(first == null)
                first = comp;
        }
        if(m_selected == null && first != null)
            ItemOnclick(first);
    }

    private void SelectItem()
    {
        bool selected = MainModel.currentSkin == m_selected.config.skin;
        bool active = MainModel.subscription;
        bool activeLock = MainModel.unlockSkins.Contains(m_selected.config.skin);
      
        m_buttonBuy.gameObject.SetActive(!active && !activeLock);
        m_buttonSelect.SetActive(active && !selected || activeLock && !selected);
        m_buttonTry.SetActive(!active && !activeLock);
        if (m_buttonAds != null)
        {
            m_buttonAds.SetActive(active);
            m_buttonAds.transform.Find("text-coin-ads").GetComponent<TextMeshProUGUI>().text = GameUtils.CoinToString(m_selected.config.coin_ads);
        }
        SkinInfo sk = MainModel.skinConfig.GetSkin(m_selected.config.skin);
        if(m_spineRed != null)
             m_spineRed.SetSkin(sk.red);
        if(m_spineBlue != null)
            m_spineBlue.SetSkin(sk.blue);
    }

    public void OnClickBuy()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.OpenPopup(PopupType.Subscription);
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickTry()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        AdsManager.Instance.ShowVideoReward(GameConstant.ADS_TRY_SKIN, m_selected.config.skin);
        ChangeSkinUI.ischangeSkin = false;
    }

    public void OnclickAds()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        AdsManager.Instance.ShowVideoReward(GameConstant.ADS_COINS_500);
       
    }

    public void OnclickSelect()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.SelectSkin(m_selected.config.skin);
        ChangeSkinUI.ischangeSkin = false;
    }

}
