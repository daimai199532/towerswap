﻿
using Spine.Unity;
using TMPro;
using UnityEngine;

public class StoreRescueSkinItem : MonoBehaviour
{
    [SerializeField] private SkeletonGraphic m_spineRed;
    [SerializeField] private SkeletonGraphic m_spineBlue;
    [SerializeField] private GameObject m_check;  
    [SerializeField] private GameObject m_lock;
    [SerializeField] private TextMeshProUGUI m_textUnlock;

    [HideInInspector]public RescueSkinConfig config;
    

    public void Init(RescueSkinConfig config, bool selected, bool unlock)
    {
        this.config = config;
        if (m_check != null)
            m_check.SetActive(selected);
        if (unlock)
        {
            m_textUnlock.gameObject.SetActive(false);            
        }
        else
        {
            m_textUnlock.text = "UNLOCK AT LEVEL " + config.level;
            m_textUnlock.gameObject.SetActive(true);
        }
        m_lock.SetActive(!unlock);
        SkinInfo sk = MainModel.skinConfig.GetSkin(config.skin);
        if(m_spineRed != null)
             m_spineRed.SetSkin(sk.red);
        if(m_spineBlue != null)
            m_spineBlue.SetSkin(sk.blue);
    }
    public void Focus(bool isFocus)
    {
        m_check.SetActive(isFocus);
    }
}
