﻿using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using UnityEngine;

public class BallStoreUi : MonoBehaviour
{
    [SerializeField] private SkeletonGraphic m_spine;
    private const int S_MAX_COUNT_ANIM = 2;
    private const float S_MAX_TIME_IDLE = 5f;


    private List<string> m_animsHome;
    private int m_count;

    void Start()
    {
        m_animsHome = new List<string>();
        m_animsHome.Add(GameConstant.PLAYER_ANIMATION_LAUGH);
        m_animsHome.Add(GameConstant.PLAYER_ANIMATION_LAUGH);
        m_animsHome.Add(GameConstant.PLAYER_ANIMATION_LIKE);
        //
        m_spine.AnimationState.Complete += OnAnimComplete;
        StartCoroutine(RandomAnim());
    }

    void OnDestroy()
    {
        m_spine.AnimationState.Complete -= OnAnimComplete;
    }

    private void OnAnimComplete(TrackEntry trackentry)
    {
        if (trackentry.Animation.Name == GameConstant.PLAYER_ANIMATION_IDLE_IN_HOME)
            return;
        m_count++;
        if (m_count >= S_MAX_COUNT_ANIM)
            PlayNormal();
    }

    IEnumerator RandomAnim()
    {
        yield return new WaitForSeconds(S_MAX_TIME_IDLE);
        m_count = 0;
        int rand = Random.Range(0, m_animsHome.Count);
        m_spine.AnimationState.SetAnimation(0, m_animsHome[rand], true);
    }



    void PlayNormal()
    {
        m_spine.AnimationState.SetAnimation(0, GameConstant.PLAYER_ANIMATION_IDLE_IN_HOME, true);
        StartCoroutine(RandomAnim());
    }
}
