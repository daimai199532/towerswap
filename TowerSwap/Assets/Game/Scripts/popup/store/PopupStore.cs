﻿
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupStore : MonoBehaviour
{
    [SerializeField] private Toggle m_togglePackage;
    [SerializeField] private GameObject m_tabPackage;
    [SerializeField] private GameObject m_tabPremiumSkins;
    [SerializeField] private GameObject m_tabCoinSkins;
    [SerializeField] private GameObject m_tabRescueSkin;
    [SerializeField] private TextMeshProUGUI m_textPackage;
    [SerializeField] private TextMeshProUGUI m_textPremiumSkins;
    [SerializeField] private TextMeshProUGUI m_textCoinSkins;
    [SerializeField] private TextMeshProUGUI m_textRescueSkins;
    [SerializeField] private TextMeshProUGUI m_textCoin;
    [SerializeField] private Color m_activeColor;
    [SerializeField] private Color m_deactiveColor;

    void OnEnable()
    {
        MainController.updateCoinEvent += OnUpdateCoin;
        MainController.UpdateCoin(0);
        //
        StartCoroutine(DelayStart());
    }

    void OnDisable()
    {
        MainController.updateCoinEvent -= OnUpdateCoin;
    }

    IEnumerator DelayStart()
    {
        yield return null;
        yield return null;
        m_togglePackage.isOn = true;
    }

    private void OnUpdateCoin(int coin)
    {
        m_textCoin.text = GameUtils.CoinToString(coin);
    }

    public void TabPackageOnclick(Toggle toggle)
    {
        if(!toggle.isOn)
            return;
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        HideAllTab();
        m_textPackage.color = m_activeColor;
        m_tabPackage.SetActive(true);
        
    }

    public void TabPremiumSkinOnclick(Toggle toggle)
    {
        if(!toggle.isOn)
            return;
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        HideAllTab();
        m_textPremiumSkins.color = m_activeColor;
        m_tabPremiumSkins.SetActive(true);
        m_tabPremiumSkins.GetComponent<TabStorePremiumSkins>().Init(MainModel.storeConfig.premium_skins);
    }

    public void TabCoinSkinOnclick(Toggle toggle)
    {
        if(!toggle.isOn)
            return;
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        HideAllTab();
        m_textCoinSkins.color = m_activeColor;
        m_tabCoinSkins.SetActive(true);
        m_tabCoinSkins.GetComponent<TabStoreCoinSkins>().Init(MainModel.storeConfig.coin_skins);
    }

    public void TabRescueSkinOnclick(Toggle toggle)
    {
        if(!toggle.isOn)
            return;
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        HideAllTab();
        m_textRescueSkins.color = m_activeColor;
        m_tabRescueSkin.SetActive(true);
        m_tabRescueSkin.GetComponent<TabStoreRescueSkins>().Init(MainModel.storeConfig.rescue_skins);
    }

    public void CloseOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.ClosePopup(PopupType.Store);
    }

    void HideAllTab()
    {
        m_textPackage.color = m_deactiveColor;
        m_textPremiumSkins.color = m_deactiveColor;
        m_textCoinSkins.color = m_deactiveColor;
        m_textRescueSkins.color = m_deactiveColor;
        m_tabPackage.SetActive(false);
        m_tabPremiumSkins.SetActive(false);
        m_tabRescueSkin.SetActive(false);
        m_tabCoinSkins.SetActive(false);
    }


}
