﻿using Spine;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class StoreCoinSkinItem : MonoBehaviour
{
    [SerializeField] private SkeletonGraphic m_spineRed;
    [SerializeField] private SkeletonGraphic m_spineBlue;
    [SerializeField] private GameObject m_check;

    [HideInInspector]public CoinSkinConfig config;

    public void Init(CoinSkinConfig config, bool selected)
    {
        this.config = config;
        if(m_check != null)
            m_check.SetActive(selected);
        SkinInfo sk = MainModel.skinConfig.GetSkin(config.skin);
        if(m_spineRed != null)
             m_spineRed.SetSkin(sk.red);
        if(m_spineBlue != null)
            m_spineBlue.SetSkin(sk.blue);
      
    }

    public void Focus(bool isFocus)
    {
        m_check.SetActive(isFocus);
    }
}
