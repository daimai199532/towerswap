﻿using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupTreasure : MonoBehaviour
{
    [SerializeField] private Image m_imageSkin;
    [SerializeField] private GameObject m_buttonGet;
    [SerializeField] private GameObject m_buttonBack; 
    [SerializeField] private GameObject[] m_objectItems;
    [SerializeField] private Image[] m_imageKeys;

    [SerializeField] private Sprite[] m_spriteKeys;
    [SerializeField] private Sprite[] m_spriteGifts;

    private int m_skin;
    private List<int> m_gifts;//skin = 1, heart = 2, coin = 3
    private bool m_canGetAds;

    void OnEnable()
    {
        m_skin = GetRandomSkin();

        Debug.Log(m_skin + " : IDSKin");
        m_gifts = new List<int>();
        m_gifts.Add(1);//add skin first
        for(int  i = 1; i < m_objectItems.Length; i++)
        {
            int ran = Random.Range(0,10);
            m_gifts.Add(ran < 5 ? 2 : 3);
        }
        //
        ResetUI();
        //
        MainController.updateKeyTreasureEvent += OnUpdateKey;
        //
        TrackingManager.TreasureOpen();
    }

    void OnDisable()
    {
        MainController.updateKeyTreasureEvent -= OnUpdateKey;
    }

    private void OnUpdateKey()
    {
        m_canGetAds = m_gifts.Count > 0;
        if(!m_buttonBack.activeSelf)
            m_buttonBack.SetActive(MainModel.keyQuantity < 1);
        m_buttonGet.SetActive(MainModel.keyQuantity < 1 && m_canGetAds);
        m_imageKeys[0].transform.parent.gameObject.SetActive(MainModel.keyQuantity > 0);
        for(int i = 0; i < m_imageKeys.Length; i++)
        {
            m_imageKeys[i].sprite = i < MainModel.keyQuantity ? m_spriteKeys[0] : m_spriteKeys[1];
        }        
    }

    void ResetUI()
    {
        m_canGetAds = true;
        m_buttonBack.SetActive(false);
        OnUpdateKey();
        GameAssetManager.api.GetAvatar(m_skin, m_imageSkin);
        
        foreach(GameObject obj in m_objectItems)
        {
            obj.GetComponent<PopupTreasureItem>().Init();           
        }
    }

    public void ItemOnclick(GameObject go)
    {    
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(MainModel.keyQuantity < 1)
            return;            
        go.GetComponent<Button>().interactable = false;        
        //random gift
        int ran = Random.Range(0, m_gifts.Count);
        int giftType = m_gifts[ran];
        m_gifts.RemoveAt(ran);
        MainController.UpdateKeyTreasure(-1);
        // 
        PopupTreasureItem item = go.GetComponent<PopupTreasureItem>();
        switch(giftType)
        {
            case 1://skin
                item.Open(m_imageSkin.sprite, 0);
                MainController.BuyCoinSkin(0, m_skin);
                break;
            case 2://heart
                int heart = Random.Range(1, 4);
                MainController.UpdateHeart(heart);
                item.Open(heart == 1 ? m_spriteGifts[0] : m_spriteGifts[1], heart);
                break;
            case 3://coin
                int coin  = Random.Range(100, 501);
                coin = coin/100*100;
                MainController.UpdateCoin(coin);
                item.Open(m_spriteGifts[2], coin);
                break;
        }  
    }

    public void BackOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.ClosePopup(PopupType.Treasure);
    }

    public void GetKeyOnclick()
    {
        if(!m_canGetAds)
            return;
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);        
        if(Application.isEditor)
            MainController.UpdateKeyTreasure(3);
        else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_GET_KEY_TREASURE);
        TrackingManager.KeyTreasure();
    }

    int GetRandomSkin()
    {
        List<PremiumSkinConfig> skins = MainModel.storeConfig.premium_skins;
        List<PremiumSkinConfig> remain = new List<PremiumSkinConfig>();
        foreach(PremiumSkinConfig skin in skins)
        {
            if(MainModel.unlockSkins.Contains(skin.skin))
                continue;
            remain.Add(skin);
        }
        int ran = Random.Range(0, remain.Count);
        if(ran >= remain.Count)
            return skins[0].skin;
        return remain[ran].skin;
    }
}
