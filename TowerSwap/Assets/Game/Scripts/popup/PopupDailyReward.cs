﻿
using System;
using UnityEngine;
using UnityEngine.UI;

public class PopupDailyReward : MonoBehaviour
{
    private const string KEY_COUNT = "daily-rewards-count";
    private const string KEY_TIME = "daily-rewards-time";

    [SerializeField] private GameObject[] m_days;
    [SerializeField] private Button m_buttonClaim;
    [SerializeField] private Button m_buttonClaimX2;
    [SerializeField] private Sprite m_grayBackground;

    private int m_count;
    private double m_time;

    void OnEnable()
    {
        MainController.claimX2DailyRewardEvent += OnClaimX2;
        LoadRewards();
        InitRewards();
    }

    void OnDisable()
    {
        MainController.claimX2DailyRewardEvent -= OnClaimX2;
    }

    private void OnClaimX2()
    {
        ClaimReward(2);
        DateTime startTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        DateTime now = DateTime.UtcNow;
        m_time = (now - startTime).TotalSeconds;
        SaveRewards();
        InitRewards();
    }

    public void CloseOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.ClosePopup(PopupType.DailyReward);
    }

    public void ClaimOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(!HasReward())
            return;
        ClaimReward(1);
        DateTime startTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        DateTime now = DateTime.UtcNow;
        m_time = (now - startTime).TotalSeconds;
        SaveRewards();
        InitRewards();
    }

    public void ClaimX2Onclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(!HasReward())
            return;
        AdsManager.Instance.ShowVideoReward(GameConstant.ADS_DAILY_REWARD_X2);
    }

    void ClaimReward(int ratio)
    {
        switch (m_count)
        {
            case 0:
                MainController.UpdateCoin(1000*ratio);
                break;
            case 1:
                MainController.BuyCoinSkin(0, 30);
                break;
            case 2:
                MainController.UpdateHeart(5*ratio);
                break;
            case 3:
                MainController.UpdateCoin(5000*ratio);
                break;
            case 4:
                MainController.BuyCoinSkin(0, 27);
                break;
            case 5:
                MainController.UpdateHeart(10*ratio);
                break;
            case 6:
                MainController.UpdateHeart(10*ratio);
                MainController.UpdateCoin(10000*ratio);
                MainController.BuyCoinSkin(0, 4);
                break;
        }
    }

    void InitRewards()
    {
        bool active = HasReward();
        m_count += active ? 1 : 0;
        for (int i = 0; i < m_days.Length; i++)
        {
            ActiveDayGlow(m_days[i], i == m_count);
            if (i == m_count)
            {
                ActiveDayCheck(m_days[i], !active);
            }
            else
            {
                ActiveDayCheck(m_days[i],i < m_count);
                if(i != (m_days.Length -1) && i < m_count)
                    ActiveGray(m_days[i]);
            }
        }
        //
        m_buttonClaim.gameObject.SetActive(active);
        m_buttonClaimX2.gameObject.SetActive(active);
    }

    void LoadRewards()
    {
        m_count = PlayerPrefs.GetInt(KEY_COUNT, -1);
        m_time = double.Parse(PlayerPrefs.GetString(KEY_TIME, "0"));
    }

    void SaveRewards()
    {
        PlayerPrefs.SetInt(KEY_COUNT, m_count);
        PlayerPrefs.SetString(KEY_TIME, m_time + "");
        PlayerPrefs.Save();
    }

    void ActiveDayGlow(GameObject go, bool active)
    {
        GameObject glow = go.transform.Find("glow").gameObject;
        if(glow == null)
            return;
        glow.SetActive(active);
    }

    void ActiveDayCheck(GameObject go, bool active)
    {
        GameObject check = go.transform.Find("check").gameObject;
        if(check == null)
            return;
        check.SetActive(active);
    }

    void ActiveGray(GameObject go)
    {
        go.GetComponent<Image>().sprite = m_grayBackground;
    }

    public bool HasReward()
    {
        DateTime lastTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        lastTime = lastTime.AddSeconds(m_time).ToLocalTime();
        DateTime now = DateTime.Now;
        return now.Year != lastTime.Year || now.DayOfYear != lastTime.DayOfYear;
    }
}
