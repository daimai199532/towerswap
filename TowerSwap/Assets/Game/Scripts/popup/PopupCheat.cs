﻿
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PopupCheat : MonoBehaviour
{
    [SerializeField] private TMP_InputField m_input;

    public void CloseOnlick()
    {
        gameObject.SetActive(false);
    }

    public void OkOnclick()
    {
        int level = 0;
        bool parse = int.TryParse(m_input.text, out level);
        if(!parse)
            return;
        MainModel.SaveMapLevel(level-1);
        CloseOnlick();
        SceneManager.LoadScene(sceneName: SceneManager.GetActiveScene().name );
    }
}
