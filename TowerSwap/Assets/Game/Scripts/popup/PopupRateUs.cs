﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupRateUs : MonoBehaviour
{
    [SerializeField] private GameObject m_content1;
    [SerializeField] private GameObject m_content2;
    [SerializeField] private Image[] m_stars;


    void OnEnable()
    {
        Init();
    }

    void Init()
    {
        m_content1.SetActive(true);
        m_content2.SetActive(false);
        foreach(Image star in m_stars)
        {
            star.color = Color.black;
        }
    }

    public void Star1Onclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        StopAllCoroutines();
        ActiveStar(0);
    }
    public void Star2Onclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        StopAllCoroutines();
        ActiveStar(1);
    }
    public void Star3Onclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        StopAllCoroutines();
        ActiveStar(2);
    }
    public void Star4Onclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        StopAllCoroutines();
        ActiveStar(3);
    }
    public void Star5Onclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        StopAllCoroutines();
        ActiveStar(4);
    }

    public void OkOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(Application.platform == RuntimePlatform.Android)
            Application.OpenURL("market://details?id=" + Application.identifier);
        MainController.ClosePopup(PopupType.RateUs);
    }

    public void CloseOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.ClosePopup(PopupType.RateUs);
    }

    void ActiveStar(int index)
    {
        for(int i = 0; i < m_stars.Length; i++)
        {
            if(i <= index)
                m_stars[i].color = Color.white;
            else
                m_stars[i].color = Color.black;
        }
        //
        StartCoroutine(DelayNextAction(index));
    }

    IEnumerator DelayNextAction(int index)
    {
        yield return new WaitForSeconds(0.5f);
        if(index < 3)
            MainController.ClosePopup(PopupType.RateUs);
        else
        {
            m_content1.SetActive(false);
            m_content2.SetActive(true);
        }
    }
}
