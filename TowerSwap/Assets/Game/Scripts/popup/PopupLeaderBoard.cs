using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using TMPro;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UI;
public class PopupLeaderBoard : MonoBehaviour
{
    [SerializeField] private GameObject m_tabLevel;
    [SerializeField] private GameObject m_tabCoin;
    [SerializeField] private Transform m_levelContainer;
    [SerializeField] private GameObject m_levelItem;
    [SerializeField] private Transform m_coinContainer;
    [SerializeField] private GameObject m_coinItem;
    [SerializeField] private Toggle m_toggleLevel;
    [SerializeField] private Toggle m_toggleCoin;
    [SerializeField] private TextMeshProUGUI m_myRank;
    [SerializeField] private TextMeshProUGUI m_myValue;
    [SerializeField] private GameObject m_loading;

    // Update is called once per frame
    void OnEnable()
    {
        LeaderBoardManager.rankEvent += OnRankUpdate;
        //
        InitMe(0, 0);
        m_toggleLevel.isOn = true;
    }

    void OnDisable()
    {
        LeaderBoardManager.rankEvent -= OnRankUpdate;
        m_toggleLevel.isOn = false;
        m_toggleCoin.isOn = false;
        StopAllCoroutines();
    }

    private void OnRankUpdate(LeaderBoard type, RankResponse data)
    {
        StopAllCoroutines();
        switch (type)
        {
            case LeaderBoard.Level:
                StartCoroutine(InitLevel(data));
                if (data != null)
                    InitMe(data.current_user.rank, data.current_user.criteria.level);
                break;
            case LeaderBoard.Coin:
                StartCoroutine(InitCoin(data));
                if (data != null)
                    InitMe(data.current_user.rank, data.current_user.criteria.coin);
                break;
        }
    }

    IEnumerator InitLevel(RankResponse data)
    {
        yield return null;
        foreach (Transform child in m_levelContainer)
        {
            GameObject.Destroy(child.gameObject);
        }
        if (data == null)
            yield break;
        //
        for (int i = 0; i < data.top_users.Count; i++)
        {
            RankInfo info = data.top_users[i];
            GameObject go = Instantiate(m_levelItem);
            go.SetActive(true);
            go.transform.SetParent(m_levelContainer, false);
            Color color;
            if (ColorUtility.TryParseHtmlString(i % 2 == 0 ? "#F3A86E" : "#F7C394", out color))
                go.GetComponent<Image>().color = color;
            bool isMe = info.rank == data.current_user.rank;
            TextMeshProUGUI rank = go.transform.Find("rank").GetComponent<TextMeshProUGUI>();
            rank.text = info.rank.ToString();
            TextMeshProUGUI userName = go.transform.Find("name").GetComponent<TextMeshProUGUI>();
            userName.text = isMe ? "You" : info.name;
            TextMeshProUGUI value = go.transform.Find("level").GetComponent<TextMeshProUGUI>();
            value.text = info.criteria.level.ToString();
            //
            rank.color = isMe ? Color.green : Color.white;
            userName.color = rank.color;
            value.color = rank.color;
            yield return null;
        }
        m_loading.SetActive(false);
    }

    IEnumerator InitCoin(RankResponse data)
    {
        yield return null;
        foreach (Transform child in m_coinContainer)
        {
            GameObject.Destroy(child.gameObject);
        }
        if (data == null)
            yield break;
        for (int i = 0; i < data.top_users.Count; i++)
        {
            RankInfo info = data.top_users[i];
            GameObject go = Instantiate(m_coinItem);
            go.SetActive(true);
            go.transform.SetParent(m_coinContainer, false);
            Color color;
            if (ColorUtility.TryParseHtmlString(i % 2 == 0 ? "#F3A86E" : "#F7C394", out color))
                go.GetComponent<Image>().color = color;
            bool isMe = info.rank == data.current_user.rank;
            TextMeshProUGUI rank = go.transform.Find("rank").GetComponent<TextMeshProUGUI>();
            rank.text = info.rank.ToString();
            TextMeshProUGUI userName = go.transform.Find("name").GetComponent<TextMeshProUGUI>();
            userName.text = isMe ? "You" : info.name;
            TextMeshProUGUI value = go.transform.Find("coin").GetComponent<TextMeshProUGUI>();
            value.text = info.criteria.coin.ToString();
            //
            rank.color = isMe ? Color.green : Color.white;
            userName.color = rank.color;
            value.color = rank.color;
            yield return null;
        }
        m_loading.SetActive(false);
    }

    void InitMe(int rank, int value)
    {
        m_myRank.text = rank.ToString();
        m_myValue.text = value.ToString();
    }

    public void LevelOnclick(Toggle toggle)
    {
        if (!toggle.isOn)
            return;
        m_loading.SetActive(true);
        m_tabLevel.SetActive(true);
        m_tabCoin.SetActive(false);

         LeaderBoardManager.api.LoadBoard(LeaderBoard.Level);
       // Debug.Log();

    }

    public void CoinOnclick(Toggle toggle)
    {
        if (!toggle.isOn)
            return;
        m_loading.SetActive(true);
        m_tabLevel.SetActive(false);
        m_tabCoin.SetActive(true);
        LeaderBoardManager.api.LoadBoard(LeaderBoard.Coin);
    }

    public void CloseOnclick()
    {
        MainController.ClosePopup(PopupType.LeaderBoard);
    }
}
