﻿using System.Collections;
using System.Collections.Generic;
using Spine.Unity;
using UnityEngine;
using UnityEngine.UI;

public class PopupUnlockSkin : MonoBehaviour
{
    [SerializeField] private Image m_skin;
    [SerializeField] private SkeletonGraphic m_ballRed;
    [SerializeField] private SkeletonGraphic m_ballBlue;
    private int m_skinId;
    void Start()
    {

      
         m_skinId = MainModel.GetUnlockRescueSkin();
        //  Debug.Log(m_skinId + "ID skin");

        ShowSkin(m_skinId);
        if(m_skinId < 0)
            return;
        if (m_ballRed != null)
            m_ballRed.SetSkin(MainModel.skinConfig.GetSkin(m_skinId).red);
        if (m_ballBlue != null)
            m_ballBlue.SetSkin(MainModel.skinConfig.GetSkin(m_skinId).blue);
        //GameAssetManager.api.GetAvatar(m_skinId, m_skin);
    }

    public void GetFreeOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(m_skinId < 0)
            return;
        if(Application.isEditor)
            MainController.BuyCoinSkin(0, m_skinId);
        else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_GET_SKIN, m_skinId);
        //
        TrackingManager.WatchAdsUnlockSkin(m_skinId);
        MainController.ClosePopup(PopupType.UnlockSkin);
    }

    public void BackOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.ClosePopup(PopupType.UnlockSkin);
    }
    public void ShowSkin(int idSkin)
    {
       foreach(var skin in MainModel.storeConfig.rescue_skins)
        {
            if(idSkin == skin.id)
            {
                Debug.Log(skin.id);
                Debug.Log(skin.level + "lv --"+ skin.skin +": Skin");
               
            }
            
           
        }
       
    }
}
