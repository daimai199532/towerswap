using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupSpecial : MonoBehaviour
{
    [SerializeField] private GameObject m_tabCoinSkins;

    // Start is called before the first frame update
    void Start()
    {
       // Debug.Log(MainModel.storeConfig.special_skin);
        m_tabCoinSkins.GetComponent<TabSpecialSkins>().Init(MainModel.storeConfig.special_skins);
    }

    public void CloseOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.ClosePopup(PopupType.SpecialSkin);
    }
}
