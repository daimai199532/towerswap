﻿
using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class PopupManager : MonoBehaviour
{
    [SerializeField] private GameObject m_store;
    [SerializeField] private GameObject m_dailyReward;
    [SerializeField] private GameObject m_subscription;
    [SerializeField] private GameObject m_removeAds;
    [SerializeField] private GameObject m_rateUs;
    [SerializeField] private GameObject m_luckySpin;
    [SerializeField] private GameObject m_unlockSkin;
    [SerializeField] private GameObject m_treasure;
    [SerializeField] private GameObject m_shop;
    [SerializeField] private GameObject m_cheat;
    [SerializeField] private GameObject m_SpecialSkin;
    [SerializeField] private GameObject m_leaderBoard;

    private List<PopupData> m_queue;
    private List<GameObject> m_prev;

    private int m_cheatCounter;

    void Awake()
    {
        m_queue = new List<PopupData>();
        m_prev = new List<GameObject>();
        //
        MainController.openPopupEvent += OnOpenPopup;
        MainController.closePopupEvent += OnClosePopup;
        MainController.showDailyRewardEvent += OnShowDailyReward;
        //
        m_cheatCounter = -1;
    }

    void OnDestroy()
    {
        MainController.openPopupEvent -= OnOpenPopup;
        MainController.closePopupEvent -= OnClosePopup;
        MainController.showDailyRewardEvent -= OnShowDailyReward;
    }

    private void OnShowDailyReward()
    {
        PopupDailyReward comp = m_dailyReward.GetComponent<PopupDailyReward>();
        if(comp.HasReward())
            MainController.OpenPopup(PopupType.DailyReward);
    }

    private void OnClosePopup(PopupType type)
    {        
        GameObject go = null;
        switch (type)
        {
            case PopupType.Store:
                go = m_store;
                break;
            case PopupType.DailyReward:
                go = m_dailyReward;
                break;
            case PopupType.Subscription:
                go = m_subscription;
                break;
            case PopupType.RemoveAds:
                go = m_removeAds;
                break;
            case PopupType.RateUs:
                go = m_rateUs;
                break;
            case PopupType.LuckySpin:
                go = m_luckySpin;
                break;
            case PopupType.UnlockSkin:
                go = m_unlockSkin;
                break;
            case PopupType.Treasure:
                go = m_treasure;
                break;
            case PopupType.Shop:  
                go = m_shop;
                break;
            case PopupType.SpecialSkin:
                go = m_SpecialSkin;
                break;
            case PopupType.LeaderBoard:
                go = m_leaderBoard;
                break;
        }

        if (go != null)
        {
            GameObject panel = go.transform.Find("panel").gameObject;
            panel.transform.localScale = Vector3.one;
            panel.transform.DOScale(Vector3.zero, 0.25f).SetEase(Ease.InBack).OnComplete(() =>
            {
                go.SetActive(false);
                m_prev.Remove(go);
                if(m_prev.Count < 1)
                {
                    if(m_queue.Count > 0)
                    {
                        PopupData p = m_queue[0];
                        m_queue.RemoveAt(0);
                        OpenPopup(p.pType, p.data);
                    }else
                        MainController.ShowMainUI();
                }
            });
        }
    }

    private void OnOpenPopup(PopupType type, object data)
    {
        if(m_prev.Count > 0 && type != PopupType.Subscription)
        {
            m_queue.Add(new PopupData(){
                data = data,
                pType = type
            });
            return;
        }
        OpenPopup(type, data);
    }

    void OpenPopup(PopupType type, object data)
    {
        GameObject go = null;
        switch (type)
        {
            case PopupType.Store:
                go = m_store;
                break;
            case PopupType.DailyReward:
                go = m_dailyReward;
                break;
            case PopupType.Subscription:
                go = m_subscription;
                break;
            case PopupType.RemoveAds:
                go = m_removeAds;
                break;
            case PopupType.RateUs:
                go = m_rateUs;
                break;
            case PopupType.LuckySpin:
                go = m_luckySpin;
                break;
            case PopupType.UnlockSkin:
                go = m_unlockSkin;
                break;
            case PopupType.Treasure:
                go = m_treasure;
                break;
            case PopupType.Shop:  
                go = m_shop;
                break;
            case PopupType.SpecialSkin:
                go = m_SpecialSkin;
                break;
            case PopupType.LeaderBoard:
                go = m_leaderBoard;
                break;
        }
        if (go != null)
        {
            go.SetActive(true);
            GameObject panel = go.transform.Find("panel").gameObject;
            panel.transform.localScale = Vector3.zero;
            panel.transform.DOScale(Vector3.one, 0.25f).SetEase(Ease.OutBack);
            m_prev.Add(go);
        }
    }

    public void CheatLeftOnclick()
    {
        if(m_cheatCounter != -1)
            return;
        StopAllCoroutines();
        m_cheatCounter = 0;        
        StartCoroutine(DelayResetCheat());
    }

    public void CheatRightOnclick()
    {
        if(m_cheatCounter != 1)
            return;
        m_cheatCounter = -1;
        StopAllCoroutines();
        m_cheat.SetActive(true);
    }

    public void CheatCenterOnclick()
    {
        if(m_cheatCounter != 0)
            return;
        StopAllCoroutines();
        m_cheatCounter = 1;        
        StartCoroutine(DelayResetCheat());
    }

    IEnumerator DelayResetCheat()
    {
        yield return new WaitForSeconds(1f);
        m_cheatCounter = -1;
    }

    private class PopupData
    {
        public PopupType pType;
        public object data;
    }
}
