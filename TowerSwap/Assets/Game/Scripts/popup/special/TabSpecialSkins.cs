using System.Collections.Generic;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TabSpecialSkins : MonoBehaviour
{
    [SerializeField] private GameObject m_item;
    [SerializeField] private Transform m_content;
   // [SerializeField] private GameObject m_buttonAds;
 //  [SerializeField] private GameObject m_buttonSelect;
    [SerializeField] public Sprite m_image;
    [SerializeField] private Sprite m_image2;
    

    private List<int> listid = new List<int>();
    private List<int> listskin = new List<int>();
    private SpecialSkinItem m_selected;
    List<SpecialConfig> config;
    // Start is called before the first frame update
    void Awake()
    {
        MainController.updateStoreSkinEvent += OnUpdate;
      
     
    }


    void OnDestroy()
    {
        MainController.updateStoreSkinEvent -= OnUpdate;
      
    }

    private void OnUpdate(int skin)
    {
        if (m_selected == null)
            return;
        if (m_selected.config.skin != skin)
            return;
       // SelectItem();
     
    }

 
  
    public void Init(List<SpecialConfig> config)
    {
        foreach (Transform child in m_content)
        {
            GameObject.Destroy(child.gameObject);
        }
        m_selected = null;
        SpecialSkinItem first = null;

        if (MainModel.listSpecialSkin.Length == 0)
        {
            int dem = 0;
            foreach (SpecialConfig skinConfig in config)
            {

                GameObject go = Instantiate(m_item);
                go.SetActive(true);
                go.transform.SetParent(m_content, false);
                SpecialSkinItem comp = go.GetComponent<SpecialSkinItem>();
                go.GetComponent<SpecialSkinItem>().id = skinConfig.id;
                go.GetComponent<SpecialSkinItem>().skin = skinConfig.skin;

                listid.Add(skinConfig.id);
                listskin.Add(skinConfig.skin);

                if (dem < 2)
                    go.GetComponent<Image>().sprite = m_image;
                else
                    go.GetComponent<Image>().sprite = m_image2;
                dem++;
                bool selected = skinConfig.skin == MainModel.currentSkin;
               comp.Init(skinConfig, selected);
                if (selected)
                    ItemOnclick(comp);
                if (first == null)
                    first = comp;
                if (dem >= 5)
                {
                    break;
                }
            }
        }
        else
        {

            int dem = 0;
            for (int i = 0; i < MainModel.listSpecialSkin.Length; i++)
            {

                GameObject go = Instantiate(m_item);
                go.SetActive(true);
                go.transform.SetParent(m_content, false);
                SpecialSkinItem comp = go.GetComponent<SpecialSkinItem>();
                go.GetComponent<SpecialSkinItem>().id = dem;
                go.GetComponent<SpecialSkinItem>().skin = int.Parse(MainModel.listSpecialSkin[i]);

                
                listid.Add(dem);
                listskin.Add(int.Parse(MainModel.listSpecialSkin[i]));
                comp.Initlist(int.Parse(MainModel.listSpecialSkin[i]), true);
          
                if (dem < 2)
                    go.GetComponent<Image>().sprite = m_image;
                else
                    go.GetComponent<Image>().sprite = m_image2;
                dem++;

                if (dem >= 5)
                {
                    break;
                }
            }
        }
    }
  
    public void ItemOnclick(SpecialSkinItem item)
    {
       
        if (m_selected != null)
            m_selected.Focus(false);
        item.Focus(true);
        m_selected = item;
       // SelectItem();
    }
    public void OnclickAds()
    {
       // SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
       // AdsManager.Instance.ShowVideoReward(GameConstant.ADS_COINS_500);
    }
    public void OnclickSelect()
    {
       // SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
      //  MainController.SelectSkin(m_selected.config.skin);
    }
}
