using Spine;
using Spine.Unity;
using TMPro;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine;
using System.Collections;

public class SpecialSkinItem : MonoBehaviour
{
    [SerializeField] private  SkeletonGraphic m_spineRed;
    [SerializeField] private SkeletonGraphic m_spineBlue;
    [SerializeField] private GameObject m_check;
    [SerializeField] private TextMeshProUGUI textCount;
    [SerializeField] private GameObject m_buttonAds;
    [SerializeField] private GameObject m_buttonSelect;
    [HideInInspector] public SpecialConfig config;
    public  int id;
    public  int skin;
    public int uncl;
    private int count;
  
    private void Awake()
    {
        MainController.updateCountAdsEvent += showTextAds;
        IAPManager.purchaseResultEvent += OnPurchaseResult;
        MainController.changeSkinEvent += OnSelect;
    }
  
    private void Start()
    {  
        MainModel.GetSpecialSkin(id, ref count);
        checkText(count, id);
        showTextAds(id);
      
    }
    private void OnEnable()
    {
        checkText(count, id);
    }
    private void OnDestroy()
    {
        MainController.updateCountAdsEvent -= showTextAds;
        MainController.changeSkinEvent -= OnSelect;
        IAPManager.purchaseResultEvent -= OnPurchaseResult;
    }
    private void OnSelect(int oldSkin, int newSkin)
    {
        if (skin != oldSkin && skin != newSkin)
            return;
        Init(config, true);
    }

  
    public void Init(SpecialConfig config, bool selected)
    {
        this.config = config;
        SkinInfo sk = MainModel.skinConfig.GetSkin(config.skin);
        if (m_spineRed != null)
            m_spineRed.SetSkin(sk.red);
        if (m_spineBlue != null)
            m_spineBlue.SetSkin(sk.blue);
    }
    public void Initlist(int skin, bool selected)
    {
        SkinInfo sk = MainModel.skinConfig.GetSkin(skin);
        if (m_spineRed != null)
            m_spineRed.SetSkin(sk.red);
        if (m_spineBlue != null)
            m_spineBlue.SetSkin(sk.blue);
    }
    private void OnPurchaseResult(string arg1, bool arg2)
    {
        showTextAds(skin);

    }
    public void Focus(bool isFocus)
    {
        m_check.SetActive(isFocus);
    }
    public void OnclickAds()
    {
        
        AdsManager.Instance.ShowVideoReward("", null, () =>
        {
            TrackingManager.WatchAdSpecialSkin(skin);
            MainController.UpdateCountAdsEvent(id);
        }
          );
      
    }
    public void OnclickSelect()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.SelectSkin(skin);
        m_buttonSelect.SetActive(false);
        ChangeSkinUI.ischangeSkin = false;
    }

    
    public void showTextAds(int idskin)
    {
      
        idskin = id;
        switch (idskin.ToString())
        {
            case "1":


                checkText(MainModel.specialskin1, id);

                break;
            case "2":


                checkText(MainModel.specialskin2, id);

                break;
            case "3":


                checkText(MainModel.specialskin3, id);

                break;
            case "4":


                checkText(MainModel.specialskin4, id);
                break;
            case "5":


                checkText(MainModel.specialskin5, id);
                break;
        }  
       
    }
    void checkText(int count, int id)
    {
       

        if (id <3)
        {
            if (count >= 5 || MainModel.subscription || MainModel.unlockSkins.Contains(skin))
            {
                count = 5;
                m_check.SetActive(true);
                m_buttonAds.SetActive(false);
                m_buttonSelect.SetActive(true);
                MainController.BuyCoinSkin(0, skin);
               
            }
            else
            {
                m_buttonAds.SetActive(true);
                m_buttonSelect.SetActive(false);
                m_check.SetActive(false);
                textCount.text = count.ToString() + "/5";
            }
                
           
        }
       else
        {
            if (count >= 4 || MainModel.subscription || MainModel.unlockSkins.Contains(skin))
            {
                count = 4;
                m_check.SetActive(true);
                m_buttonAds.SetActive(false);
                m_buttonSelect.SetActive(true);
                MainController.BuyCoinSkin(0, skin);
            }
            else
            {
                m_buttonAds.SetActive(true);
                m_buttonSelect.SetActive(false);
                m_check.SetActive(false);
                textCount.text = count.ToString() + "/4";
            }
         
        }
      
    }
  

}
