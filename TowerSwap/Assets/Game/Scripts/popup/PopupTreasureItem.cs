﻿using System;
using System.Collections;
using System.Collections.Generic;
using Spine;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupTreasureItem : MonoBehaviour
{
    private SkeletonGraphic m_cover;
    private TextMeshProUGUI m_text;
    private Image m_giftIcon;
    private Transform m_gift;

    void Start()
    {
        m_cover.AnimationState.Complete += OnComplete;
    }

    public void Init()
    {
        m_cover = transform.Find("cover").GetComponent<SkeletonGraphic>();
        m_gift = transform.Find("fx");
        m_giftIcon = m_gift.Find("icon").GetComponent<Image>();
        m_text = m_gift.Find("text").GetComponent<TextMeshProUGUI>();
        //
        m_cover.gameObject.SetActive(true);
        m_gift.gameObject.SetActive(false);
        StartCoroutine(DelayPlayAnim());
    }

    IEnumerator DelayPlayAnim()
    {
        yield return null;
        yield return null;
        m_cover.AnimationState.SetAnimation(0, "idle", true);
    }


    private void OnComplete(TrackEntry trackEntry)
    {
        if(trackEntry.Animation.Name != "open")
            return;
        m_cover.gameObject.SetActive(false);
        m_gift.gameObject.SetActive(true);
        SoundManager.PlaySound(GameConstant.AUDIO_SPIN_WIN, false);
    }

    public void Open(Sprite sprite, int quantity)
    {
        m_giftIcon.sprite = sprite;
        m_text.text = quantity < 1 ? "" : ("+" + quantity); 
        m_giftIcon.SetNativeSize();   
        m_cover.AnimationState.SetAnimation(0, "open", false);
    }
}
