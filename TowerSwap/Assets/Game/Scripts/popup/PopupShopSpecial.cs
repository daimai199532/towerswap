﻿
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PopupShopSpecial : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI m_textTotalCoin;
    [SerializeField] private Image m_iconItem1;
    [SerializeField] private TextMeshProUGUI m_textItem2;
    [SerializeField] private TextMeshProUGUI m_priceItem3;
    [SerializeField] private Button m_buttonItem1;
    [SerializeField] private Button m_buttonItem2;
    [SerializeField] private Button m_buttonItem3;

    private int m_skin;
    private int m_coin;
    private int m_heartPrice;

    void OnEnable()
    {
        m_skin = GetRandomSkin();
        m_coin = GetRandomCoin();
        m_heartPrice = GetRandomPriceHeart();
        GameAssetManager.api.GetAvatar(m_skin, m_iconItem1);
        m_textItem2.text = "+" + m_coin;
        m_priceItem3.text = "" + m_heartPrice;
        //
        OnUpdateCoin(0);
        //
        MainController.updateCoinEvent += OnUpdateCoin;
        //
        TrackingManager.SpecialShopOpen();
    }

    void OnDisable()
    {
        MainController.updateCoinEvent -= OnUpdateCoin;
    }

    private void OnUpdateCoin(int coin)
    {
        m_textTotalCoin.text = GameUtils.CoinToString(MainModel.totalCoin);
        m_buttonItem3.interactable = MainModel.totalCoin >= m_heartPrice;
    }

    public void BackOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.ClosePopup(PopupType.Shop);
    }

    public void Item1Onclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(Application.isEditor)
        {
            MainController.BuyCoinSkin(0, m_skin);
            m_buttonItem1.interactable = false;
        }else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_SPECIAL_SHOP_BUY_SKIN, m_skin, ()=>{
                m_buttonItem1.interactable = false;
                TrackingManager.SpecialShopBuySkin();
            });
    }

    public void Item2Onclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(Application.isEditor)
        {
            MainController.UpdateCoin(m_coin);
            m_buttonItem2.interactable = false;
        }else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_SPECIAL_SHOP_BUY_COIN, m_coin, ()=>{
                m_buttonItem2.interactable = false;
                TrackingManager.SpecialShopBuyCoin();
            });
    }

    public void Item3Onclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(MainModel.totalCoin < m_heartPrice)
            return;
        MainController.UpdateCoin(-m_heartPrice);
        MainController.UpdateHeart(1);
        m_buttonItem3.interactable = MainModel.totalCoin >= m_heartPrice;
        //
        TrackingManager.SpecialShopBuyLive();
    }

    int GetRandomSkin()
    {
        List<PremiumSkinConfig> skins = MainModel.storeConfig.premium_skins;
        List<PremiumSkinConfig> remain = new List<PremiumSkinConfig>();
        foreach(PremiumSkinConfig skin in skins)
        {
            if(MainModel.unlockSkins.Contains(skin.skin))
                continue;
            remain.Add(skin);
        }
        int ran = Random.Range(0, remain.Count);
        if(ran >= remain.Count)
            return skins[0].skin;
        return remain[ran].skin;
    }

    int GetRandomCoin()
    {
        int ran = Random.Range(500, 2000);
        return ran/100*100;
    }

    int GetRandomPriceHeart()
    {
        int ran = Random.Range(1000, 2000);
        return ran/100*100;
    }
}
