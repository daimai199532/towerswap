﻿
using UnityEngine;
public class TrackingManager
{
    public static void WatchAdsMenuLive()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_MENU_LIVE");
    }

    public static void WatchAdsMenuCoin()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_MENU_COIN");
    }

    public static void StartLevel(int level)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("LEVEL_START", "level", level);
    }

    public static void CompleteLevel(int level)
    {
        if(level < 11)
        {
            Firebase.Analytics.FirebaseAnalytics.LogEvent("COMPLETE_LEVEL_" + level);
            FacebookManager.TrackingCompleteLevel(level);
            //TenjinManager.TrackingCompleteLevel(level);
        }
        Firebase.Analytics.FirebaseAnalytics.LogEvent("COMPLETE_LEVEL", "level", level);
    }

    public static void FailLevel(int level)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("FAIL_LEVEL", "level", level);
    }

    public static void ReviveLevel(int level)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("REVIVE_LEVEL", "level", level);
    }

    public static void WatchAdsGameLive()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_INGAME_LIVE");
    }

    public static void WatchAdsGameCoin()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_INGAME_COIN");
    }

    public static void WatchAdsGameMagnet(int level)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_INGAME_MAGNET" ,"level", level);
    }

    public static void UnlockSkin(int skinId)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("UNLOCK_SKIN", "skin", skinId);
    }

    public static void WatchAdsTrySkin(int skinId)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_TRY_SKIN", "skin", skinId);
       
    }

    public static void WatchAdsUnlockSkin(int skinId)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_UNLOCK_SKIN", "skin", skinId);
    }

    public static void WatchAdsX2Reward()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_X2REWARD");
    }

    public static void TrySkinAdsInGame(int skin)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_INGAME_TRY_SKIN", "skin", skin);
    }


    //
    public static void WatchAdMenuTrySkin(int skinID)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_MENU_TRY_SKIN_"+ skinID);
      
        
    }
    public static void WatchAdSpecialSkin(int skinID)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_SPECIAL_SKIN_" + skinID);


    }


    public static void LuckySpin()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_LUCKY_SPIN");
    }
    public static void LuckySpinX2()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_LUCKY_SPIN_X2");
    }

    public static void KeyTreasure()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("WATCH_AD_TREASURE_KEY");
    }

    public static void TreasureOpen()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("TREASURE_OPEN");
    }

    public static void SpecialShopOpen()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("SPECIAL_SHOP_OPEN");
    }

    public static void SpecialShopBuySkin()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("SPECIAL_SHOP_BUY_SKIN");
    }
    public static void SpecialShopBuyCoin()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("SPECIAL_SHOP_BUY_COIN");
    }
    public static void SpecialShopBuyLive()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("SPECIAL_SHOP_BUY_LIVE");
    }

    public static void Replay(int level)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("REPLAY_LEVEL", "level", level);
    }

    public static void SkipLevel(int level)
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("SKIP_LEVEL", "level", level);
    }

    public static void LoadingStart()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("LOADING_START");
    }

    public static void LoadingEnd()
    {
        Firebase.Analytics.FirebaseAnalytics.LogEvent("LOADING_FINISH");
    }

    public static void AdsImpression(IronSourceImpressionData impressionData)
    {
        if (impressionData != null) 
        {
            Firebase.Analytics.Parameter[] AdParameters = {
                new Firebase.Analytics.Parameter("ad_platform", "ironSource"),
                new Firebase.Analytics.Parameter("ad_source", impressionData.adNetwork),
                new Firebase.Analytics.Parameter("ad_unit_name", impressionData.adUnit),
                new Firebase.Analytics.Parameter("ad_format", impressionData.instanceName),
                new Firebase.Analytics.Parameter("currency", "USD"),
                new Firebase.Analytics.Parameter("value", impressionData.revenue == null ? "0" : impressionData.revenue.ToString())
            };
            Firebase.Analytics.FirebaseAnalytics.LogEvent("ad_impression", AdParameters);
	    }
    }
}
