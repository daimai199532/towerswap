﻿using System.Collections;
using System.Collections.Generic;
using Facebook.Unity;
using UnityEngine;

public class FacebookManager : MonoBehaviour
{
    void Awake ()
    {
        DontDestroyOnLoad(gameObject);
        if (FB.IsInitialized) {
            FB.ActivateApp();
        } else {
            //Handle FB.Init
            FB.Init( () => {
                FB.ActivateApp();
            });
        }
    }

    void OnApplicationPause (bool pauseStatus)
    {
        // Check the pauseStatus to see if we are in the foreground
        // or background
        if (!pauseStatus) {
            //app resume
            if (FB.IsInitialized) {
                FB.ActivateApp();
            } else {
                //Handle FB.Init
                FB.Init( () => {
                    FB.ActivateApp();
                });
            }
        }
    }

    public static void TrackingCompleteLevel(int level)
    {
        FB.LogAppEvent("COMPLETE_LEVEL_" + level);
    }
}
