﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Spine.Unity;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Random = UnityEngine.Random;

public class MainScene : MonoBehaviour
{
    [SerializeField] private Transform m_heart;
    [SerializeField] private GameObject m_maskObject;
    [SerializeField] private RectTransform m_mask;
    [SerializeField] private GameObject m_mainPanel;
    [SerializeField] private TextMeshProUGUI m_textCoin;
    [SerializeField] private TextMeshProUGUI m_textHeart;
    [SerializeField] private SkeletonGraphic m_ballRed;
    [SerializeField] private SkeletonGraphic m_ballBlue;
    [SerializeField] private GameObject m_objComplete;
    [SerializeField] private GameObject m_objFail;
    [SerializeField] private GameObject m_buttonPlay;
    [SerializeField] private Transform m_coinCompletePos;
    [SerializeField] private RectTransform m_coinCompleteHolder;
    [SerializeField] private Transform m_coinCompleteFinishPos;
    [SerializeField] private TextMeshProUGUI m_textLevel;
    [SerializeField] private Slider m_levelProgress;
    [SerializeField] private Image m_bossAvatar;
    [SerializeField] private GameObject m_noVideoAds;
    [SerializeField] private GameObject m_bonusHeart;
    [SerializeField] private GameObject m_bonusCoin;
    [SerializeField] private GameObject m_bonusSkin;
    [SerializeField] private ParticleSystem m_effectWin;
    [SerializeField] private GameObject m_trySkinUi;
    [SerializeField] private TextMeshProUGUI m_textTimeLuckySpin;
    [SerializeField] private GameObject m_objectKeys;
    [SerializeField] private Sprite[] m_spriteKeys;

    private bool m_stop;
    private List<GameObject> m_coins;
    private int m_currentCoins;
    private Tweener m_coinTween;
    private Vector3 m_startPosNoVideo;
    private LevelResult m_levelResult;
    private Coroutine m_nothankTimer;

    void OnDestroy()
    {
        MainController.openPopupEvent -= OnOpenPopup;
        MainController.showMainUIEvent -= OnShow;
        MainController.updateCoinEvent -= OnUpdateCoin;
        MainController.changeSkinEvent -= OnChangeSkin;
        MainController.updateHeartEvent -= OnUpdateHeart;
        MainController.updateLevelResultEvent -= OnUpdateLevelResult;
        MainController.noticeEvent -= OnNotice;
        MainController.bonusHeartEvent -= OnBonusHeart;
        MainController.bonusCoinEvent -= OnBonusCoin;
        MainController.bonusSkinEvent -= OnBonusSkin;
        MainController.resetUiEvent -= OnResetUi;
        MainController.trySkinUIEvent -= OnTrySkin;
        MainController.updateSpinEvent -= OnUpdateSpin;
        MainController.updateKeyTreasureEvent -= OnUpdateKey;
        //
        
    }

    void Awake()
    {
        if(!MainController.ConfigLoaded())
            return;
        m_stop = false;
        m_startPosNoVideo = m_noVideoAds.transform.localPosition;
        //
        MainController.openPopupEvent += OnOpenPopup;
        MainController.showMainUIEvent += OnShow;
        MainController.updateCoinEvent += OnUpdateCoin;
        MainController.changeSkinEvent += OnChangeSkin;
        MainController.updateHeartEvent += OnUpdateHeart;
        MainController.updateLevelResultEvent += OnUpdateLevelResult;
        MainController.noticeEvent += OnNotice;
        MainController.bonusHeartEvent += OnBonusHeart;
        MainController.bonusCoinEvent += OnBonusCoin;
        MainController.bonusSkinEvent += OnBonusSkin;
        MainController.resetUiEvent += OnResetUi;
        MainController.trySkinUIEvent += OnTrySkin;
        MainController.updateSpinEvent += OnUpdateSpin;
        MainController.updateKeyTreasureEvent += OnUpdateKey;
        //
        if (MainModel.mapLevel < 1)
        {
            m_buttonPlay.transform.GetChild(1).gameObject.SetActive(true);
        }

    }

    void Start()
    {
        //Debug.Log("======================");
        MainController.UpdateUI();
        //
        //m_shop.AnimationState.SetAnimation(0, Random.Range(0,2) == 0 ? "animation" : "animation2", true);
        //AdsManager.Instance.ShowBanner();
        StartCoroutine(SceneIn());
       
    }

    private void OnUpdateKey()
    {  
        for(int i = 0; i < m_objectKeys.transform.childCount; i++)
        {
            Image img = m_objectKeys.transform.GetChild(i).GetComponent<Image>();
            img.sprite = i < MainModel.keyQuantity ? m_spriteKeys[0] : m_spriteKeys[1];
        }
    }

    private void OnUpdateSpin()
    {
        if(MainModel.spinQuatity > 0)
            m_textTimeLuckySpin.text = "LUCKY SPIN";
        else
            StartCoroutine(ScheduleLuckySpin());
    }

    private void OnResetUi()
    {
        if(m_nothankTimer != null)
            StopCoroutine(m_nothankTimer);
        m_objComplete.transform.Find("text-nothanks").gameObject.SetActive(false);
        m_objComplete.transform.Find("button-x2-reward").gameObject.SetActive(false);
        m_objComplete.transform.Find("button-x2-reward-ads").gameObject.SetActive(false);
        m_buttonPlay.SetActive(true);
       
    }

    private void OnBonusSkin(int skin)
    {
        m_bonusSkin.SetActive(true);
        SkeletonGraphic iconRed = m_bonusSkin.transform.Find("icon-red").GetComponent<SkeletonGraphic>();
       iconRed.SetSkin(MainModel.skinConfig.GetSkin(skin).red);
        SkeletonGraphic iconBlue = m_bonusSkin.transform.Find("icon-blue").GetComponent<SkeletonGraphic>();
        iconBlue.SetSkin(MainModel.skinConfig.GetSkin(skin).blue);
        StartCoroutine(BonusSkin());
    }

    private void OnBonusCoin(int coin)
    {
        SoundManager.PlaySound(GameConstant.AUDIO_EAT_ITEM, false);
        m_bonusCoin.SetActive(true);
        m_bonusCoin.transform.Find("text-coin-value").GetComponent<TextMeshProUGUI>().text = "+" + coin;
        StartCoroutine(BonusCoin());
    }

    private void OnBonusHeart(int heart)
    {
        SoundManager.PlaySound(GameConstant.AUDIO_EAT_ITEM, false);
        m_bonusHeart.SetActive(true);
        m_bonusHeart.transform.Find("text-heart-value").GetComponent<TextMeshProUGUI>().text = "+" + heart;
        StartCoroutine(BonusHeart(heart));
    }

    IEnumerator BonusSkin()
    {
        yield return new WaitForSeconds(2f);
        m_bonusSkin.SetActive(false);
    }

    IEnumerator BonusHeart(int heart)
    {
        int startHeart = MainModel.totalHeart - heart;
        yield return new WaitForSeconds(0.5f);
        GameObject icon = m_bonusHeart.transform.Find("icon").gameObject;
        for (int i = 0; i < heart; i++)
        {
            GameObject go = Instantiate(icon, icon.transform.parent);
            go.transform.position = icon.transform.position;
            var i1 = i;
            SoundManager.PlaySound(GameConstant.AUDIO_BONUS_HEART, false);
            go.transform.DOMove(m_heart.position, 0.2f).OnComplete(() =>
            {
                OnUpdateHeart(startHeart+i1+1, null);
                Destroy(go);
            });
            yield return new WaitForSeconds(0.2f);
        }
        m_bonusHeart.SetActive(false);
    }

    IEnumerator BonusCoin()
    {
        yield return new WaitForSeconds(0.5f);
        GameObject sound = SoundManager.PlaySound(GameConstant.AUDIO_RAIN_COIN, true);
        GameObject icon = m_bonusCoin.transform.Find("icon").gameObject;
        OnUpdateCoin(MainModel.totalCoin);
        for (int i = 0; i < 10; i++)
        {
            GameObject go = Instantiate(icon, icon.transform.parent);
            go.SetActive(true);
            go.transform.position = icon.transform.position;
            go.transform.DOMove(m_coinCompleteFinishPos.position, 0.1f).OnComplete(() =>
            {
                Destroy(go);
            });
            yield return new WaitForSeconds(0.1f);
        }
        m_bonusCoin.SetActive(false);
        Destroy(sound);
    }

    void OnNotice(string message)
    {
        m_noVideoAds.transform.Find("text").GetComponent<TextMeshProUGUI>().text = message;
        m_noVideoAds.transform.DOKill();
        m_noVideoAds.transform.localPosition = m_startPosNoVideo;
        m_noVideoAds.SetActive(true);
        float y = m_noVideoAds.transform.localPosition.y;
        m_noVideoAds.transform.DOLocalMoveY(y-400, 0.5f).OnComplete(() =>
        {
            m_noVideoAds.transform.DOLocalMoveY(y, 0.5f).SetDelay(2f).OnComplete(() =>
            {
                m_noVideoAds.SetActive(false);
            });
        });
    }

    private void OnUpdateLevelResult(LevelResult obj)
    {
        int mapLevel = MainModel.GetMapLevel();
        m_levelResult = obj;
        //
        if(obj == null)
        {
            SoundManager.PlaySound(GameConstant.AUDIO_MUSIC_HOME, true, true);            
            m_textLevel.transform.parent.gameObject.SetActive(mapLevel > 0);
            m_textLevel.text = mapLevel < 2 ? "Complete Tutorial" : "Level " + (mapLevel-1);
            m_objComplete.SetActive(false);
            m_objFail.SetActive(false);
            m_buttonPlay.SetActive(true);
            m_currentCoins = MainModel.totalCoin;
            OnUpdateCoin(m_currentCoins);   
            m_trySkinUi.SetActive(true);
        }
        else
        {
            m_buttonPlay.SetActive(false);
            m_currentCoins = obj.oldCoin;
            m_textLevel.text = obj.mapLevel < 2 ? "Complete Tutorial" : "Level " + (obj.mapLevel-1);
            if (obj.isComplete)
            {
                m_trySkinUi.SetActive(false);
                OnUpdateCoin(m_currentCoins); 
                m_objComplete.SetActive(true);
                m_objFail.SetActive(false);
            }            
            else
            {
                m_trySkinUi.SetActive(true);
                OnUpdateCoin(m_currentCoins);
                m_objComplete.SetActive(false);
                m_objFail.SetActive(true);
            }
        }
    }

    private void OnUpdateHeart(int heart, Vector3? itemPos)
    {
        m_textHeart.text = heart + "";
    }

    private void OnChangeSkin(int oldSkin, int newSkin)
    {        
        if(m_ballRed != null)
            m_ballRed.SetSkin(MainModel.skinConfig.GetSkin(newSkin).red);
        if(m_ballBlue != null)
            m_ballBlue.SetSkin(MainModel.skinConfig.GetSkin(newSkin).blue);
    }

    private void OnTrySkin(int skin)
    {
        if (m_ballRed != null)
            m_ballRed.SetSkin(MainModel.skinConfig.GetSkin(skin).red);
        if(m_ballBlue != null)
            m_ballBlue.SetSkin(MainModel.skinConfig.GetSkin(skin).blue);
    }

    private void OnUpdateCoin(int coin)
    {
        //m_currentCoins = coin;
        m_coinTween?.Kill();
        if(m_currentCoins == coin)
            m_textCoin.text = GameUtils.CoinToString(m_currentCoins);
        else
        {
            m_coinTween =  DOTween.To(() => m_currentCoins, x => {
                m_currentCoins = x;
                m_textCoin.text = GameUtils.CoinToString(x);
                }, coin, 1f);
        }
    }

    private void OnShow()
    {
        m_mainPanel.SetActive(true);
        AdsManager.Instance.ShowBanner();
        if(m_levelResult != null)
        {
            if(m_levelResult.isComplete)
                ShowEffectWin(m_levelResult);
            m_levelResult = null;
        }
    }

    private void OnOpenPopup(PopupType type, object data)
    {
        m_mainPanel.SetActive(false);
        AdsManager.Instance.HideBanner();  

        
    }

    public void PlayOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(m_stop)
            return;
        m_stop = true;
        //m_mask.sizeDelta = new Vector2(3000, 3000);
        SceneOut();
        AdsManager.Instance.HideBanner();      
    }

    public void ShopOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.OpenPopup(PopupType.Store);
    }

    public void DailyRewardOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.OpenPopup(PopupType.DailyReward);
    }

    public void HeartAdsOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(Application.isEditor)
            MainController.BonusHeart(GameConstant.BONUS_HEART_ADS);
        else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_HEART);
        TrackingManager.WatchAdsMenuLive();
    }

    public void CoinAdsOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(Application.isEditor)
            MainController.BonusCoin(GameConstant.BONUS_COIN_ADS);
        else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_COINS_500);
        TrackingManager.WatchAdsMenuCoin();
    }
    public void TopOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.OpenPopup(PopupType.LeaderBoard);
    }
    public void SkipLevelOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(Application.isEditor)
        {
            MainController.SkipLevel();
            SceneOut();
        }
        else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_SKIP_LEVEL, null, ()=>{
                SceneOut();
            });
    }

    public void X2LevelRewardOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        if(Application.isEditor || (!MainModel.activeAdsLevel0 && MainModel.GetMapLevel() == 1))
            MainController.X2LevelReward();
        else
            AdsManager.Instance.ShowVideoReward(GameConstant.ADS_X2_LEVEL_REWARD);
        //
        TrackingManager.WatchAdsX2Reward();
    }
    public void X2FreeLevelRewardOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.X2LevelReward();
    }

    public void RemoveAdsOnclick()
    {
        MainController.OpenPopup(PopupType.RemoveAds);
    }
    public void LuckySpinOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.OpenPopup(PopupType.LuckySpin);
    }
    public void SpecialSkinOnclick()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_CLICK, false);
        MainController.OpenPopup(PopupType.SpecialSkin);
    }

    void ShowLevelComplete(LevelResult result)
    {
        SoundManager.PlaySound(GameConstant.AUDIO_VICTORY, false);    
        bool hasPopup = false;
        //show lucksySpin
        if(result.mapLevel -1 == MainModel.levelShowLuckySpin)
        {
            hasPopup = true;
            StartCoroutine(ScheduleShowLuckSpin());
        }
        //unlock skin
        if(MainModel.GetUnlockRescueSkin() >= 0)
        {
            hasPopup = true;
            StartCoroutine(ScheduleShowUnlockSkin()); 
        }                    
        //treasure
        if(MainModel.keyQuantity >= 3)
        {
            hasPopup = true;
            StartCoroutine(ScheduleShowTreasure());
        }
        //check daily reward
        if (result.mapLevel == 4)
        {
            hasPopup = true;
            StartCoroutine(ScheduleShowDailyReward());
        }
        //check remove ads
        if((result.mapLevel - 1) == MainModel.levelShowPopupRemoveAds)
        {
            hasPopup = true;
            StartCoroutine(ScheduleShowRemoveAds());
        }
        //check show rate us
        if((result.mapLevel - 1) == MainModel.levelShowPopupRateUs)
        {
            hasPopup = true;
            StartCoroutine(ScheduleShowRateUs());
        }
        //special shop
        if(MainModel.levelsSpecialShop.ToList().Contains(MainModel.levelResult.realMapLevel+""))
        {
            hasPopup = true;
            StartCoroutine(ScheduleShowSpecialShop());
        }
        if(hasPopup)
            return;
        ShowEffectWin(result);   
    }

    IEnumerator ScheduleShowDailyReward()
    {
        yield return new WaitForSeconds(0.5f);
        MainController.CheckAndShowDailyReward();
    }

    IEnumerator ScheduleShowRemoveAds()
    {
        yield return new WaitForSeconds(0.5f);
        MainController.OpenPopup(PopupType.RemoveAds);
    }
    IEnumerator ScheduleShowRateUs()
    {
        yield return new WaitForSeconds(0.5f);
        MainController.OpenPopup(PopupType.RateUs);
    }

    IEnumerator ScheduleShowLuckSpin()
    {
        yield return new WaitForSeconds(0.5f);
        MainController.OpenPopup(PopupType.LuckySpin);
    }
    IEnumerator ScheduleShowSpecialShop()
    {
        yield return new WaitForSeconds(0.5f);
        MainController.OpenPopup(PopupType.Shop);
    }

    IEnumerator ScheduleShowTreasure()
    {
        yield return new WaitForSeconds(0.5f);
        MainController.OpenPopup(PopupType.Treasure);
    }

    IEnumerator ScheduleShowUnlockSkin()
    {
        yield return new WaitForSeconds(0.5f);
        MainController.OpenPopup(PopupType.UnlockSkin);
    }
    IEnumerator ScheduleShowSpecialSkin()
    {
        yield return new WaitForSeconds(0.5f);
        MainController.OpenPopup(PopupType.SpecialSkin);
    }

    IEnumerator SchedulePlaySoundBG(float time)
    {
        yield return new WaitForSeconds(time);
        SoundManager.PlaySound(GameConstant.AUDIO_MUSIC_HOME, true, true);
        m_trySkinUi.SetActive(true);
    }

    IEnumerator ScheduleShowNoThanks()
    {
        yield return new WaitForSeconds(MainModel.timeDelayNothanks/1000f);
        GameObject nothanks = m_objComplete.transform.Find("text-nothanks").gameObject;
        nothanks.SetActive(true);
    }

    IEnumerator CoinMoveFinish()
    {
        OnUpdateCoin(MainModel.totalCoin);
        foreach(GameObject go in m_coins)
        {
            SoundManager.PlaySound(GameConstant.AUDIO_EAT_COIN, false);
            go.transform.DOMove(m_coinCompleteFinishPos.position, 0.1f).OnComplete(()=>{
                Destroy(go);
            });
            yield return new WaitForSeconds(0.05f);
        }
    }
    IEnumerator CreateCompleteCoin(int count)
    {
        if(count == 0)
        {
            yield break;
        }
        GameObject sound = SoundManager.PlaySound(GameConstant.AUDIO_RAIN_COIN, true);
        m_coins = new List<GameObject>();
        int counter = 0;
        count = count > 20 ? 20 : count;
        GameObject coinObj = m_coinCompleteHolder.Find("coin").gameObject;
        for(int i = 0; i < count; i++)
        {
            GameObject go = Instantiate(coinObj);
            m_coins.Add(go);
            go.SetActive(true);
            go.transform.SetParent(m_coinCompleteHolder, false);
            go.transform.position = m_coinCompletePos.position;
            float x = Random.Range(-m_coinCompleteHolder.sizeDelta.x/2,  m_coinCompleteHolder.sizeDelta.x/2);
            float y = Random.Range(-m_coinCompleteHolder.sizeDelta.y/2,  m_coinCompleteHolder.sizeDelta.y/2);
            Vector2 pos = new Vector2(x, y);
            go.transform.DOLocalMove(pos, 0.1f).OnComplete(()=>{
                counter++;
                if(counter >= count)
                {
                    Destroy(sound);
                    StartCoroutine(CoinMoveFinish());
                }
            });
            yield return new WaitForSeconds(0.05f);
        }
    }    

    void ShowLevelFail()
    {
        SoundManager.PlaySound(GameConstant.AUDIO_GAMEOVER, false);
        StartCoroutine(SchedulePlaySoundBG(3.5f));
        m_ballRed.GetComponent<BallUi>().SetAnimation(GameConstant.PLAYER_ANIMATION_SAD);
        m_ballBlue.GetComponent<BallUi>().SetAnimation(GameConstant.PLAYER_ANIMATION_SAD);
    }

    void ShowEffectWin(LevelResult result)
    {
        m_effectWin.gameObject.SetActive(true);
        if (result.mapLevel > MainModel.levelShowX2Reward)
        {
            m_objComplete.transform.Find("button-x2-reward-ads").gameObject.SetActive(true);
            m_objComplete.transform.Find("button-x2-reward").gameObject.SetActive(false);
            StartCoroutine(ScheduleShowNoThanks());
        }
        else
        {
            m_objComplete.transform.Find("button-x2-reward-ads").gameObject.SetActive(false);
            Transform x2Free = m_objComplete.transform.Find("button-x2-reward");
            x2Free.gameObject.SetActive(true);
            x2Free.Find("hand").gameObject.SetActive(result.mapLevel < 3);
            if(result.mapLevel > 2)
                StartCoroutine(ScheduleShowNoThanks());
        }

        StartCoroutine(SchedulePlaySoundBG(4f));
        TextMeshProUGUI textCoin = m_objComplete.transform.Find("level-reward/text-level-value").GetComponent<TextMeshProUGUI>();
        textCoin.text = (result.coin*GameConstant.COIN_RATIO).ToString();
        m_ballRed.GetComponent<BallUi>().SetAnimation(GameConstant.PLAYER_ANIMATION_LAUGH);
        m_ballBlue.GetComponent<BallUi>().SetAnimation(GameConstant.PLAYER_ANIMATION_LAUGH);
        //
        StartCoroutine(CreateCompleteCoin(result.coin));  
    }

    IEnumerator ScheduleLuckySpin()
    {
        m_textTimeLuckySpin.gameObject.SetActive(true);
        DateTime startTime = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
        DateTime now = DateTime.UtcNow;
        double time = (now - startTime).TotalSeconds;
        double timeStart =  double.Parse(PlayerPrefs.GetString(GameConstant.PLAYER_PREF_LUCKY_SPIN_START_TIME, time.ToString()));
        double count = double.Parse(PlayerPrefs.GetString(GameConstant.PLAYER_PREF_LUCKY_SPIN_COUNTDOWN_TIME, "43200"));
        PlayerPrefs.SetString(GameConstant.PLAYER_PREF_LUCKY_SPIN_START_TIME, timeStart.ToString());
        PlayerPrefs.SetString(GameConstant.PLAYER_PREF_LUCKY_SPIN_COUNTDOWN_TIME, count.ToString());
        PlayerPrefs.Save();
        //
        count -= (now - startTime.AddSeconds(timeStart)).TotalSeconds;
        while(count > 0)
        {
            TimeSpan t = TimeSpan.FromSeconds(count);
            m_textTimeLuckySpin.text = t.ToString(@"hh\:mm\:ss");
            yield return new WaitForSeconds(1f);
            count--;
        }
        MainController.UpdateSpin(1);
        PlayerPrefs.DeleteKey(GameConstant.PLAYER_PREF_LUCKY_SPIN_START_TIME);
        PlayerPrefs.DeleteKey(GameConstant.PLAYER_PREF_LUCKY_SPIN_COUNTDOWN_TIME);
    }

    void SceneOut()
    {
        m_mask.sizeDelta = new Vector2(7000, 7000);
        m_maskObject.SetActive(true);
        m_mask.DOSizeDelta(new Vector2(630, 630), 0.8f).SetEase(Ease.OutQuart).OnComplete(()=>{
            m_mask.DOSizeDelta(Vector2.zero, 0.5f).SetEase(Ease.OutQuart).OnComplete(()=>{
                GameAssetManager.api.OpenSceneLevel();
            }).SetDelay(0.5f); 
        });
    }

    IEnumerator SceneIn()
    {
        yield return null;
        m_mask.sizeDelta = Vector2.zero;
        m_maskObject.SetActive(true);
        m_mask.DOSizeDelta(new Vector2(630, 630), 0.5f).SetEase(Ease.InQuad).OnComplete(()=>{
            m_mask.DOSizeDelta(new Vector2(7000, 7000), 0.8f).SetEase(Ease.InQuad).OnComplete(()=>{  
                m_maskObject.SetActive(false);         
                AdsManager.Instance.ShowBanner();
                if(m_levelResult == null)
                    return;
                if (m_levelResult.isComplete)
                    ShowLevelComplete(m_levelResult); 
                else
                    ShowLevelFail();
            }).SetDelay(0.5f);
        });
    }
}
